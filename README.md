# CBIS
CBIS je informační systém pro sbory CB. Umožňuje správu akcí sboru.

## Instalace
Systém běží na PHP 5.7 a MySQL

1. Nastavte správně věci v data/access_config.php
1. Nahoďte DB
1. V tabule users vytvořte záznam - login je podle e-mailu, heslo se generuje klasicky password_hash. Privilegia nastavte na "userMgmt;privilegeElevation" - to by mělo stačit, abyste si v administraci přidali další
1. (Volitelné) Zařiďte si API účet pro Google calendar, zapněte to v access_config a nastavte cron na http://web/?/cron (ten periodicky aktualizuje google kalendář). Google kalendář je read-only.

## Nastavení systému
Teď se mi to nehce psát :D