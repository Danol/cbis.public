$(document).ready(function () {
	$(".eventCalendarItem").each(function () {
		var self = $(this);
		var loadingId = 0;

		self.hover(function () {
			loadingId++;
			var myLoadingId = loadingId;

			$.get({
				url: "?/ajax-eventPopup?id=" + self.attr("data-eventid"),
				success: function (data) {
					if (loadingId !== myLoadingId)
						return;

					self.popup("destroy");
					self.popup({
						html: data,
						inline: true,
						position: "left center"
					}).popup("show");

					loadingId ++;
				}
			});
		}, function () {
			loadingId++;
			self.popup("destroy");
		});
	});
	$(".eventOverviewEventMenu").dropdown({
		action: function(t, v, e) {
			e.click();
			return true;
		}
	});

	$(".eventOverviewMenu").each(function () {
		var self = $(this);
		self.sticky({
			context: $(self.attr("data-context"))
		});
	});
});