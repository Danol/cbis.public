$(document).ready(function () {
	$("#menuNotificationsButton").popup({
		popup: "#menuNotificationsMenu",
		on: "click",
		position: "bottom right",
		hideOnScroll: false,

		onShow: function () {
			$("#menuNotificationsMenu").html("<div class=\"ui active inline centered loader\"></div>");
			$.get("?/ajax-notifications", function (data) {
				$("#menuNotificationsCount:visible").transition("scale");
				$("#menuNotificationsMenu").html(data);
				$("#menuNotificationsButton").popup('reposition');
			});
		}
	});

	$('.announcementMenuPopup').each(function () {
		$(this).popup({
			hoverable: true,
			position: 'top right',
			distanceAway: -10
		});
	});
});