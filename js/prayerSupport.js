$(document).ready(function () {
	$("#prayerSupportTable tr.baseRow:not(.-disabled) td:not(.clockColumn)").mousedown(function () {
		$(this).toggleClass("selected");
	});
	$("#prayerSupportTable tr.baseRow:not(.-disabled) td:not(.clockColumn)").mouseenter(function () {
		if (mouseLeftButtonIsDown)
			$(this).toggleClass("selected");
	});

	prevPrayerSupportPopup = null;
	$("#prayerSupportTable td.record").mouseenter(function () {
		if (prevPrayerSupportPopup != null)
			prevPrayerSupportPopup.popup("destroy");
		prevPrayerSupportPopup = $(this);

		$("#prayerSupportPopup img.avatar.image").attr("src", "");
		$("#prayerSupportPopup img.avatar.image").attr("src", $(this).attr("data-avatar"));
		if ($(this).attr("data-avatar"))
			$("#prayerSupportPopup img.avatar.image").show();
		else
			$("#prayerSupportPopup img.avatar.image").hide();

		$("#prayerSupportPopup .displayTime").text($(this).attr("data-time"));
		$("#prayerSupportPopup .displayName").html($(this).attr("data-pseudonym"));

		$(this).popup({
			popup: "#prayerSupportPopup",
			setFluidWidth: false
		}).popup('show');
	});
});

function prayerSupportSaveChanges() {
	var data = [];
	$("#prayerSupportTable td.selected").each(function () {
		data.push($(this).attr("data-time"));
	});

	$(formElementsCSSID("updatePrayerSupportCalendar", "data", "")).val(data.join(";"));
	$(formCSSID("updatePrayerSupportCalendar", "")).submit();
}

function prayerSupportClear() {
	$(formElementsCSSID("updatePrayerSupportCalendar", "data", "")).val("");
	$(formCSSID("updatePrayerSupportCalendar", "")).submit();
}

