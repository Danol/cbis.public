$(document).ready(function () {
	$(".commentReplyButton").click(function () {
		var instanceId = $(this).attr("data-instanceid");
		var replyId = $(this).attr("data-replyid");
		var deepReplyId = $(this).attr("data-deepreplyid");

		var replyForm = $(formCSSID("comment", instanceId + "-replyForm"));
		replyForm.appendTo($(this).closest(".content"));
		replyForm.transition("hide").transition({
			animation: "fade down",
			reverse: false
		});

		$(formElementsCSSID("comment", "replyTo", instanceId + "-replyForm")).val(replyId);
		$(formElementsCSSID("comment", "deepReplyTo", instanceId + "-replyForm")).val(deepReplyId);
		$(formCSSID("comment", instanceId + "-replyForm")).attr("action", $(this).attr("data-action"));
		$(formElementsCSSID("comment", "text", instanceId + "-replyForm")).focus();

		$(".messageGroup-replyComments-" + instanceId).remove();
		replyForm.find(".field.error").removeClass("error");
	});

	$(".comment .replyToPrefix").mouseenter(function () {
		var comment = $(".comment[data-id=\"" + $(this).attr("data-sourceid") + "\"]");
		if (!comment.transition("is animating"))
			comment.transition("shake");
	});
});