<?php
	$INCLUDED ?? false or die;
	
	date_default_timezone_set( "Europe/Prague" );
	
	include "$DATA_PATH/access_config.php";
	
	$ENGINE_MODULES = [
			"efficiencyUtils", ":modalsContent", "tmpId", "db", "users", "url", "messages", "pages", ":privileges", "privileges", ":extPrivileges", ":parsedown", ":textFormatting", ":log", ":users", ":groups", ":dateUtils", ":announcements", "js", ":comments", ":texts", ":notifications", ":regime", "forms", "pagination", ":eventOverview", "dataTable"
	];
	
	$PRIVILEGE_DESCS = [
			"testing" => "Testování nových modulů",
			
			"<i class=\"user icon\"></i>Uživatelé a skupiny",
			"userMgmt" => "Správa uživatelů",
			"groupMgmt" => "Správa skupin",
			"privilegeElevation" => "Možnost přiřazovat libovolná privilegia",
			
			"<i class='calendar icon'></i>Události",
			"eventMgmt" => "Správa událostí",
			"eventCreating" => "Vytváření nových událostí",
			
			"<i class=\"sign language icon\"></i>Přímluvné modlitby",
			"prayerSupport" => "Přístup do sekce Přímluvné modlitby",
			"prayerSupportMgmt" => "Správa sekce Přímluvné modlitby",
			
			"<i class=\"settings icon\"></i>Ostatní",
			"commentMgmt" => "Moderování komentářů",
			"announcements" => "Úpravy oznámení na webu",
			"roomMgmt" => "Správa místností"
	];
	
	$PRIVILEGE_LIST = array_keys( $PRIVILEGE_DESCS );