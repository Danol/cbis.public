<?php
	$INCLUDED ?? false or die;
	
	function usersRegisterEA_canCurrentUserChange( $attr, $canRegister = true ) {
		if( $canRegister )
			return !array_search( loggedUserId(), usersSemicolonStrToArray( $attr["deducedValue"] ) ) && currentUserMeetsSemicolonUsersGroupStr( $attr["params"] );
		else
			return array_search( loggedUserId(), usersSemicolonStrToArray( $attr["deducedValue"] ) ) !== false;
	}
	
	$DATA = [
			"elements" => [
					"attr-allowRegister" => [
							"label" => "Kdo se může přihlásit",
							"type" => "multiSearchSelect",
							"items" => usersGroupsIconAssocArray(),
							"valueTransformers" => ":semicolonWrapArray"
					],
					"attr--value" => [
							"type" => "multiSearchSelect",
							"label" => "Výchozí hodnota",
							"items" => usersAssocArray(),
							"valueTransformers" => ":semicolonWrapArray"
					]
			],
		
		// Manage fields -> params
			"toParamsTransformer" => function( $data ) {
				return $data["attr-allowRegister"];
			},
			"fromParamsTransformer" => function( $params ) {
				return [ "attr-allowRegister" => $params ];
			},
			
			"displayValueTransformer" => function( $value, $params ) {
				return usersSemicolonStrLabels( $value );
			},
			"inlineDisplayValueTransformer" => function( $value, $params ) {
				$assocArray = usersAssocArray();
				$result = usersSemicolonStrToArray( $value );
				foreach( $result as &$val )
					$val = $assocArray[$val];

				$result = implode( ",", $result );
			},
			
			"interactiveDisplayValueTransformer" => function( $attr, $eventId ) {
				$result = "";
				
				if( $attr["deducedValue"] )
					$result .= usersSemicolonStrLabels( $attr["deducedValue"] );
				
				if( usersRegisterEA_canCurrentUserChange( $attr, false ) ) {
					$result .= formHTML( "usersRegisterEA_unregisterUser", [
							"values" => [
									"event" => $eventId,
									"attribute" => $attr["uniqueId"]
							]
					] );
				} elseif( usersRegisterEA_canCurrentUserChange( $attr, true ) ) {
					$result .= formHTML( "usersRegisterEA_registerUser", [
							"values" => [
									"event" => $eventId,
									"attribute" => $attr["uniqueId"]
							]
					] );
				}
				
				return $result;
			},
		
		// $canRegister = false -> returns if can unregister
			"canCurrentUserChange" => "usersRegisterEA_canCurrentUserChange",
			
			"editElements" => function( $attr ) {
				return [
						"attr-{$attr["uniqueId"]}-value" => [
								"type" => "multiSearchSelect",
								"label" => $attr["name"],
								"defaultValue" => $attr["deducedValue"],
								"items" => semicolonUsersGroupStrFilteredUserAssocArray($attr["params"]),
								"valueTransformers" => ":semicolonWrapArray"
						]
				];
			},
	];