<?php
	$INCLUDED ?? false or die;
	
	require_once "$DATA_PATH/config/rooms.php";
	
	$DATA = [
			"elements" => [
					"attr--value" => [
							"type" => "multiSearchSelect",
							"label" => "Výchozí hodnota",
							"items" => roomsAssocArray(),
							"valueTransformers" => ":semicolonWrapArray"
					],
					
					"name" => [
							"defaultValue" => "Místnost"
					],
					"commonId" => [
							"defaultValue" => "rooms"
					],
					"icon" => [
							"defaultValue" => "building"
					],
			],
			
			"displayValueTransformer" => function( $value, $params ) {
				$assoc = roomsAssocArray();
				
				$result = "";
				foreach( explode( ";", $value ) as $room ) {
					if( !isset( $assoc[$room] ) )
						continue;
					
					$result .= "<a href='" . subLevelUrl( [ "roomCalendar", "id" => $room ] ) . "' class='ui tiny compact label'>{$assoc[$room]}</a>";
				}
				return $result;
			},
			"inlineDisplayValueTransformer" => function( $value, $params ) {
				$assoc = roomsAssocArray();
				$result = [];

				foreach( explode( ";", $value ) as $room ) {
					if( !isset( $assoc[$room] ) )
						continue;
					
					$result[] = $assoc[$room];
				}
				return implode( ", ", $result );
			},
		
		// $canRegister = false -> returns if can unregister
			"canCurrentUserChange" => "usersRegisterEA_canCurrentUserChange",
			
			"editElements" => function( $attr ) {
				return [
						"attr-{$attr["uniqueId"]}-value" => [
								"type" => "multiSearchSelect",
								"label" => $attr["name"],
								"defaultValue" => $attr["deducedValue"],
								"items" => roomsAssocArray(),
								"valueTransformers" => ":semicolonWrapArray"
						]
				];
			},
	];