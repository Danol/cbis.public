<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"attr--value" => [
							"type" => "searchAdditionsSelect",
							"label" => "Výchozí hodnota",
							"items" => [ "" => "(nikdo)" ] + usersAssocArray(),
					],
					"attr-allowRegister" => [
						"label" => "Kdo se může přihlásit",
						"type" => "multiSearchSelect",
						"items" => usersGroupsIconAssocArray(),
						"valueTransformers" => ":semicolonWrapArray"
					],
			],

			// Manage fields -> params
			"toParamsTransformer" => function( $data ) {
				return $data["attr-allowRegister"];
			},
			"fromParamsTransformer" => function( $params ) {
				return [ "attr-allowRegister" => $params ];
			},
			
			
			"interactiveDisplayValueTransformer" => function( $attr, $eventId ) {
				global $USER_DATA;

				if( $attr["deducedValue"] != "" ) {
					$result = "<span class='ui compact label'>{$attr["deducedValue"]}</span>";
					
					if( $attr["deducedValue"] == ($USER_DATA["displayName"]??"") ) {
						$result .= formHTML( "userTextEA_unregisterUser", [
								"values" => [
										"event" => $eventId,
										"attribute" => $attr["uniqueId"]
								]
						] );
					}
					return $result;
				}
				
				if( currentUserMeetsSemicolonUsersGroupStr( $attr["params"] ) ) {
					return formHTML( "userTextEA_registerUser", [
							"values" => [
									"event" => $eventId,
									"attribute" => $attr["uniqueId"]
							]
					] );
				}
				
				return "";
			},
		
			// $canRegister = false -> returns if can unregister
			"canCurrentUserChange" => function( $attr, $canRegister = true ) {
				global $USER_DATA;

				if( $canRegister )
					return !$attr["deducedValue"] && currentUserMeetsSemicolonUsersGroupStr( $attr["params"] );
				else
					return $attr["deducedValue"] == ($USER_DATA["displayName"]??"");
			},
			
			"editElements" => function( $attr ) {
				return [
						"attr-{$attr["uniqueId"]}-value" => [
								"type" => "searchAdditionsSelect",
								"label" => $attr["name"],
								"defaultValue" => $attr["deducedValue"],
								"items" => [ "" => "(nikdo)" ] + semicolonUsersGroupStrFilteredUserAssocArray($attr["params"])
						]
				];
			},
	];