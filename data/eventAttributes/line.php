<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"attr--value" => [
							"label" => "Výchozí hodnota",
							"type" => "line"
					]
			],
		
			"editElements" => function( $attr ) {
				return [
						"attr-{$attr["uniqueId"]}-value" => [
								"type" => "line",
								"label" => $attr["name"],
								"defaultValue" => $attr["deducedValue"]
						]
				];
			},
	];