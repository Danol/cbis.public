<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"attr-items" => [
							"label" => "Položky seznamu (každou na nový řádek)",
							"type" => "textarea"
					],
					"attr-allowCustomValues" => [
							"type" => "checkbox",
							"label" => "Povolit uživateli zadat vlastní hodnotu (mimo položky seznamu)"
					],
					"attr--value" => [
							"label" => "Výchozí hodnota",
							"type" => "line"
					]
			],
		
		// Manage fields -> params
			"toParamsTransformer" => function( $data ) {
				return json_encode( dbSecureFieldsArray( $data, [ "attr-items", "attr-allowCustomValues" ] ) );
			},
			"fromParamsTransformer" => function( $params ) {
				return json_decode( $params, true ) ?? [];
			},
			
			
			"editElements" => function( $attr ) {
				$params = json_decode( $attr["params"], true ) ?? [];
				$allowCustom = $params["attr-allowCustomValues"] ?? false;
				
				$items = explode( "\n", $params["attr-items"] ?? "" );
				if( !$allowCustom )
					$items = array_combine( $items, $items );
				
				return [
						"attr-{$attr["uniqueId"]}-value" => [
								"type" => $allowCustom ? "searchAdditionsSelect" : "searchSelect",
								"label" => $attr["name"],
								"defaultValue" => $attr["deducedValue"],
								"items" => $items
						]
				];
			},
	];