<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"attr--value" => [
							"label" => "Implicitně zaškrtnuto",
							"type" => "checkbox"
					]
			],
			
			"displayValueTransformer" => function( $value, $params ) {
				return $value ? "ano" : "ne";
			},
			
			"editElements" => function( $attr ) {
				return [
						"attr-{$attr["uniqueId"]}-value" => [
								"type" => "checkbox",
								"label" => $attr["name"],
								"defaultValue" => $attr["deducedValue"]
						]
				];
			},
	];