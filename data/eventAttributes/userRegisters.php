<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"attr-allowRegister" => [
							"label" => "Kdo se může přihlásit",
							"type" => "multiSearchSelect",
							"items" => usersGroupsIconAssocArray(),
							"valueTransformers" => ":semicolonWrapArray"
					],
					"attr--value" => [
							"type" => "searchSelect",
							"label" => "Výchozí hodnota",
							"items" => [ "" => "(nikdo)" ] + usersAssocArray(),
					]
			],
		
		// Manage fields -> params
			"toParamsTransformer" => function( $data ) {
				return $data["attr-allowRegister"];
			},
			"fromParamsTransformer" => function( $params ) {
				return [ "attr-allowRegister" => $params ];
			},
			
			"displayValueTransformer" => function( $value, $params ) {
				return $value ? usersGroupsArrayLabels( [ "u$value" ] ) : "";
			},
			"displayValueTransformer" => function( $value, $params ) {
				return $value ? usersAssocArray()[ $value ] : "";
			},
			
			"interactiveDisplayValueTransformer" => function( $attr, $eventId ) {
				if( $attr["deducedValue"] != "" ) {
					$result = usersArrayLabels( [ $attr["deducedValue"] ] );
					
					if( $attr["deducedValue"] == loggedUserId() ) {
						$result .= formHTML( "userRegistersEA_unregisterUser", [
								"values" => [
										"event" => $eventId,
										"attribute" => $attr["uniqueId"]
								]
						] );
					}
					return $result;
				}
				
				if( currentUserMeetsSemicolonUsersGroupStr( $attr["params"] ) ) {
					return formHTML( "userRegistersEA_registerUser", [
							"values" => [
									"event" => $eventId,
									"attribute" => $attr["uniqueId"]
							]
					] );
				}
				
				return "";
			},
		
		// $canRegister = false -> returns if can unregister
			"canCurrentUserChange" => function( $attr, $canRegister = true ) {
				if( $canRegister )
					return !$attr["deducedValue"] && currentUserMeetsSemicolonUsersGroupStr( $attr["params"] );
				else
					return $attr["deducedValue"] == loggedUserId();
			},
			
			
			"editElements" => function( $attr ) {
				return [
						"attr-{$attr["uniqueId"]}-value" => [
								"type" => "searchSelect",
								"label" => $attr["name"],
								"defaultValue" => $attr["deducedValue"],
								"items" => [ "" => "(nikdo)" ] + semicolonUsersGroupStrFilteredUserAssocArray($attr["params"])
						]
				];
			},
	];