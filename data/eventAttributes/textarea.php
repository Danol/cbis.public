<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"attr--value" => [
							"label" => "Výchozí hodnota",
							"type" => "textarea"
					]
			],
			
			"editElements" => function( $attr ) {
				return [
						"attr-{$attr["uniqueId"]}-value" => [
								"type" => "textarea",
								"label" => $attr["name"],
								"defaultValue" => $attr["deducedValue"]
						]
				];
			},
	];