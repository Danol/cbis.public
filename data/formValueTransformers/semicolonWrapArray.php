<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"fromPOST" => function( $data ) {
				return ";" . implode( ";", array_filter( $data ) ) . ";";
			},
			"toPOST" => function( $data ) {
				if( $data == ";;" || !$data )
					return [];
				
				return explode( ";", substr( $data, 0, -1 ) );
			}
	];