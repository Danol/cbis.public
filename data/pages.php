<?php
$INCLUDED ?? false or die;

/*
 * $PAGE_TREE val:
 * "ID" => [
 *  "label" => "LABEL"
 * }
 */

$PAGE_404 = [
	"label" => "Stránka nenalezena",
	"file" => "system/404"
];

$PAGE_403 = [
	"label" => "Nedostatečná oprávnění",
	"file" => "system/403"
];

function selectBasedPageLabel($table, $suffix, $field = "name") {
	return function ($r) use ($table, $field, $suffix) {
		return dbQueryValueDef("SELECT $field FROM $table WHERE id = ?", "404", $r["id"] ?? 0) . " ($suffix)";
	};
}

$PAGES = [
	"login" => [
		"label" => "Přihlášení",
		"file" => "system/login",
		"defaultShowMessages" => false,
		"uiContainer" => false
	],

	"home" => [
		"label" => "Hlavní stránka",
		"icon" => "home icon",
		"privileges" => "login",
		"file" => "users/home",
		"showLabel" => false
	],
	"monthCalendar" => [
		"label" => "Měsíční kalendář",
		"icon" => "calendar icon",
		"file" => "events/monthCalendar",
		"privileges" => "login"
	],
	"weekCalendar" => [
		"label" => "Týdenní kalendář",
		"icon" => "calendar outline icon",
		"file" => "events/weekCalendar",
		"privileges" => "login"
	],
	"musicCalendar" => [
		"label" => "Rozvrh hudebníků",
		"icon" => "music icon",
		"file" => "events/musicCalendar",
		"privileges" => "login"
	],
	"projectionCalendar" => [
		"label" => "Rozvrh promítačů",
		"icon" => "video icon",
		"file" => "events/projectionCalendar",
		"privileges" => "login"
	],
	"eventList" => [
		"label" => "Seznam událostí",
		"icon" => "list icon",
		"file" => "events/eventList",
		"privileges" => "login"
	],
	"feedback" => [
		"label" => "Zpětná vazba",
		"icon" => "bug icon",
		"privileges" => "login",
	],

	"regularEvents" => [
		"label" => "Pravidelné události",
		"icon" => "retweet icon",
		"file" => "events/regularEvents",
		"privileges" => "login"
	],
	"regularEvent" => [
		"label" => selectBasedPageLabel("regularEvents", "pravidelná událost"),
		"file" => "events/regularEvent",
		"privileges" => "login"
	],
	"event" => [
		"label" => "Událost",
		"file" => "events/event",
		"privileges" => "login"
	],
	"newEvent" => [
		"label" => "Nová událost",
		"file" => "events/newEvent",
		"privileges" => "login"
	],

	"userPanel" => [
		"label" => "Uživatelský panel",
		"privileges" => "login",
		"file" => "users/userPanel"
	],

	"userMgmt" => [
		"label" => "Uživatelé",
		"privileges" => "login",
		"icon" => "user icon",
		"file" => "users/userMgmt"
	],
	"user" => [
		"label" => selectBasedPageLabel("users", "uživatel", "displayName"),
		"privileges" => "login",
		"file" => "users/user"
	],
	"ajax-notifications" => [
		"ajax" => true,
		"privileges" => "login",
		"file" => "ajax/notifications"
	],
	"ajax-eventPopup" => [
		"ajax" => true,
		"privileges" => "login",
		"file" => "ajax/eventPopup"
	],
	"api-eventList" => [
		"ajax" => true,
		"file" => "api/eventList"
	],

	"groupMgmt" => [
		"label" => "Skupiny",
		"privileges" => "login",
		"file" => "groups/groupMgmt",
		"icon" => "users icon"
	],
	"group" => [
		"label" => selectBasedPageLabel("groups", "skupina"),
		"privileges" => "login",
		"file" => "groups/group"
	],

	"prayerSupport" => [
		"label" => "Přímluvné modlitby",
		"icon" => "sign language icon",
		"file" => "prayerSupport/prayerSupport"
	],

	"roomMgmt" => [
		"label" => "Místnosti",
		"icon" => "building icon",
		"file" => "rooms/roomMgmt",
		"privileges" => "login"
	],
	"roomCalendar" => [
		"label" => selectBasedPageLabel("rooms", "rozvrh místnosti"),
		"icon" => "building icon",
		"file" => "rooms/roomCalendar",
		"privileges" => "login"
	],

	"printables" => [
		"label" => "K tisku",
		"icon" => "print icon",
		"file" => "printables/printables",
		"privileges" => "login"
	],
	"printable-monthCalendar" => [
		"label" => "Měsíční kalendář (k tisku)",
		"file" => "printables/monthCalendar",
		"privileges" => "login",
		"ajax" => true
	],

	"cron" => [
		"file" => "ajax/cron",
		"ajax" => true
	]
];

if (isLoggedIn()) {
	$MENU_PAGES = [
		"home",
		[
			"label" => "Události",
			"icon" => "calendar icon",
			"items" => ["weekCalendar", "monthCalendar", "eventList", "musicCalendar", "projectionCalendar", "printables"]
		],
		"prayerSupport",
		[
			"label" => "Přehledy",
			"icon" => "list layout icon",
			"items" => ["regularEvents", "userMgmt", "groupMgmt", "roomMgmt"]
		],
		"feedback"
	];
	$PAGE_HOME = "home";
} else {
	$MENU_PAGES = ["login", "prayerSupport"];
	$PAGE_HOME = "login";
}