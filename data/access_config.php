<?php
	$INCLUDED ?? false or die;
	
	$DB_HOST = "HOST";
	$DB_USER = "USER";
	$DB_PASSWD = "PASSWORD";
	$DB_DB = "DB";
	
	// Requires to run CRON: http://web/?/cron
	// Also requires $DATA_PATH/calendar_key.json
	$UPDATE_GOOGLE_CALENDAR = false;
	$GOOGLE_CALENDAR_ID = "xxx";