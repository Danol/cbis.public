<?php
	$INCLUDED ?? false or die;
	
	function iconsAssocArray() {
		global $ICONS_ASSOC_ARRAY;
		if( isset( $ICONS_ASSOC_ARRAY ) )
			return $ICONS_ASSOC_ARRAY;
		
		$ICONS_ASSOC_ARRAY = [];
		
		$data = [
				"" => "Bez ikony",
				"student" => "Akademická čepice",
				
				"building" => "Budova",
				
				"gift" => "Dárek",
				"child" => "Dítě",
				"doctor" => "Doktor",
				"shipping" => "Doprava",
				"birthday" => "Dort",
				
				"film" => "Film",
				
				"wait" => "Hodiny",
				
				"info" => "Info",
				
				"food" => "Jídlo",
				
				"coffee" => "Káva",
				"privacy" => "Klíč",
				"address book" => "Kontakt",
				"comment" => "Komentář",
				
				"hotel" => "Lůžko",
				
				"announcement" => "Megafon",
				"soccer" => "Míč",
				"spy" => "Moderátor",
				"sign language" => "Modlitba",
				
				"shopping basket" => "Nákup",
				"music" => "Noty",
				
				"unhide" => "Oko",
				
				"money" => "Peníze",
				"laptop" => "Počítač",
				"trophy" => "Pohár",
				"translate" => "Překlad",
				"pin" => "Připínáček",
				"first aid" => "Privní pomoc",
				
				"users" => "Skupina",
				"legal" => "Soudcovské kladivo",
				"tag" => "Štítek",
				"world" => "Svět",
				
				"marker" => "Umístění",
				"university" => "Univerzita",
				
				"flag" => "Vlajka",
				"ticket" => "Vstupenka",
				
				"idea" => "Žárovka",
				"alarm" => "Zvonek",
				"volume up" => "Zvuk",
		];
		
		foreach( $data as $i => $desc )
			$ICONS_ASSOC_ARRAY[$i] = "<i class='$i icon'></i>$desc";
		
		return $ICONS_ASSOC_ARRAY;
	}