<?php
	$INCLUDED ?? false or die;
	
	call_user_func( function() {
		global $NOTIFICATION_TYPES;
		
		$NOTIFICATION_TYPES = [
				"+groupOp" => [
						"icon" => "spy",
						"text" => "%actors% vás ustanovil|ustanovili správcem skupiny %group:%id%%."
				],
				"-groupOp" => [
						"icon" => "spy",
						"text" => "%actors% vám odebral|odebrali správcovství skupiny %group:%id%%."
				],
				"+groupMem" => [
						"icon" => "users",
						"text" => "%actors% vás přidal|přidali do skupiny %group:%id%%."
				],
				"-groupMem" => [
						"icon" => "users",
						"text" => "%actors% vás odebral|odebrali ze skupiny %group:%id%%."
				],
				"commentReply" => [
						"icon" => "comment",
						"text" => function( $subjectId ) {
							return "%actors% odpověděl|odpověděli na <a href='" . linkToComment( $subjectId ) . "'>váš komentář</a>";
						}
				]
		];
	} );