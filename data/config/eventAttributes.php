<?php
$INCLUDED ?? false or die;

require_once "$DATA_PATH/config/icons.php";

call_user_func(function () {
	global $EVENT_ATTRIBUTE_TYPES;

	$EVENT_ATTRIBUTE_TYPES = [
		"line" => "Krátký text",
		"textarea" => "Dlouhý text",
		"select" => "Seznam",
		"checkbox" => "Zaškrtávací políčko",
		"userRegisters" => "Uživatel",
		"usersRegister" => "Výčet uživatelů",
		"userText" => "Uživatel nebo krátký text",
		"rooms" => "Místnosti"
	];
});

function eventAttributeData($type) {
	global $DATA_PATH, $EVENT_ATTRIBUTE_DATA, $EVENT_ATTRIBUTE_TYPES, $INCLUDED;
	if (isset($EVENT_ATTRIBUTE_DATA[$type]))
		return $EVENT_ATTRIBUTE_DATA[$type];

	if (!isset($EVENT_ATTRIBUTE_TYPES[$type]))
		throw new Exception("Typ atributu '$type' neexistuje");

	include "$DATA_PATH/eventAttributes/$type.php";

	$DATA = array_merge([
		// Manage fields -> params
		"toParamsTransformer" => function ($data) {
			return "";
		},
		"fromParamsTransformer" => function ($params) {
			return [];
		},

		// From POST
		"toValueTransformer" => function ($data, $attrId = null) {
			return $data["attr-$attrId-value"] ?? "";
		},
		"fromValueTransformer" => function ($value, $attrId = null) {
			return ["attr-$attrId-value" => $value];
		},

		// Value -> displayValue
		"displayValueTransformer" => function ($value, $params) {
			return $value;
		},
	], $DATA);

	$EVENT_ATTRIBUTE_DATA[$type] = $DATA;
	return $DATA;
}

function regularEventAttributeForm($type, $attribute) {
	global $INCLUDED, $REGULAR_EVENT_ATTRIBUTE_FORMS;

	if (!isset($REGULAR_EVENT_ATTRIBUTE_FORMS))
		$REGULAR_EVENT_ATTRIBUTE_FORMS = [];

	if (isset($REGULAR_EVENT_ATTRIBUTE_FORMS[$type])) {
		$result = $REGULAR_EVENT_ATTRIBUTE_FORMS[$type];
	} else {
		$result = getForm("editRegularEventAttribute", eventAttributeData($type));
		$result["elements"]["type"]["defaultValue"] = $type;
		$result["elements"][] = [
			"type" => "submit",
			"label" => "Uložit změny"
		];

		$REGULAR_EVENT_ATTRIBUTE_FORMS[$type] = $result;
	}

	$result["values"] = array_merge(
		$attribute,
		$result["fromValueTransformer"]($attribute["defaultValue"] ?? ""),
		$result["fromParamsTransformer"]($attribute["params"] ?? "")
	);

	return $result;
}

function eventAttributeForm($type, $attribute) {
	global $INCLUDED, $REGULAR_EVENT_ATTRIBUTE_FORMS;

	if (!isset($EVENT_ATTRIBUTE_FORMS))
		$EVENT_ATTRIBUTE_FORMS = [];

	if (isset($EVENT_ATTRIBUTE_FORMS[$type])) {
		$result = $EVENT_ATTRIBUTE_FORMS[$type];
	} else {
		$result = getForm("editEventAttribute", eventAttributeData($type));
		$result["elements"]["type"]["defaultValue"] = $type;
		$result["elements"][] = [
			"type" => "submit",
			"label" => "Uložit změny"
		];

		$EVENT_ATTRIBUTE_FORMS[$type] = $result;
	}

	$result["values"] = array_merge(
		$attribute,
		$result["fromValueTransformer"]($attribute["defaultValue"] ?? ""),
		$result["fromParamsTransformer"]($attribute["params"] ?? "")
	);

	return $result;
}

function eventAttributes($event, $regularEvent) {
	$result = [];

	if (!$event["id"] && $regularEvent) {
		$q = dbQuery("
						SELECT
							regularEventsAttributes.id AS id, icon, name, NULL AS value, defaultValue, params, type, defaultValue AS deducedValue, visibility, visibleTo, commonId,
							TRUE AS isRegularEventAttribute, 'eventsRegularAttributes' AS editTable, regularEventsAttributes.id AS recId,
							CONCAT('r', regularEventsAttributes.id) AS uniqueId
						FROM regularEventsAttributes
						INNER JOIN regularEventsAttributesHistory ON ( regularEventsAttributes.id = regularEventsAttributesHistory.id ) AND ( validSince <= ? ) AND ( ( validUntil > ? ) OR ( validUntil IS NULL ) )
						WHERE ( regularEvent = ? )",
			$event["attributesVersion"], $event["attributesVersion"], $regularEvent["id"]
		);
		$result = array_merge($result, $q->fetchAll());

	} elseif ($event["id"] && $regularEvent) {
		$q = dbQuery("
						SELECT
							eventsRegularAttributes.id AS id, icon, name, value, defaultValue, params, type, IFNULL(value, defaultValue) AS deducedValue, visibility, visibleTo, commonId,
							TRUE AS isRegularEventAttribute, 'eventsRegularAttributes' AS editTable, eventsRegularAttributes.id AS recId,
							CONCAT('r', regularEventsAttributes.id) AS uniqueId
						FROM eventsRegularAttributes
						INNER JOIN regularEventsAttributesHistory ON ( attribute = regularEventsAttributesHistory.id ) AND ( validSince <= ? ) AND ( ( validUntil > ? ) OR ( validUntil IS NULL ) )
						INNER JOIN regularEventsAttributes ON ( attribute = regularEventsAttributes.id )
						WHERE event = ?",
			$event["attributesVersion"], $event["attributesVersion"], $event["id"]
		);
		$result = array_merge($result, $q->fetchAll());

	}

	if ($event["id"]) {
		$q = dbQuery("
						SELECT
							id, icon, name, value, params, type, value AS defaultValue, value AS deducedValue, visibility, visibleTo, commonId,
							FALSE AS isRegularEventAttribute, 'eventsAttributes' AS editTable, id AS recId,
							CONCAT('e', id) AS uniqueId
						FROM eventsAttributes
						WHERE event = ?",
			$event["id"]
		);
		$result = array_merge($result, $q->fetchAll());
	}

	usort($result, function ($a, $b) {
		if ($a["name"] == $b["name"])
			return 0;
		elseif ($a["name"] < $b["name"])
			return -1;
		else
			return 1;
	});

	return $result;
}

function eventHasAttribute($event, $regularEvent, $commonId) {
	foreach (eventAttributes($event, $regularEvent) as $attr) {
		if ($attr["commonId"] == $commonId)
			return true;
	}

	return false;
}

function eventAttributeValue($event, $regularEvent, $commonId, $defaultValue = null) {
	foreach (eventAttributes($event, $regularEvent) as $attr) {
		if ($attr["commonId"] == $commonId)
			return $attr["deducedValue"];
	}

	return $defaultValue;
}

function eventAttributeValueFromAttributes($attributes, $commonId, $defaultValue = null) {
	foreach ($attributes as $attr) {
		if ($attr["commonId"] == $commonId)
			return $attr["deducedValue"];
	}

	return $defaultValue;
}

function eventAttribute($event, $regularEvent, $commonId, $defaultValue = null) {
	foreach (eventAttributes($event, $regularEvent) as $attr) {
		if ($attr["commonId"] == $commonId)
			return $attr;
	}

	return $defaultValue;
}