<?php
	$INCLUDED ?? false or die;
	
	function roomsAssocArray() {
		global $ROOMS_ASSOC_ARRAY;
		
		if( isset( $ROOMS_ASSOC_ARRAY ) )
			return $ROOMS_ASSOC_ARRAY;
		
		$ROOMS_ASSOC_ARRAY = [];
		$q = dbQuery( "SELECT id, name FROM rooms WHERE NOT deleted ORDER BY name ASC" );
		while( $r = $q->fetch() )
			$ROOMS_ASSOC_ARRAY[$r["id"]] = $r["name"];
		
		return $ROOMS_ASSOC_ARRAY;
	}