<?php
	$INCLUDED ?? false or die;
	
	function canManageAnnouncements( $sectionType, $sectionId ) {
		switch( $sectionType ) {
			
			case "prayerSupport":
				return userHasPrivilege( "prayerSupportMgmt" );
			
			case "homePage":
				return userHasPrivilege( "announcements" );
			
			default:
				return false;
		}
	}