<?php
	$INCLUDED ?? false or die;
	
	function canComment( $topicType, $topicId ) {
		switch( $topicType ) {
			
			case "prayerSupport":
				return userHasPrivilege( "prayerSupport" );
			
			case "feedback":
				return isLoggedIn();
				
			case "event":
				return isLoggedIn();
			
			default:
				return false;
			
		}
	}
	
	function canModerateComments( $topicType, $topicId ) {
		if( userHasPrivilege( "commentMgmt" ) )
			return true;
		
		switch( $topicType ) {
			
			case "prayerSupport":
				return userHasPrivilege( "prayerSupportMgmt" );
			
			default:
				return false;
			
		}
	}
	
	function linkToComment( $commentId ) {
		$q = dbQuery( "SELECT topicType, topicId FROM comments WHERE ( NOT deleted ) AND ( id = ? )", $commentId );
		if( !$q->rowCount() )
			return "";
		
		$r = $q->fetch();
		switch( $r["topicType"] ) {
			
			case "prayerSupport":
				return absoluteUrlAdv( [ [ "prayerSupport", "week" => $r["topicId"] ] ], "comment-$commentId" );
			
			case "feedback":
				return absoluteUrlAdv( [ "feedback" ], "comment-$commentId" );
			
			default:
				return "";
			
		}
	}