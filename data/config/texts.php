<?php
	$INCLUDED ?? false or die;
	
	function userCanEditText( $textId ) {
		switch( $textId ) {
			
			case "prayerSupport-longInfo":
			case "prayerSupport-shortInfo":
				return userHasPrivilege( "prayerSupportMgmt" );
			
			default:
				return false;
			
		}
	}