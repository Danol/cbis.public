<?php
$INCLUDED ?? false or die;

const EVENT_STATUS_CONFIRMED = 0;
const EVENT_STATUS_CANCELLED = 1;
const EVENT_STATUS_UNSURE = 2;
const EVENT_STATUS_CONFIRMED_NOT_HAPPENING = 3;

function canEditRegularEvent($eventId) {
	return userHasPrivilege("eventMgmt");
}

function currentUserIsRegularEventOp($row) {
	return userHasPrivilege("eventMgmt") || currentUserMeetsSemicolonUsersGroupStr($row["ops"]);
}

function currentUserIsEventOp($event, $regularEvent) {
	if ($regularEvent && currentUserIsRegularEventOp($regularEvent))
		return true;

	return userHasPrivilege("eventMgmt") || currentUserMeetsSemicolonUsersGroupStr($event["ops"]);
}

function currentUserIsRegularEventOp_id($regEventId) {
	$q = dbQuery("SELECT ops FROM regularEvents WHERE ( id = ? ) AND ( NOT deleted )", $regEventId);
	if (!$q->rowCount())
		return false;

	$row = $q->fetch();
	return userHasPrivilege("eventMgmt") || currentUserMeetsSemicolonUsersGroupStr($row["ops"]);
}

function getEventData($identification) {
	$data = explode(";", $identification);

	$event = null;
	$schedule = null;
	$regularEvent = null;
	if (count($data) == 1) {
		$event = dbQueryRowDef("SELECT * FROM events WHERE ( NOT deleted ) AND ( id = ? ) LIMIT 1", null, $data[0]);
		if (!$event)
			return "Událost neexistuje.";

	} elseif ($data[0] == "fc" && count($data) == 3) {
		if ($data[1]) {
			$regularEvent = dbQueryRowDef("SELECT * FROM regularEvents WHERE ( NOT deleted ) AND ( id = ? ) LIMIT 1", null, $data[1]);

			if (!$regularEvent)
				return "Neplatná data (odkaz na neplatnou pravidelnou událost)";

			if (!currentUserIsRegularEventOp($regularEvent))
				return "Nedostatečná oprávnění";

		} elseif (!userHasPrivilege("eventCreating"))
			return "Nedostatečná oprávnění";

		$event = [
			"id" => null,
			"regularEvent" => $data[1],
			"commonId" => null,
			"schedule" => null,
			"start" => $data[2],
			"end" => $data[2],
			"regularStart" => null,
			"attributesVersion" => time(),
			"name" => null,
			"subheading" => null,
			"ops" => ";u" . loggedUserId() . ";",
			"status" => 0,
			"rawNotes" => null,
			"htmlNotes" => null,
		];

	} elseif ($data[0] == "r" && count($data) == 3) {
		$schedule = dbQueryRowDef("SELECT * FROM regularEventsSchedules WHERE ( NOT deleted ) AND ( id = ? ) LIMIT 1", null, $data[1]);
		$regularStart = $data[2];

		if (!$schedule)
			return "Neplatná data (režim neexistuje).";

		if (!is_numeric($data[2]))
			return "Neplatná data (nečíselný regularStart).";

		if (regimeNextEvent($schedule, $regularStart) != $regularStart)
			return "Neplatná data (nesedí do režimu)";

		$event = dbQueryRowDef("SELECT * FROM events WHERE ( NOT deleted ) AND ( regularEvent = ? ) AND ( regularStart = ? ) AND ( schedule IS NOT NULL ) LIMIT 1", null, $schedule["event"], $regularStart);
		if (is_null($event))
			$event = eventFromSchedule($schedule, $regularStart);

	} else
		return "Neplatná data.";

	if (!$schedule && $event["schedule"]) {
		$schedule = dbQueryRowDef("SELECT * FROM regularEventsSchedules WHERE ( NOT deleted ) AND ( id = ? ) LIMIT 1", null, $event["schedule"]);
		if (!$schedule)
			return "Vnitřní chyba databáze (odkaz na neplatný režim)";
	}

	if (!$regularEvent && $event["regularEvent"]) {
		$regularEvent = dbQueryRowDef("SELECT * FROM regularEvents WHERE ( NOT deleted ) AND ( id = ? ) LIMIT 1", null, $event["regularEvent"]);
		if (!$regularEvent)
			return "Vnitřní chyba databáze (odkaz na neplatnou pravidelnou událost)";
	}

	return [
		"event" => $event,
		"schedule" => $schedule,
		"regularEvent" => $regularEvent
	];
}

function createEventRegularAttributes($eventId, $regularEventId, $attributesVersion) {
	dbExec("
			INSERT INTO eventsRegularAttributes
			SELECT NULL AS id, ? AS event, regularEventsAttributes.id AS attribute, NULL AS value
			FROM regularEventsAttributes
			INNER JOIN regularEventsAttributesHistory ON ( regularEventsAttributes.id = regularEventsAttributesHistory.id ) AND ( validSince <= ? ) AND ( ( validUntil > ? ) OR ( validUntil IS NULL ) )
			WHERE ( regularEvent = ? )
		", $eventId, $attributesVersion, $attributesVersion, $regularEventId);
}

// Returns event id, does not check if the user can create the event
function getOrCreateEvent($identification) {
	global $DB;
	$data = explode(";", $identification);

	if (count($data) == 1)
		return dbQueryValueDef("SELECT id FROM events WHERE ( NOT deleted ) AND ( id = ? ) LIMIT 1", "Událost neexistuje.", $data[0]);

	elseif ($data[0] == "fc" && count($data) == 3) {
		$attributesVersion = time();
		$id = dbInsert("INSERT INTO events( regularEvent, start, end, attributesVersion ) VALUES( ?, ?, ?, ? )", $data[1] ? $data[1] : null, $data[2], $data[2], $attributesVersion);
		logAction("+event", "event", $id);
		if ($data[1])
			createEventRegularAttributes($id, $data[1], $attributesVersion);
		return $id;

	} elseif ($data[0] == "r" && count($data) == 3) {
		$schedule = dbQueryRowDef("SELECT * FROM regularEventsSchedules WHERE ( NOT deleted ) AND ( id = ? ) LIMIT 1", null, $data[1]);
		$regularStart = $data[2];

		if (!$schedule)
			return "Neplatná data (režim neexistuje).";

		if (!is_numeric($data[2]))
			return "Neplatná data (nečíselný regularStart).";

		if (regimeNextEvent($schedule, $regularStart) != $regularStart)
			return "Neplatná data (nesedí do režimu)";

		$DB->beginTransaction();
		$id = dbQueryValueDef("SELECT id FROM events WHERE ( NOT deleted ) AND ( regularEvent = ? ) AND ( regularStart = ? ) AND ( schedule IS NOT NULL ) LIMIT 1", null, $schedule["event"], $regularStart);
		if (!$id) {
			$ev = eventFromSchedule($schedule, $regularStart, false);
			$id = dbInsertRow("events", $ev);
			createEventRegularAttributes($id, $ev["regularEvent"], $ev["attributesVersion"]);
		}

		$DB->commit();
		return $id;

	} else
		return "Neplatná data.";
}

function getOrCreateEventData($identification) {
	$eventData = getEventData($identification);
	if (is_string($eventData))
		return $eventData;

	$id = getOrCreateEvent($identification);

	if (!is_numeric($id))
		return $id;

	$eventData["event"]["id"] = $id;
	return $eventData;
}

function regularEventsCurrentUserIsOpOf() {
	global $_regularEventsCurrentUserIsOpOfResult;

	if (isset($_regularEventsCurrentUserIsOpOfResult))
		return $_regularEventsCurrentUserIsOpOfResult;

	$q = dbQuery("SELECT * FROM regularEvents WHERE NOT deleted ORDER BY name");
	$result = [];

	while ($r = $q->fetch()) {
		if (currentUserIsRegularEventOp($r))
			$result[] = $r;
	}

	$_regularEventsCurrentUserIsOpOfResult = $result;
	return $result;
}

function eventFromSchedule($schedule, $regularStart, $includeTime = true) {
	$result = [
		"id" => null,
		"regularEvent" => $schedule["event"],
		"commonId" => null,
		"schedule" => $schedule["id"],
		"start" => $regularStart,
		"end" => is_null($schedule["timeUntil"]) ? $regularStart : strtotime(sprintf("%+d", $schedule["timeUntil"] - $schedule["timeSince"]) . " minutes +{$schedule["days"]} days", $regularStart),
		"regularStart" => $regularStart,
		"attributesVersion" => $regularStart,
		"name" => null,
		"subheading" => null,
		"ops" => null,
		"rawNotes" => null,
		"htmlNotes" => null,
		"status" => $schedule["defaultStatus"]
	];

	if ($includeTime)
		$result["time"] = $regularStart;

	return $result;
}

function eventEditForm($event, $regularEvent, $schedule, $attributes) {
	if (!isset($event["startDate"]) && !isset($event["endDate"]) && !isset($event["startTime"]) && !isset($event["endTime"])) {
		$event["startDate"] = strtotime("midnight", $event["start"]);
		$event["endDate"] = strtotime("midnight", $event["end"] ?? $event["start"]);
		$event["startTime"] = floor(($event["start"] - $event["startDate"]) / 60);
		$event["endTime"] = floor((($event["end"] ?? $event["start"]) - $event["endDate"]) / 60);

		if (!$event["startTime"])
			$event["startTime"] = null;

		if (!$event["endTime"]) {
			$event["endTime"] = null;
			$event["endDate"] = strtotime("-1 days", $event["endDate"]);
		}

		if ($event["startDate"] == $event["endDate"])
			$event["endDate"] = null;

		if (!$event["end"] || $event["start"] == $event["end"]) {
			$event["endDate"] = null;
			$event["endTime"] = null;

		} elseif (strtotime("midnight", $event["start"]) == strtotime("midnight", $event["end"])) {
			$event["endDate"] = null;
		}
	}

	$form = getForm("editEvent", [
		"values" => $event
	]);

	if ($regularEvent)
		unset($form["elements"]["name"]);

	if ($schedule) {
		unset($form["elements"]["regularStart"]);
		unset($form["elements"]["impl-rooms"]);
	}

	{
		$elements = [];

		foreach ($attributes as $attr) {
			$attrData = eventAttributeData($attr["type"]);
			$elements = array_merge($elements, $attrData["editElements"]($attr));
			$form["values"] = array_merge($form["values"], $attrData["fromValueTransformer"]($attr["deducedValue"], $attr["uniqueId"]));

			if ($attr["commonId"] == "rooms")
				unset($form["elements"]["impl-rooms"]);
		}

		$form["elements"] = array_slice($form["elements"], 0, count($form["elements"]) - 2) + $elements + array_slice($form["elements"], count($form["elements"]) - 2);
	}

	return $form;
}