<?php
	$INCLUDED ?? false or die;
	
	global $PRIVILEGE_DESCS;
	
	$DATA = [
			"elements" => [
					"id" => [ "type" => "hidden" ],
					"displayName" => [
							"type" => "line",
							"label" => "Jméno"
					],
					"email" => [
							"type" => "email",
							"label" => "E-mail"
					],
					"password" => [
							"type" => "password",
							"label" => "Heslo (prázdné = beze změny)",
					],
					"privileges" => [
							"type" => "checkboxArray",
							"items" => $PRIVILEGE_DESCS,
							"numericKeysAsGroups" => true,
							"htmlBefore" => "<div class='field'><div class=\"ui styled fluid accordion\"><div class=\"title\"><i class=\"legal icon\"></i>Privilegia</div><div class=\"content\">",
							"htmlAfter" => "</div></div></div>"
					],
					[
							"type" => "submit",
							"label" => "Uložit změny"
					],
			],
			"autocomplete" => false,
			"privileges" => "userMgmt",
			"action" => function( $data ) {
				global $USER_PRIVILEGES_ARRAY;
				
				if( dbQuery( "SELECT id FROM users WHERE ( email = ? ) AND ( id <> ? ) AND ( banState <> 'deleted' )", $data["email"], $data["id"] )->rowCount() )
					return [ "error" => "Uživatel se zadaným e-mailem už existuje", "errorElement" => "email" ];
				
				if( !userHasPrivileges( "privilegeElevation" ) && array_diff( explode( ";", $data["privileges"] ), $USER_PRIVILEGES_ARRAY ) )
					return [ "error" => "Nemůžete přiřazovat privilegia, která sám nemáte", "errorElement" => "privileges" ];
				
				if( !isValidEmail( $data["email"] ) )
					return [ "error" => "Nevalidní e-mail", "errorElement" => "email" ];
				
				if( !$data["id"] && !$data["password"] )
					$data["password"] = generatePasswordAndSendEmail( $data["email"], true );
				
				$secureData = dbSecureFieldsArray( $data, [ "displayName", "email", "privileges", "password" => $data["password"] != "" ] );
				if( isset( $secureData["password"] ) )
					$secureData["password"] = password_hash( $secureData["password"], PASSWORD_DEFAULT );
				
				if( $data["id"] ) {
					dbUpdateRow( "users", $data["id"], $secureData );
					logAction( "!user", "user", $data["id"] );
					
					return [ "succMessage" => "Uživatel změněn" ];
				} else {
					$secureData["registered"] = time();
					
					$id = dbInsertRow( "users", $secureData );
					logAction( "+user", "user", $id );
					
					return [ "succMessage" => "Uživatel vytvořen", "resultData" => $id ];
				}
			}
	];