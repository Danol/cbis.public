<?php
	$INCLUDED ?? false or die;
	
	require_once "$DATA_PATH/config/events.php";
	
	$DATA = [
			"elements" => [
					"id" => [ "type" => "hidden" ]
			],
			"privileges" => "login",
			"action" => function( $data ) {
				$attribute = dbQueryRowDef( "SELECT id, regularEvent FROM regularEventsAttributes WHERE ( id = ? )", null, $data["id"] );
				if( !$attribute )
					return "Záznam neexistuje.";
				
				$regularEvent = dbQueryRow( "SELECT id, ops FROM regularEvents WHERE ( NOT deleted ) AND ( id = ? )", $attribute["regularEvent"] );
				if( !currentUserIsRegularEventOp( $regularEvent ) )
					return "Nedostatečná oprávnění";
				
				dbQuery( "UPDATE regularEventsAttributesHistory SET validUntil = ? WHERE ( id = ? ) AND ( validUntil IS NULL )", time(), $attribute["id"] );
				dbExec( "
						DELETE FROM eventsRegularAttributes
						WHERE ( attribute = ? ) AND ( id IN (
							SELECT id
							FROM events
							WHERE ( regularEvent = ? ) AND ( attributesVersion >= ? )
						) )
						", $data["id"], $data["regularEvent"], time() );
				
				logAction( "-regEventAttr", "regEventAttr", $attribute["id"] );
				
				return [ "succMessage" => "Atribut smazán." ];
			}
	];