<?php
	$INCLUDED ?? false or die;
	
	global $EVENT_ATTRIBUTE_ICONS;
	require_once "$DATA_PATH/config/events.php";
	require_once "$DATA_PATH/config/eventAttributes.php";
	require_once "$DATA_PATH/config/icons.php";
	
	$DATA = [
			"elements" => [
					"id" => [
							"type" => "hidden"
					],
					"regularEvent" => [
							"type" => "hidden"
					],
					"type" => [
							"type" => "hidden"
					],
					"name" => [
							"type" => "line",
							"label" => "Název"
					],
					"commonId" => [
							"type" => "line",
							"label" => "Identifikátor (pokud nevíte, o co jde, neměňte)"
					],
					"icon" => [
							"type" => "select",
							"label" => "Symbol",
							"items" => iconsAssocArray()
					],
					"visibility" => [
							"type" => "select",
							"label" => "Zobrazovat atribut",
							"items" => [
									"Nikde",
									"Pouze v detailu události",
									"I v přehledu události",
									"I v kalendáři",
									"I v tištěném kalendáři"
							],
							"defaultValue" => 2
					],
					"visibleTo" => [
							"type" => "multiSearchSelect",
							"label" => "Atribut je viditelný pro",
							"items" => usersGroupsIconAssocArray(),
							"defaultValue" => ";a;",
							"valueTransformers" => ":semicolonWrapArray"
					]
			],
			"privileges" => "login",
			"action" => function( $data ) {
				global $DB;
				
				$regularEvent = dbQueryRowDef( "SELECT id, ops FROM regularEvents WHERE ( NOT deleted ) AND ( id = ? )", null, $data["regularEvent"] );
				if( !currentUserIsRegularEventOp( $regularEvent ) )
					return "Nedostatečná oprávnění";
				
				$isInsert = !$data["id"];
				if( $isInsert ) {
					$data["id"] = dbInsert( "INSERT INTO regularEventsAttributes( regularEvent, type ) VALUES( ?, ? )", $data["regularEvent"], $data["type"] );
					
				} else {
					$attribute = dbQueryRowDef( "SELECT regularEvent, type FROM regularEventsAttributes WHERE id = ?", null, $data["id"] );
					if( !$attribute )
						return "Záznam neexistuje";
					
					if( $attribute["regularEvent"] != $data["regularEvent"] )
						return "Neplatná data (neshoduje se rehgularEvent)";
					
					if( $attribute["type"] != $data["type"] )
						return "Neplatná data (neshoduje se type)";
				}
				
				$secureData = dbSecureFieldsArray( $data, [ "id", "name", "icon", "commonId", "visibility", "visibleTo" ] );
				
				// We cannot use just $data, because the current form is missing some dynamically-generated elements (those elements data would not show up)
				$form = regularEventAttributeForm( $data["type"], [] );
				$formData = getFormPostData( $form );
				$now = time();
				
				$secureData["params"] = $form["toParamsTransformer"]( $formData );
				$secureData["defaultValue"] = $form["toValueTransformer"]( $formData );
				$secureData["validSince"] = $now;
				
				$DB->beginTransaction();
				dbExec( "UPDATE regularEventsAttributesHistory SET validUntil = ? WHERE ( id = ? ) AND ( validUntil IS NULL )", $secureData["validSince"], $data["id"] );
				dbInsertRow( "regularEventsAttributesHistory", $secureData );
				$DB->commit();
				
				if( $isInsert )
					dbExec( "
						INSERT INTO eventsRegularAttributes
						SELECT NULL AS id, events.id AS event, ? AS attribute, NULL AS value
						FROM events
						WHERE ( regularEvent = ? ) AND ( attributesVersion >= ? )
						", $data["id"], $data["regularEvent"], $now );
				
				logAction( $isInsert ? "+regEventAttr" : "!regEventAttr", "regEventAttr", $data["id"] );
				
				return [ "succMessage" => "Atribut " . ($isInsert ? "vytvořen" : "upraven"), "resultData" => $data["id"] ];
			}
	];