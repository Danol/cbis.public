<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"id" => [ "type" => "hidden" ]
			],
			"privileges" => "groupMgmt",
			"action" => function( $data ) {
				$q = dbQuery( "SELECT name FROM groups WHERE ( id = ? ) AND ( NOT deleted )", $data["id"] );
				if( $q->rowCount() == 0 )
					return "Zadaná skupina neexistuje.";
				
				$r = $q->fetch();
				
				dbQuery( "UPDATE groups SET deleted = TRUE WHERE id = ?", $data["id"] );
				dbQuery( "DELETE FROM associations WHERE ( type IN ('groupMember','groupOp') ) AND ( arg1 = ? )", $data["id"] );
				//dbQuery( "DELETE FROM associations WHERE ( type IN ('') ) AND ( arg2 = ? )", $data["id"] );
				logAction( "-group", "group", $data["id"] );
				
				return [ "succMessage" => "Skupina '{$r["name"]}' smazána" ];
			}
	];