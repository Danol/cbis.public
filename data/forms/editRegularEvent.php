<?php
	$INCLUDED ?? false or die;
	
	global $PRIVILEGE_DESCS;
	require_once "$DATA_PATH/config/events.php";
	
	$DATA = [
			"elements" => [
				"id" => [ "type" => "hidden" ],
				"name" => [
						"type" => "line",
						"label" => "Název"
				],
				"commonId" => [
					"type" => "line",
					"label" => "Identifikátor (pokud nevíte, o co jde, neměňte)"
				],
				"ops" => [
						"type" => "multiSearchSelect",
						"label" => "Správci",
						"items" => usersGroupsIconAssocArray(),
						"valueTransformers" => ":semicolonWrapArray"
				],

				"<div class=\"ui two fields\">",
				"defaultStartTime" => [
					"type" => "time",
					"label" => "Výchozí čas počátku",
					"allowNull" => true,
				],
				"defaultEndTime" => [
						"type" => "time",
						"label" => "Výchozí čas konce",
						"allowNull" => true
				],
				"</div>",

				[
						"type" => "submit",
						"label" => "Uložit změny"
				]
			],
			"privileges" => "login",
			"action" => function( $data ) {
				$secureData = dbSecureFieldsArray( $data, [ "name", "commonId", "ops", "defaultStartTime", "defaultEndTime" ] );
				
				if( $data["id"] ) {
					if( !canEditRegularEvent( $data["id"] ) )
						return "Nedostatečná oprávnění";
					
					dbUpdateRow( "regularEvents", $data["id"], $secureData );
					logAction( "!regEvent", "regularEvent", $data["id"] );
					
					return [ "succMessage" => "Událost upravena" ];
					
				} else {
					if( !userHasPrivileges( "eventCreating" ) )
						return "Nedostatečná oprávnění";
					
					$secureData["created"] = time();
					
					$id = dbInsertRow( "regularEvents", $secureData );
					logAction( "+regEvent", "regularEvent", $id );
					
					return [ "succMessage" => "Událost vytvořena", "resultData" => $id ];
				}
			}
	];