<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"id" => [ "type" => "hidden" ],
					"text" => [
							"type" => "textarea",
							"label" => "Text <div class='ui left pointing tiny label'>Podporuje notaci <a href=\"http://www.edgering.org/markdown/\" target=\"_blank\">Markdown</a>.</div>",
							"htmlAfter" => "<div class='ui field'>" . markdownHelp() . "</div>"
					],
					"submit" => [ "type" => "submit", "label" => "Upravit text" ]
			],
			"privileges" => "login",
			"action" => function( $data ) {
				if( !userCanEditText( $data["id"] ) )
					return "Nedostatečná oprávnění";
				
				$data["text"] = trim( $data["text"] );
				$text = formatText( $data["text"] );
				
				dbQuery( "REPLACE INTO texts( id, rawText, formattedText ) VALUES( ?, ?, ? )", $data["id"], $data["text"], $text );
				return [ "succMessage" => "Text upraven." ];
			}
	];