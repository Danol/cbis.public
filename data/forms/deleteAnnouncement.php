<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"id" => [ "type" => "hidden" ],
			],
			"privileges" => "login",
			"action" => function( $data ) {
				$q = dbQuery( "SELECT sectionType, sectionId FROM announcements WHERE id = ?", $data["id"] );
				if( !$q->rowCount() )
					return "Oznámení neexistuje";
				
				$r = $q->fetch();
				if( !canManageAnnouncements( $r["sectionType"], $r["sectionId"] ) )
					return "Nedostatečná oprávnění";
				
				dbQuery( "DELETE FROM announcements WHERE id = ?", $data["id"] );
				logAction( "-annment", "announcement", $data["id"] );
				
				return [ "succMessage" => "Oznámení smazáno" ];
			}
	];