<?php
$INCLUDED ?? false or die;

global $PRIVILEGE_DESCS;
require_once "$DATA_PATH/config/events.php";
require_once "$DATA_PATH/config/rooms.php";
require_once "$DATA_PATH/config/eventAttributes.php";

$DATA = [
	"elements" => [
		"id" => ["type" => "hidden"],
		"name" => [
			"type" => "line",
			"label" => "Název"
		],
		"subheading" => [
			"type" => "line",
			"label" => "Podtitul"
		],

		"<div class=\"ui fields\">",
		"startDate" => [
			"type" => "date",
			"label" => "Začátek",
			"fieldClasses" => "five wide",
			"moreClasses" => "startDate"
		],
		"startTime" => [
			"type" => "time",
			"label" => "&nbsp;",
			"fieldClasses" => "three wide",
			"placeholder" => "Čas",
			"allowNull" => true,
		],
		"endDate" => [
			"type" => "date",
			"label" => "Konec",
			"allowNull" => true,
			"fieldClasses" => "five wide",
			"moreClasses" => "endDate"
		],
		"endTime" => [
			"type" => "time",
			"label" => "&nbsp;",
			"fieldClasses" => "three wide",
			"placeholder" => "Čas",
			"allowNull" => true
		],
		"</div>",

		"regularStart" => [
			"type" => "datetime",
			"label" => "Plánovaný začátek <div class='ui left pointing tiny label'>Nastavení tohoto pole označí událost jako přesunutou ze zadaného data</div>",
			"placeholder" => "Plánovaný začátek",
			"allowNull" => true
		],

		"ops" => [
			"type" => "multiSearchSelect",
			"label" => "Správci <div class='ui left pointing tiny label'>Dodatečně ke správcům z pravidelné události</div>",
			"placeholder" => "Správci",
			"items" => usersGroupsIconAssocArray(),
			"valueTransformers" => ":semicolonWrapArray"
		],
		"status" => [
			"type" => "select",
			"label" => "Stav",
			"items" => [
				0 => "<span style='color: green;'><i class='check icon'></i> Uskuteční se</span>",
				1 => "<span style='color: red;'><i class='remove icon'></i> Zrušeno</span>",
				2 => "<span style='color: gray;'><i class='question icon'></i> Nepotvrzeno</span>",
				3 => "<span style='color: gray;'><i class='remove icon'></i> Neuskuteční se</span>"
			],
			"defaultValue" => 0
		],

		"impl-rooms" => [
			"type" => "multiSearchSelect",
			"label" => "Místnost",
			"items" => roomsAssocArray(),
			"valueTransformers" => ":semicolonWrapArray"
		],

		"rawNotes" => [
			"type" => "textarea",
			//"label" => "Poznámky"
			"label" => "Poznámky <div class='ui left pointing tiny label'>Podporuje notaci <a href=\"http://www.edgering.org/markdown/\" target=\"_blank\">Markdown</a></div>",
			"htmlAfter" => "<div class='ui field'>" . markdownHelp() . "</div>"
		],

		"submit" => [
			"type" => "submit",
			"label" => "Uložit změny"
		]
	],
	"privileges" => "login",
	"action" => function ($data) {
		$eventData = getOrCreateEventData($data["id"] ?? null);
		if (is_string($eventData))
			return $eventData;

		$event = $eventData["event"];
		$regularEvent = $eventData["regularEvent"];
		$schedule = $eventData["schedule"];
		$eventId = $event["id"];

		if (!currentUserIsEventOp($event, $regularEvent))
			return "Nemáte oprávnění pro úpravu/vytvoření události.";

		$data["start"] = $data["startDate"] + ($data["startTime"] ?? 0) * 60;

		if (isset($data["endDate"]) && isset($data["endTime"]))
			$data["end"] = $data["endDate"] + $data["endTime"] * 60;
		elseif (isset($data["endDate"]))
			$data["end"] = strtotime("+1 days", $data["endDate"]);
		elseif (isset($data["endTime"]))
			$data["end"] = $data["startDate"] + $data["endTime"] * 60;
		else
			$data["end"] = $data["start"];

		$secureData = dbSecureFieldsArray($data, ["name" => !$regularEvent, "regularStart" => !$schedule, "start", "end", "ops", "status", "rawNotes", "subheading"]);

		if (($secureData["name"] ?? "") == "")
			$secureData["name"] = null;

		if ($secureData["end"] < $secureData["start"])
			return ["error" => "Konec události nemůže být dříve jak začátek", "errorElement" => "endTime"];
		
		if (strlen($secureData["subheading"]) > 40)
			return ["error" => "Podtitul je příliš dlouhý", "errorElement" => "subheading"];

		$secureData["htmlNotes"] = formatText($data["rawNotes"]);
		$attributes = eventAttributes($event, $regularEvent);

		// We cannot use just $data, because the current form is missing some dynamically-generated elements (those elements data would not show up)
		$form = eventEditForm($event, $regularEvent, $schedule, $attributes);
		$formData = getFormPostData($form);

		foreach ($attributes as $attr) {
			$newValue = eventAttributeData($attr["type"])["toValueTransformer"]($formData, $attr["uniqueId"]);
			if (!is_null($attr["value"]) || $attr["defaultValue"] != $newValue)
				dbQuery("UPDATE {$attr["editTable"]} SET value = ? WHERE id = ?", $newValue, $attr["recId"]);
		}

		if ($formData["impl-rooms"] != ";;" && $formData["impl-rooms"] != ";" && $formData["impl-rooms"] != "") {
			dbInsertRow("eventsAttributes", ["event" => $eventId, "commonId" => "rooms", "icon" => "building", "type" => "rooms", "name" => "Místnost", "params" => "", "value" => $data["impl-rooms"], "visibility" => 2, "visibleTo" => ";a;"]);
		}

		dbUpdateRow("events", $eventId, $secureData);
		logAction("!event", "event", $eventId);

		return ["succMessage" => "Událost vytvořena/upravena", "resultData" => $eventId];
	}
];