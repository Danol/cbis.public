<?php
	$INCLUDED ?? false or die;
	
	require_once "$DATA_PATH/config/events.php";
	
	$DATA = [
			"elements" => [
					"id" => [ "type" => "hidden" ]
			],
			"privileges" => "login",
			"action" => function( $data ) {
				$q = dbQuery( "SELECT name FROM regularEvents WHERE ( id = ? ) AND ( NOT deleted )", $data["id"] );
				if( $q->rowCount() == 0 )
					return "Zadaná událost neexistuje.";
				
				if( !canEditRegularEvent( $data["id"] ) )
					return "Nedostatečná oprávnění";
				
				$r = $q->fetch();
				
				dbQuery( "UPDATE regularEventsSchedules SET deleted = deleted | 2 WHERE event = ?", $data["id"] );
				dbQuery( "UPDATE events SET deleted = deleted | 3 WHERE regularEvent = ?", $data["id"] );
				dbQuery( "UPDATE regularEvents SET deleted = TRUE WHERE id = ?", $data["id"] );
				logAction( "-regEvent", "regularEvent", $data["id"] );
				
				return [ "succMessage" => "Pravidelná událost '{$r["name"]}' smazána" ];
			}
	];