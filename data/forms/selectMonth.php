<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"getField" => [
							"type" => "hidden"
					],
					"redirect" => [
							"type" => "hidden"
					],
					"link" => [
							"type" => "hidden",
							"defaultValue" => parse_url( $_SERVER["REQUEST_URI"], PHP_URL_FRAGMENT )
					],
					"month" => [
							"type" => "monthSelectButton",
							"label" => "Vybrat měsíc",
							"defaultValue" => time()
					],
			],
			"inline" => true,
			"getField" => "month",
			"action" => function( $data ) {
				if( $data["redirect"] ?? null )
					return [ "redirect" => str_replace( "%month%", $data["month"], $data["redirect"] ) ];
				else
					return [ "redirect" => currentLevelUrl( [ $data["getField"] => $data["month"] ], $data["link"] ) ];
			},
	];