<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"showPseudonymInPrayerSupport" => [
							"type" => "checkbox",
							"classes" => "toggle",
							"label" => "Vystupovat pod pseudonymem"
					],
					"prayerSupportPseudonym" => [
							"type" => "line",
							"label" => "Pseudonym",
					],
					"submit" => [
							"type" => "submit",
							"label" => "Uložit"
					]
			],
			"privileges" => "login",
			"action" => function( $data ) {
				$secureData = dbSecureFieldsArray( $data, [ "showPseudonymInPrayerSupport", "prayerSupportPseudonym" ] );
				if( $secureData["showPseudonymInPrayerSupport"] && strlen( $secureData["prayerSupportPseudonym"] ) == 0 )
					return [ "error" => "Prosím, vyplňte pseudonym, pod kterým chcete v této sekci vystupovat.", "errorElement" => "prayerSupportPseudonym" ];
				
				if( $secureData["showPseudonymInPrayerSupport"] && dbQueryValue( "SELECT COUNT(*) FROM users WHERE ( showPseudonymInPrayerSupport ) AND ( prayerSupportPseudonym = ? ) AND ( id <> ? )", $secureData["prayerSupportPseudonym"], loggedUserId() ) )
					return [ "error" => "Tento pseudonym už využívá někdo jiný.", "errorElement" => "prayerSupportPseudonym" ];
				
				dbUpdateRow( "users", loggedUserId(), $secureData );
			}
	];