<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"id" => [ "type" => "hidden" ],
			],
			"privileges" => "login",
			"action" => function( $data ) {
				$q = dbQuery( "SELECT topicType, topicId, user FROM comments WHERE ( id = ? ) AND ( NOT deleted )", $data["id"] );
				if( !$q->rowCount() )
					return "Komentář neexistuje";
				
				$r = $q->fetch();
				if( $r["user"] != loggedUserId() && !canModerateComments( $r["topicType"], $r["topicId"] ) )
					return "Nedostatečná oprávnění";
				
				dbQuery( "UPDATE comments SET deleted = TRUE WHERE id = ?", $data["id"] );
				logAction( "-comment", "comment", $data["id"] );
				
				return [ "succMessage" => "Komentář smazán" ];
			}
	];