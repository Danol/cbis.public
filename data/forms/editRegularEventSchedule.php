<?php
	$INCLUDED ?? false or die;
	
	global $PRIVILEGE_DESCS;
	require_once "$DATA_PATH/config/events.php";
	
	$DATA = [
			"elements" => [
					"event" => [ "type" => "hidden" ],
					"id" => [ "type" => "hidden" ],
					
					"regime" => [
							"type" => "searchSelect",
							"label" => "Režim",
							"items" => regimeItemsAssocArray()
					],
					"<div class=\"ui three fields\">",
					"timeSince" => [
							"type" => "time",
							"label" => "Čas od",
							"allowNull" => true
					],
					"timeUntil" => [
							"type" => "time",
							"label" => "Čas do",
							"allowNull" => true
					],
					"days" => [
							"type" => "line",
							"label" => "Trvání dní",
							"defaultValue" => "0",
							"defaultValueTransformers" => "number",
							"minValue" => 0,
							"maxValue" => 100
					],
					"</div>",
					
					"defaultStatus" => [
						"type" => "select",
						"label" => "Výchozí stav události",
						"defaultValue" => "0",
						"items" => [
							0 => "<span style='color: green;'><i class='check icon'></i> Uskuteční se</span>",
							2 => "<span style='color: gray;'><i class='question icon'></i> Nepotvrzeno</span>",
						]
					],
					
					"<div class=\"ui two fields\">",
					
					"since" => [
							"type" => "date",
							"label" => "Platí od"
					],
					
					"until" => [
							"type" => "date",
							"label" => "Platí do",
							"allowNull" => true
					],
					
					"</div>",
					
					"submit" => [
							"type" => "submit",
							"label" => "Přidat"
					]
			],
			"privileges" => "login",
			"action" => function( $data ) {
				if( $data["id"] ?? null ) {
					$schedule = dbQueryRowDef( "SELECT * FROM regularEventsSchedules WHERE ( NOT deleted ) AND ( id = ? ) AND ( ( until IS NULL ) OR ( until > ? ) )", null, $data["id"], time() );
					if( !$schedule )
						return "Režim neexistuje nebo je již uzavřený";
					
					$newSince = regimeNextEvent( $schedule, time() );
					
					$data["event"] = $schedule["event"];
					$data["since"] = $newSince;
					
					dbExec( "UPDATE events SET deleted = deleted | 2 WHERE ( SCHEDULE = ? ) AND ( regularStart >= ? )", $schedule["id"], $newSince );
					
					if( $schedule["since"] > $newSince ) {
						logAction( "-regEvSchedule", "regEvSchedule", $schedule["id"] );
						dbExec( "UPDATE regularEventsSchedules SET deleted = deleted | 1 WHERE id = ?", $schedule["id"] );
					} else {
						logAction( "!regEvSchedule", "regEvSchedule", $schedule["id"] );
						dbExec( "UPDATE regularEventsSchedules SET until = ? WHERE id = ?", $data["since"], $schedule["id"] );
					}
				}
				
				$secureData = dbSecureFieldsArray( $data, [ "event", "regime", "timeSince", "timeUntil", "since", "until", "days", "defaultStatus" ] );
				
				if( !currentUserIsRegularEventOp_id( $data["event"] ) )
					return "Nedostatečná oprávnění";
				
				if( $data["since"] < strtotime( "midnight", time() ) )
					return [ "error" => "'Platnost od' nesmí být dříve jak aktuální datum", "errorElement" => "since" ];
				
				if( !is_null( $data["until"] ) && $data["until"] < $data["since"] )
					return [ "error" => "'Platnost do' musí být větší než 'platnost od'", "errorElement" => "until" ];
				
				if( !is_null( $data["timeUntil"] ) && is_null( $data["timeSince"] ) )
					return [ "error" => "Pokud je nastaven 'čas do', musíte nastavit i 'čas od'", "errorElement" => "timeSince" ];
				
				if( !is_null( $data["timeUntil"] ) && !is_null( $data["timeSince"] ) && ($data["timeUntil"] + $data["days"] * 3600 * 24) < $data["timeSince"] )
					return [ "error" => "'Čas do' nemůže být před 'časem od'.", "errorElement" => "timeSince" ];
				
				$id = dbInsertRow( "regularEventsSchedules", $secureData );
				logAction( "+regEvSchedule", "regEvSchedule", $id );
				
				return [ "succMessage" => "Položka vytvořena", "resultData" => $id ];
			}
	];