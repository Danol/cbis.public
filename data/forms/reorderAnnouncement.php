<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"id" => [ "type" => "hidden" ],
					"up" => [
							"type" => "submit",
							"classes" => "tiny gray compact icon",
							"label" => "<i class='caret up icon'></i>"
					],
					"down" => [
							"type" => "submit",
							"classes" => "tiny gray compact icon",
							"label" => "<i class='caret down icon'></i>"
					]
			],
			"inline" => true,
			"privileges" => "login",
			"action" => function( $data ) {
				if( $data["id"] ) {
					$q = dbQuery( "SELECT sectionType, sectionId, orderId FROM announcements WHERE id = ?", $data["id"] );
					if( !$q->rowCount() )
						return "Oznámení neexistuje";
					
					$r = $q->fetch();
				}
				
				if( !canManageAnnouncements( $r["sectionType"], $r["sectionId"] ) )
					return "Nedostatečná oprávnění";
				
				$oid1 = $r["orderId"];
				$oid2 = dbQuery( "SELECT orderId FROM announcements WHERE " . (!is_null( $data["up"] ) ? "orderId < ? ORDER BY orderId DESC" : "orderId > ? ORDER BY orderId ASC") . " LIMIT 1", $oid1 );
				
				if( !$oid2->rowCount() )
					return;
				
				$oid2 = $oid2->fetch()[0];
				
				dbQuery( "UPDATE announcements SET orderId = ? WHERE orderId = ?", $oid1, $oid2 );
				dbQuery( "UPDATE announcements SET orderId = ? WHERE id = ?", $oid2, $data["id"] );
			}
	];