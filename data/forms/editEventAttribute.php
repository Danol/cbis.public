<?php
	$INCLUDED ?? false or die;
	
	global $EVENT_ATTRIBUTE_ICONS;
	require_once "$DATA_PATH/config/events.php";
	require_once "$DATA_PATH/config/eventAttributes.php";
	require_once "$DATA_PATH/config/icons.php";
	
	$DATA = [
			"elements" => [
					"id" => [
							"type" => "hidden"
					],
					"event" => [
							"type" => "hidden"
					],
					"type" => [
							"type" => "hidden"
					],
					"name" => [
							"type" => "line",
							"label" => "Název"
					],
					"commonId" => [
							"type" => "line",
							"label" => "Identifikátor (pokud nevíte, o co jde, neměňte)"
					],
					"icon" => [
							"type" => "select",
							"label" => "Symbol",
							"items" => iconsAssocArray()
					],
					"visibility" => [
							"type" => "select",
							"label" => "Zobrazovat atribut",
							"items" => [
									"Nikde",
									"Pouze v detailu události",
									"I v přehledu události",
									"I v kalendáři",
									"I v tištěném kalendáři"
							],
							"defaultValue" => 2
					],
					"visibleTo" => [
							"type" => "multiSearchSelect",
							"label" => "Atribut je viditelný pro",
							"items" => usersGroupsIconAssocArray(),
							"defaultValue" => ";a;",
							"valueTransformers" => ":semicolonWrapArray"
					]
			],
			"privileges" => "login",
			"action" => function( $data ) {
				$eventData = getOrCreateEventData( $data["event"] ?? null );
				if( is_string( $eventData ) )
					return $eventData;
				
				$event = $eventData["event"];
				$eventId = $event["id"];
				
				if( !currentUserIsEventOp( $event, $eventData["regularEvent"] ) )
					return "Nemáte oprávnění pro úpravu/vytvoření události.";
				
				// We cannot use just $data, because the current form is missing some dynamically-generated elements (those elements data would not show up)
				$form = eventAttributeForm( $data["type"], [] );
				$formData = getFormPostData( $form );
				
				$isInsert = !$data["id"];
				$secureData = dbSecureFieldsArray( $data, [ "name", "icon", "commonId", "type", "visibility", "visibleTo" ] );
				$secureData["params"] = $form["toParamsTransformer"]( $formData );
				$secureData["value"] = $form["toValueTransformer"]( $formData );
				
				if( $isInsert ) {
					$secureData["event"] = $eventId;
					$data["id"] = dbInsertRow( "eventsAttributes", $secureData );
					
				} else {
					$attribute = dbQueryRowDef( "SELECT event, type FROM eventsAttributes WHERE id = ?", null, $data["id"] );
					if( !$attribute )
						return "Záznam neexistuje";
					
					if( $attribute["event"] != $data["event"] )
						return "Neplatná dat (neshoduje se rehgularEvent)";
					
					if( $attribute["type"] != $data["type"] )
						return "Neplatná dat (neshoduje se type)";
					
					dbUpdateRow( "eventsAttributes", $data["id"], $secureData );
				}
				
				logAction( $isInsert ? "+eventAttr" : "!eventAttr", "eventAttr", $data["id"] );
				return [ "succMessage" => "Atribut " . ($isInsert ? "vytvořen" : "upraven"), "resultData" => $data["id"] ];
			}
	];