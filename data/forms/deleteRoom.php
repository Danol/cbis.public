<?php
	$INCLUDED ?? false or die;
	
	global $SECTION_ASSOC_ARRAY;
	
	$DATA = [
			"elements" => [
					"id" => [ "type" => "hidden" ]
			],
			"privileges" => "roomMgmt",
			"action" => function( $data ) {
				dbQuery( "DELETE FROM rooms WHERE id = ?", $data["id"] );
				logAction( "-room", "room", $data["id"] );
				return [ "succMessage" => "Místnost smazána." ];
			}
	];