<?php
	$INCLUDED ?? false or die;
	
	require_once "$DATA_PATH/config/events.php";
	
	$DATA = [
			"elements" => [
					"id" => [ "type" => "hidden" ]
			],
			"privileges" => "login",
			"action" => function( $data ) {
				$attribute = dbQueryRowDef( "SELECT id, event FROM eventsAttributes WHERE ( id = ? )", null, $data["id"] );
				if( !$attribute )
					return "Záznam neexistuje.";
				
				$event = dbQueryRow( "SELECT id, regularEvent, ops FROM events WHERE ( NOT deleted ) AND ( id = ? )", $attribute["event"] );
				$regularEvent = dbQueryRowDef( "SELECT id, ops FROM regularEvents WHERE ( NOT deleted ) AND ( id = ? )", null, $event["regularEvent"] );
				if( !currentUserIsEventOp( $event, $regularEvent ) )
					return "Nedostatečná oprávnění";
				
				dbExec( "DELETE FROM eventsAttributes WHERE id = ?", $attribute["id"] );
				logAction( "-eventAttr", "eventAttr", $attribute["id"] );
				
				return [ "succMessage" => "Atribut smazán." ];
			}
	];