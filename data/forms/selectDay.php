<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"getField" => [
							"type" => "hidden"
					],
					"link" => [
							"type" => "hidden",
							"defaultValue" => parse_url( $_SERVER["REQUEST_URI"], PHP_URL_FRAGMENT )
					],
					"day" => [
							"type" => "daySelectButton",
							"label" => "Vybrat den"
					],
			],
			"inline" => true,
			"action" => function( $data ) {
				return [ "redirect" => currentLevelUrl( [ $data["getField"] => $data["day"] ], $data["link"] ) ];
			},
	];