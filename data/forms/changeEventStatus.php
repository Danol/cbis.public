<?php
	$INCLUDED ?? false or die;
	require_once "$DATA_PATH/config/events.php";
	
	$DATA = [
			"elements" => [
					"event" => [ "type" => "hidden" ],

					"<span class='ui right pointing basic label'>Změna stavu </span>",					
					"confirm" => [
						"type" => "submit",
						"classes" => "mini compact green",
						"label" => "<i class='check icon'></i> Uskuteční se"
					],
					"decline" => [
						"type" => "submit",
						"classes" => "mini compact grey",
						"label" => "<i class='delete icon'></i> Neuskuteční se"
					],
					"cancel" => [
						"type" => "submit",
						"classes" => "mini compact red",
						"label" => "<i class='delete icon'></i> Zrušit"
					]
			],
			"privileges" => "login",
			"action" => function( $data ) {
				$eventData = getOrCreateEventData( $data["event"] );
				if( is_string( $eventData ) )
					return $eventData;
				
				$event = $eventData["event"];
				$regularEvent = $eventData["regularEvent"];

				if( !currentUserIsEventOp( $event, $regularEvent ) )
					return "Nemáte oprávnění pro úpravu události.";

				$status = 0;
				if( $data["confirm"] )
					$status = 0;
				elseif( $data["decline"] )
					$status = 3;
				elseif( $data["cancel"] )
					$status = 1;
				
				dbQuery( "UPDATE events SET status = ? WHERE id = ?", $status, $event["id"] );
				logAction( "!eventStatus", "event", $event["id"] );
				
				return [ "succMessage" => "Stav události upraven." ];
			}
	];