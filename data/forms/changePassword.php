<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"origPassword" => [ "type" => "password", "label" => "Akutální heslo" ],
					"newPassword1" => [ "type" => "password", "label" => "Nové heslo", "refillOnSend" => true ],
					"newPassword2" => [ "type" => "password", "label" => "Nové heslo (znovu)" ],
					[ "type" => "submit", "label" => "Změnit heslo" ]
			],
			"privileges" => "login",
			"action" => function( $data ) {
				global $USER_DATA;
				
				$pwd = dbQuery( "SELECT password FROM users WHERE id = ?", $USER_DATA["id"] )->fetch()["password"];
				if( !password_verify( $data["origPassword"], $pwd ) )
					return [ "error" => "Nesprávné původní heslo.", "errorElement" => "origPassword" ];
				
				if( strlen( $data["newPassword1"] ) < 6 )
					return [ "error" => "Nové heslo musí mít alespoň 6 znaků.", "errorElement" => "newPassword1" ];
				
				if( strlen( $data["newPassword1"] ) > 64 )
					return [ "error" => "Nové heslo musí být kratší jak 64 znaků.", "errorElement" => "newPassword1" ];
				
				if( $data["newPassword1"] != $data["newPassword2"] )
					return [ "error" => "Nová hesla se neshodují.", "errorElement" => "newPassword2" ];
				
				dbQuery( "UPDATE users SET password = ? WHERE id = ?", password_hash( $data["newPassword1"], PASSWORD_DEFAULT ), $USER_DATA["id"] );
				logAction( "!pwd" );
				
				return [ "succMessage" => "Heslo změneno" ];
			}
	];