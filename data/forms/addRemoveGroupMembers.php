<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"htmlInsideBegin" => "<div class='three fields'>",
			"htmlInsideEnd" => "</div>",
			"elements" => [
					"groupId" => [
							"type" => "hidden"
					],
					"users" => [
							"type" => "multiSearchSelect",
							"items" => usersAssocArray()
					],
					"add" => [
							"type" => "submit",
							"label" => "<i class='plus icon'></i>Přidat",
							"classes" => "basic green"
					],
					"remove" => [
							"type" => "submit",
							"label" => "<i class='ban icon'></i>Odebrat",
							"classes" => "basic red"
					],
			],
			"action" => function( $data ) {
				if( !is_array( $data["users"] ) || count( $data["users"] ) == 0 )
					return [ "error" => "Je třeba vybrat uživatele", "errorElement" => "user" ];
				
				if( !currentUserIsGroupOperator( $data["groupId"] ) )
					return "Nejste operátorem skupiny.";
				
				if( !isUserIdArray( $data["users"] ) )
					return "Neplatné pole uživatelů";
				
				foreach( $data["users"] as $user ) {
					if( $data["remove"] ) {
						if( !dbQuery( "DELETE FROM associations WHERE ( type IN ('groupOp','groupMember' ) ) AND ( arg1 = ? ) AND ( arg2 = ? )", $data["groupId"], $user )->rowCount() )
							continue;
						addNotification( $user, "-groupMem", $data["groupId"] );
						logAction( "-groupMem", "user", $user, [ "group" => $data["groupId"] ] );
						
					} else {
						if( !dbQuery( "INSERT IGNORE INTO associations( type, arg1, arg2 ) VALUES( 'groupMember', ?, ? )", $data["groupId"], $user )->rowCount() )
							continue;
						
						addNotification( $user, "+groupMem", $data["groupId"] );
						logAction( "+groupMem", "user", $user, [ "group" => $data["groupId"] ] );
					}
				}
			},
			"privileges" => "login"
	];