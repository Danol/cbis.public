<?php
	$INCLUDED ?? false or die;
	require_once "$DATA_PATH/config/events.php";
	require_once "$DATA_PATH/config/eventAttributes.php";
	
	$DATA = [
			"elements" => [
					"event" => [ "type" => "hidden" ],
					"attribute" => [ "type" => "hidden" ],
					[
							"type" => "submit",
							"label" => "<i class='remove icon'></i>Odhlásit se",
							"classes" => "tiny orange compact"
					]
			],
			"inline" => true,
			"privileges" => "login",
			"action" => function( $data ) {
				$eventData = getOrCreateEventData( $data["event"] );
				if( is_string( $eventData ) )
					return $eventData;
				
				$event = $eventData["event"];
				$regularEvent = $eventData["regularEvent"];
				$attributes = eventAttributes( $event, $regularEvent );
				
				foreach( $attributes as $attr ) {
					if( $attr["uniqueId"] == $data["attribute"] && $attr["type"] == "usersRegister" ) {
						$attributeData = eventAttributeData( $attr["type"] );
						if( !$attributeData["canCurrentUserChange"]( $attr, false ) )
							return "Nemožno se odhlásit";
						
						$val = arrayToSemicolonStr( array_diff( usersSemicolonStrToArray( $attr["deducedValue"] ), [ loggedUserId() ] ) );
						dbExec( "UPDATE {$attr["editTable"]} SET value = ? WHERE id = ?", $val, $attr["recId"] );
						return [
								"succMessage" => "Odhlášen z '{$attr["name"]}' na akci '" . ($event["name"] ?? $regularEvent["name"]) . "' " . eventDateToString( $event["start"], $event["end"] ),
								"resultData" => $event["id"]
						];
					}
				}
				
				return "Atribut neexistuje";
			}
	];