<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"id" => [ "type" => "hidden" ]
			],
			"privileges" => "userMgmt",
			"action" => function( $data ) {
				$q = dbQuery( "SELECT displayName FROM users WHERE ( id = ? ) AND ( banState = 'none' )", $data["id"] );
				if( $q->rowCount() == 0 )
					return "Zadaný uživatel neexistuje.";
				
				$r = $q->fetch();
				
				dbQuery( "DELETE FROM associations WHERE ( type IN ('groupMember','groupOp') ) AND ( arg2 = ? )", $data["id"] );
				dbQuery( "UPDATE users SET banState = 'deleted' WHERE id = ?", $data["id"] );
				
				logAction( "-user", "user", $data["id"] );
				
				return [ "succMessage" => "Uživatel {$r["displayName"]} smazán" ];
			}
	];