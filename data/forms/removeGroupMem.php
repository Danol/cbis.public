<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"user" => [ "type" => "hidden" ],
					"group" => [ "type" => "hidden" ]
			],
			"action" => function( $data ) {
				if( !currentUserIsGroupOperator( $data["group"] ) )
					return "Nedostatečná privilegia.";
				
				if( !dbQuery( "DELETE FROM associations WHERE ( ( type = 'groupOp' ) OR ( type = 'groupMember' ) ) AND ( arg1 = ? ) AND ( arg2 = ? )", $data["group"], $data["user"] ) )
					return "Uživatel v zadané skupině není";
				
				addNotification( $data["user"], "-groupMem", $data["group"] );
				logAction( "-groupMem", "user", $data["user"], [ "group" => $data["group"] ] );
			},
			"privileges" => "login"
	];