<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"id" => [ "type" => "hidden" ],
					"sectionType" => [ "type" => "hidden" ],
					"sectionId" => [ "type" => "hidden" ],
					"title" => [ "type" => "line", "label" => "Nadpis" ],
					"text" => [
							"type" => "textarea",
							"label" => "Text <div class='ui left pointing tiny label'>Podporuje notaci <a href=\"http://www.edgering.org/markdown/\" target=\"_blank\">Markdown</a>.</div>",
							"htmlAfter" => "<div class='ui field'>" . markdownHelp() . "</div>"
					],
					"submit" => [ "type" => "submit", "label" => "Upravit oznámení" ]
			],
			"privileges" => "login",
			"action" => function( $data ) {
				if( $data["id"] ) {
					$q = dbQuery( "SELECT sectionType, sectionId FROM announcements WHERE id = ?", $data["id"] );
					if( !$q->rowCount() )
						return "Oznámení neexistuje";
					
					$r = $q->fetch();
					$data["sectionType"] = $r["sectionType"];
					$data["sectionId"] = $r["sectionId"];
				}
				
				if( !canManageAnnouncements( $data["sectionType"], $data["sectionId"] ) )
					return "Nedostatečná oprávnění";
				
				if( strlen( $data["title"] ) == 0 )
					return [ "error" => "Vyplňte prosím nadpis", "errorElement" => "title" ];
				
				//if( strlen( $data["text"] ) > 250 )
				//	return [ "error" => "Text může obsahovat maximálně 250 znaků", "errorElement" => "text" ];
				
				$text = formatText( $data["text"] );
				
				if( !$data["id"] ) {
					$orderId = dbQueryValue("SELECT IFNULL(MAX(orderId),0) FROM announcements WHERE ( sectionType = ? ) AND ( sectionId = ? )", $data["sectionType"], $data["sectionId"]) + 1;
					
					dbQuery(
							"INSERT INTO announcements ( lastEdit, user, title, rawText, formattedText, sectionType, sectionId, orderId ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )",
							time(), loggedUserId(), $data["title"], $data["text"], $text, $data["sectionType"], $data["sectionId"], $orderId
					);
					logAction( "+annment", "announcement", dbLastInsertId(), [ "sectionType" => $data["sectionType"], "sectionId" => $data["sectionId"] ] );
					return [ "succMessage" => "Oznámení přidáno" ];
					
				} else {
					dbQuery(
							"UPDATE announcements SET lastEdit = ?, user = ?, title = ?, rawText = ?, formattedText = ? WHERE id = ?",
							time(), loggedUserId(), $data["title"], $data["text"], $text, $data["id"]
					);
					logAction( "!annment", "announcement", dbLastInsertId() );
					return [ "succMessage" => "Oznámení upraveno" ];
				}
			}
	];