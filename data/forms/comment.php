<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"topicType" => [ "type" => "hidden" ],
					"topicId" => [ "type" => "hidden" ],
					"replyTo" => [ "type" => "hidden" ],
					"deepReplyTo" => [ "type" => "hidden" ],
					"text" => [
							"type" => "textarea",
						//"label" => "Zpráva <div class='ui left pointing tiny label'>Podporuje notaci <a href=\"http://www.edgering.org/markdown/\" target=\"_blank\">Markdown</a>.</div>"
							"label" => "Zpráva <div class='ui left pointing tiny label'>Podporuje notaci <a href=\"http://www.edgering.org/markdown/\" target=\"_blank\">Markdown</a>.</div>",
							"htmlAfter" => "<div class='ui field'>" . markdownHelp() . "</div>"
					],
					"submit" => [ "type" => "submit", "label" => "Přidat komentář" ]
			],
			"privileges" => "login",
			"action" => function( $data ) {
				if( !canComment( $data["topicType"], $data["topicId"] ) )
					return "Nedostatečná oprávnění";
				
				$data["text"] = trim( $data["text"] );
				if( strlen( $data["text"] ) == 0 )
					return [ "error" => "Vyplňte prosím tělo zprávy", "errorElement" => "text" ];
				
				if( strlen( $data["text"] ) > 250 )
					return [ "error" => "Zpráva může obsahovat maximálně 250 znaků", "errorElement" => "text" ];
				
				if( $data["replyTo"] ) {
					$replyCommentQuery = dbQuery( "SELECT user FROM comments WHERE ( comments.id = ? ) AND ( topicType = ? ) AND ( topicId = ? ) AND ( replyTo IS NULL ) AND ( NOT deleted )", $data["replyTo"], $data["topicType"], $data["topicId"] );
					if( !$replyCommentQuery->rowCount() )
						return "Neplatný příspěvek na odpovídání";
					
					$deepReplyCommentQuery = dbQuery( "SELECT user FROM comments WHERE ( comments.id = ? ) AND ( topicType = ? ) AND ( topicId = ? ) AND ( NOT deleted )", $data["deepReplyTo"], $data["topicType"], $data["topicId"] );
					if( !$deepReplyCommentQuery->rowCount() )
						return "Neplatný příspěvek na odpovídání";
					
					$deepReplyCommentData = $deepReplyCommentQuery->fetch();
				}
				
				$text = formatText( $data["text"] );
				$newCommentId = dbInsert(
						"INSERT INTO comments ( topicType, topicId, replyTo, deepReplyTo, user, timeSent, rawText, formattedText ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )",
						$data["topicType"], $data["topicId"], $data["replyTo"] ? $data["replyTo"] : null, $data["deepReplyTo"] ? $data["deepReplyTo"] : null, loggedUserId(), time(), $data["text"], $text
				);
				
				if( $data["replyTo"] && $deepReplyCommentData["user"] != loggedUserId() )
					addNotification( $deepReplyCommentData["user"], "commentReply", $newCommentId );
				
				return [ "succMessage" => "Komentář přidán" ];
			}
	];