<?php
	$INCLUDED ?? false or die;
	
	global $PRIVILEGE_DESCS;
	
	$DATA = [
			"elements" => [
					"userId" => [ "type" => "hidden" ],
			],
			"autocomplete" => false,
			"privileges" => "userMgmt",
			"action" => function( $data ) {
				global $USER_PRIVILEGES_ARRAY;
				
				$q = dbQuery( "SELECT id, email FROM users WHERE ( id = ? ) AND ( banState <> 'deleted' )", $data["userId"] );
				if( !$q->rowCount() )
					return "Uživatel neexistuje";
				
				$r = $q->fetch();
				$pwd = generatePasswordAndSendEmail( $r["email"], false );
				dbQuery( "UPDATE users SET password = ? WHERE id = ?", password_hash( $pwd, PASSWORD_DEFAULT ), $r["id"] );
				
				return [ "succMessage" => "Heslo změněno, uživateli byl zaslán e-mail s novým heslem." ];
			}
	];