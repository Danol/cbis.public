<?php
$INCLUDED ?? false or die;

require_once "$DATA_PATH/config/events.php";

$DATA = [
	"elements" => [
		"id" => ["type" => "hidden"]
	],
	"privileges" => "login",
	"action" => function ($data) {
		$event = dbQueryRowDef("SELECT * FROM events WHERE ( id = ? ) AND ( NOT deleted )", null, $data["id"]);
		if (!$event)
			return "Zadaná událost neexistuje.";

		$regularEvent = $event["regularEvent"] ? dbQueryRow("SELECT * FROM regularEvents WHERE ( id = ? ) AND ( NOT deleted )", $event["regularEvent"]) : null;

		if (!currentUserIsEventOp($event, $regularEvent))
			return "Nemáte oprávnění pro úpravu události.";

		if ($event["schedule"])
			return "Nelze smazat událost řízenou rozvrhem (pouze zrušit). Tato chyba by se neměla zobrazovat, nahlašte, prosím, chybu.";

		dbQuery("UPDATE events SET deleted = ( deleted | 1 ) WHERE id = ?", $event["id"]);
		logAction("-event", "event", $event["id"]);

		return ["succMessage" => "Událost smazána."];
	}
];