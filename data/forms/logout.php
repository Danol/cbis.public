<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [],
			"privileges" => "login",
			"action" => function( $data ) {
				logout();
				return [ "succMessage" => "Odhlášen" ];
			},
			"redirect" => ""
	];