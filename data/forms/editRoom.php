<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"id" => [ "type" => "hidden" ],
					"name" => [
							"type" => "line",
							"label" => "Název"
					],
					"shortName" => [
							"type" => "line",
							"label" => "Zkratka"
					],
					[
							"type" => "submit",
							"label" => "Uložit změny"
					],
			],
			"privileges" => "roomMgmt",
			"action" => function( $data ) {
				$secureData = dbSecureFieldsArray( $data, [ "name", "shortName" ] );
				
				if( $data["id"] ) {
					dbUpdateRow( "rooms", $data["id"], $secureData );
					logAction( "!room", "room", $data["id"] );
					return [ "succMessage" => "Místnost upravena" ];
					
				} else {
					$id = dbInsertRow( "rooms", $secureData );
					logAction( "+room", "room", $id );
					return [ "succMessage" => "Místnost vytvořena", "resultData" => $id ];
				}
			}
	];