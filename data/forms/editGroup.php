<?php
	$INCLUDED ?? false or die;
	
	require_once "$DATA_PATH/config/icons.php";
	
	global $PRIVILEGE_DESCS;
	
	$DATA = [
			"elements" => [
					"id" => [ "type" => "hidden" ],
					"name" => [
							"type" => "line",
							"label" => "Název"
					],
					"icon" => [
							"label" => "Ikona",
							"type" => "select",
							"items" => iconsAssocArray(),
							"defaultValue" => "users"
					],
					"publicMembership" => [
							"type" => "checkbox",
							"label" => "Členství ve skupině je veřejné"
					],
					"opPrivileges" => [
							"type" => "checkboxArray",
							"items" => $PRIVILEGE_DESCS,
							"numericKeysAsGroups" => true,
							"htmlBefore" => "<div class='field'><div class=\"ui styled fluid accordion\"><div class=\"title\"><i class=\"spy icon\"></i>Privilegia správců</div><div class=\"content\">",
							"htmlAfter" => "</div></div></div>"
					],
					"privileges" => [
							"type" => "checkboxArray",
							"items" => $PRIVILEGE_DESCS,
							"numericKeysAsGroups" => true,
							"htmlBefore" => "<div class='field'><div class=\"ui styled fluid accordion\"><div class=\"title\"><i class=\"legal icon\"></i>Privilegia členů</div><div class=\"content\">",
							"htmlAfter" => "</div></div></div>"
					],
					[
							"type" => "submit",
							"label" => "Uložit změny"
					],
			],
			"privileges" => "groupMgmt",
			"action" => function( $data ) {
				global $USER_PRIVILEGES_ARRAY;
				
				if( !userHasPrivileges( "privilegeElevation" ) && array_diff( explode( ";", $data["privileges"] ), $USER_PRIVILEGES_ARRAY ) )
					return [ "error" => "Nemůžete přiřazovat privilegia, která sám nemáte", "errorElement" => "privileges" ];
				
				if( !userHasPrivileges( "privilegeElevation" ) && array_diff( explode( ";", $data["opPrivileges"] ), $USER_PRIVILEGES_ARRAY ) )
					return [ "error" => "Nemůžete přiřazovat privilegia, která sám nemáte", "errorElement" => "opPrivileges" ];
				
				$secureData = dbSecureFieldsArray( $data, [ "name", "privileges", "opPrivileges", "publicMembership", "icon" ] );
				
				if( $data["id"] ) {
					dbUpdateRow( "groups", $data["id"], $secureData );
					logAction( "!group", "group", $data["id"] );
					
					return [ "succMessage" => "Skupina změněna" ];
				} else {
					$id = dbInsertRow( "groups", $secureData );
					logAction( "+group", "group", $id );
					
					return [ "succMessage" => "Skupina vytvořena", "resultData" => $id ];
				}
			}
	];