<?php
	$INCLUDED ?? false or die;
	require_once "$DATA_PATH/config/events.php";
	require_once "$DATA_PATH/config/eventAttributes.php";
	
	$DATA = [
			"elements" => [
					"event" => [ "type" => "hidden" ],
					"attribute" => [ "type" => "hidden" ],
					[
							"type" => "submit",
							"label" => "<i class='add icon'></i>Přihlásit se",
							"classes" => "tiny green compact"
					]
			],
			"inline" => true,
			"privileges" => "login",
			"action" => function( $data ) {
				$eventData = getOrCreateEventData( $data["event"] );
				if( is_string( $eventData ) )
					return $eventData;
				
				$event = $eventData["event"];
				$regularEvent = $eventData["regularEvent"];
				$attributes = eventAttributes( $event, $regularEvent );
				
				foreach( $attributes as $attr ) {
					if( $attr["uniqueId"] == $data["attribute"] && $attr["type"] == "userRegisters" ) {
						$attributeData = eventAttributeData( $attr["type"] );
						if( !$attributeData["canCurrentUserChange"]( $attr, true ) )
							return "Nemožno se přihlásit";
						
						dbExec( "UPDATE {$attr["editTable"]} SET value = ? WHERE id = ?", loggedUserId(), $attr["recId"] );
						return [
								"succMessage" => "Přihlášen jako '{$attr["name"]}' na akci '" . ($event["name"] ?? $regularEvent["name"]) . "' " . eventDateToString( $event["start"], $event["end"] ),
								"resultData" => $event["id"]
						];
					}
				}
				
				return "Atribut neexistuje";
			}
	];