<?php
	$INCLUDED ?? false or die;
	
	require_once "$DATA_PATH/config/prayerSupport.php";
	
	$DATA = [
			"elements" => [
					"week" => [ "type" => "hidden" ],
					"data" => [ "type" => "hidden" ]
			],
			"privileges" => "prayerSupport",
			"action" => function( $data ) {
				global $PRAYER_SUPPORT_FUTURE_WEEK_LIMIT, $PRAYER_SUPPORT_CALENDAR_EXTRA_DAYS;
				
				$week = weekStartTime( $data["week"] );
				$weekEnd = modifiedDate( $week, "+" . (7 + $PRAYER_SUPPORT_CALENDAR_EXTRA_DAYS) . " days" );
				$weekStart = modifiedDate( $week, "-$PRAYER_SUPPORT_CALENDAR_EXTRA_DAYS days" );
				
				$thisWeek = weekStartTime( time() );
				
				if( $week < $thisWeek || $week > modifiedDate( $thisWeek, "+$PRAYER_SUPPORT_FUTURE_WEEK_LIMIT weeks" ) )
					return "Lze upravovat pouze $PRAYER_SUPPORT_FUTURE_WEEK_LIMIT týdnů dopředu";
				
				dbQuery( "DELETE FROM prayerSupportRecords WHERE ( user = ? ) AND ( startTime < ? ) AND ( endTime > ? )", loggedUserId(), $weekEnd, max( $thisWeek, $weekStart ) );
				
				if( !$data["data"] )
					return [ "succMessage" => "Rozvrh uložen" ];
				
				$cdata = explode( ";", $data["data"] );
				$resData = [];
				
				$prev = -($PRAYER_SUPPORT_CALENDAR_EXTRA_DAYS * 24 + 1);
				$start = -$prev;
				foreach( $cdata as $rec ) {
					if( !is_numeric( $rec ) || $rec < -($PRAYER_SUPPORT_CALENDAR_EXTRA_DAYS * 24 + 1) || $rec >= (7 + $PRAYER_SUPPORT_CALENDAR_EXTRA_DAYS) * 24 + 1 || $rec <= $prev || floor( $rec * 2 ) != $rec * 2 )
						continue;
					
					if( $rec != $prev + 0.5 || floor( $rec / 24 ) != floor( $prev / 24 ) ) {
						if( $start != -1 )
							$resData[] = [ $start, $prev + 0.5 ];
						
						$start = $rec;
					}
					
					$prev = $rec;
				}
				
				if( $start != -1 )
					$resData[] = [ $start, $prev + 0.5 ];
				
				foreach( $resData as $res ) {
					$start = $week + $res[0] * 3600;
					if( $start < $thisWeek )
						continue;
					
					dbQuery( "INSERT INTO prayerSupportRecords ( user, startTime, endTime ) VALUES( ?, ?, ? )", loggedUserId(), $start, $week + $res[1] * 3600 );
				}
				
				return [ "succMessage" => "Rozvrh uložen" ];
			}
	];