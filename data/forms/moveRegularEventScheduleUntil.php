<?php
	$INCLUDED ?? false or die;
	
	global $PRIVILEGE_DESCS;
	require_once "$DATA_PATH/config/events.php";
	
	$DATA = [
			"elements" => [
					"id" => [ "type" => "hidden" ],
					"<div class='ui yellow message'>Zkrácení platnosti režimu smaže všechny výskyty události daného režimu, které mají datum mimo novou platnost!</div>",
					
					"until" => [
							"type" => "date",
							"label" => "Nový konec platnosti režimu",
							"allowNull" => true
					],
					[
							"type" => "submit",
							"label" => "Posunout"
					]
			],
			"privileges" => "login",
			"action" => function( $data ) {
				$q = dbQuery( "SELECT id, event, since FROM regularEventsSchedules WHERE ( NOT deleted ) AND ( id = ? )", $data["id"] );
				if( !$q->rowCount() )
					return "Režim neexistuje.";
				$r = $q->fetch();
				
				if( !currentUserIsRegularEventOp_id( $r["event"] ) )
					return "Nedostatečná oprávnění";
				
				if( !is_null( $data["until"] ) && $data["until"] < $r["since"] )
					return [ "error" => "'Platnost do' musí být větší než 'platnost od'", "errorElement" => "until" ];
				
				dbQuery( "UPDATE regularEventsSchedules SET until = ? WHERE id = ?", $data["until"], $r["id"] );
				dbQuery( "UPDATE events SET deleted = deleted | 2 WHERE ( schedule = ? ) AND ( start > ? )", $r["id"], $data["until"] );
				
				logAction( "!regEvSchedule", "regEvSchedule", $r["id"] );
				
				return [ "succMessage" => "Platnost posunuta." ];
			}
	];