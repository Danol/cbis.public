<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"email" => [ "type" => "email", "label" => "E-mail" ],
					"password" => [ "type" => "password", "label" => "Heslo" ],
					[ "type" => "submit", "label" => "Přihlásit se" ]
			],
			"action" => function( $data ) {
				$loginResult = tryLogin( $data["email"], $data["password"] );
				if( $loginResult )
					return [
							"invalidEmail" => [ "error" => "Neplatný email", "errorElement" => "email" ],
							"wrongPassword" => [ "error" => "Špatné heslo", "errorElement" => "password" ],
							"accountDeleted" => [ "error" => "Účet byl smazán" ]
					][$loginResult];
				
				return [ "succMessage" => "Přihlášení proběhlo úspěšně" ];
			},
			"redirect" => function() {
				global $URL;
				
				// Redirect only if on login page
				return $URL[count( $URL ) - 1] == "login" ? "" : null;
			}
	];