<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"user" => [
							"type" => "hidden"
					],
					"group" => [
							"type" => "searchSelectButton",
							"icon" => "plus",
							"placeholder" => "Přidat do skupiny",
							"classes" => "green submitOnChange",
							"items" => groupsAssocArray()
					],
			],
			"inline" => true,
			"action" => function( $data ) {
				if( !isUserId( $data["user"] ) )
					return "Neplatné ID uživatele";
				
				if( !isGroupId( $data["group"] ) )
					return "Neplatná skupina";
				
				if( !currentUserIsGroupOperator( $data["group"] ) )
					return "Nejste operátorem skupiny";
				
				if( dbQuery( "INSERT IGNORE INTO associations( type, arg1, arg2 ) VALUES( 'groupMember', ?, ? )", $data["group"], $data["user"] )->rowCount() == 0 )
					return "Uživatel již je ve skupině";
				
				addNotification( $data["user"], "+groupMem", $data["group"] );
				logAction( "+groupMem", "user", $data["user"], [ "group" => $data["group"] ] );
			},
			"privileges" => "login"
	];