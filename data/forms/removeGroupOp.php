<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"elements" => [
					"user" => [ "type" => "hidden" ],
					"group" => [ "type" => "hidden" ]
			],
			"action" => function( $data ) {
				if( !currentUserIsGroupOperator( $data["group"] ) )
					return "Nedostatečná privilegia.";
				
				if(
						!dbQuery( "UPDATE IGNORE associations SET type = 'groupMember' WHERE ( type = 'groupOp' ) AND ( arg1 = ? ) AND ( arg2 = ? )", $data["group"], $data["user"] )->rowCount()
						&& !dbQuery( "DELETE FROM associations WHERE ( type = 'groupOp' ) AND ( arg1 = ? ) AND ( arg2 = ? )", $data["group"], $data["user"] )->rowCount()
				)
					return "Uživatel není operátorem skupiny";
				
				addNotification( $data["user"], "-groupOp", $data["group"] );
				logAction( "-groupOp", "user", $data["user"], [ "group" => $data["group"] ] );
			},
			"privileges" => "login"
	];