<?php
	$INCLUDED ?? false or die;
	
	function showAnnouncements( $sectionType, $sectionId = 0, $additionalData = [] ) {
		$canManage = canManageAnnouncements( $sectionType, $sectionId );
		$q = dbQuery( "SELECT id, title, rawText AS text, formattedText FROM announcements WHERE ( sectionType = ? ) AND ( sectionId = ? ) ORDER BY orderId ASC", $sectionType, $sectionId );
		
		while( $r = $q->fetch() ) {
			echo $additionalData["htmlBeforeItem"] ?? "";
			?>
			<div class="ui message<?= $canManage ? " announcementMenuPopup" : "" ?>" style=" <?= $additionalData["itemStyle"] ?? "" ?>">
				<?php
					if( $canManage ) {
						$modalId = "editAnnouncement-$sectionType-$sectionId-" . $r["id"];
						?>
						<div class="ui modal" id="<?= $modalId ?>">
							<div class="header"><i class="announcement icon"></i>Upravit oznámení</div>
							<div class="content">
								<?= messagesHTML( $modalId ) ?>
								<?= formHtml( "editAnnouncement", [ "modalID" => $modalId, "formAction" => "#", "values" => $r ] ) ?>
							</div>
						</div>
						<?php
					}
					if( $r["title"] )
						echo "<div class=\"header\">" . $r["title"] . "</div>";
					
					echo "<div class='markdown'>{$r["formattedText"]}</div>";
				
				
				?>
				<?= formHTML( "deleteAnnouncement", [ "id" => $sectionType . "-" . $sectionId, "formAction" => "#", "hidden" => true ] ) ?>
			</div>
			<?php
			if( $canManage ) {
				?>
				<div class='ui wide popup'>
					<a class='ui tiny compact blue button' onclick='$("#<?= $modalId ?>").modal("show")'><i class='edit icon'></i>Upravit</a>
					<a class='ui tiny compact red button' onclick='formSubmitConfirm("<?= formCSSID( "deleteAnnouncement", $sectionType . "-" . $sectionId ) ?>", "Smazání oznámení", "Opravdu smazat vybrané oznámení?", {id: "<?= $r["id"] ?>"} )'><i class='delete icon'></i>Smazat</a>
					<?= formHtml( "reorderAnnouncement", [ "values" => [ "id" => $r["id"] ] ] ) ?>
				</div>
				<?php
			}
			
			echo $additionalData["htmlAfterItem"] ?? "";
		}
		
		if( ($additionalData["showAddButton"] ?? true) && canManageAnnouncements( $sectionType, $sectionId ) ) {
			echo formModalHTML( "editAnnouncement", [
							"modalID" => "addAnnouncementModal-$sectionType",
							"formAction" => "#",
							"elements" => [ "submit" => [ "label" => "Přidat oznámení" ] ],
							"values" => [ "sectionType" => $sectionType, "sectionId" => $sectionId ],
							"modalHeader" => "Přidat oznámení"
					]
			);
			echo "<a onclick=\"$('#addAnnouncementModal-$sectionType').modal('show');\" class=\"ui button\"><i class=\"announcement icon\"></i>Přidat oznámení</a>";
		}
	}
	
	require_once "$DATA_PATH/config/announcements.php";