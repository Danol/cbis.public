<?php
	$INCLUDED ?? false or die;
	
	function logAction( $actionType, $subjectType = null, $subjectId = null, $data = [] ) {
		global $USER_DATA;
		
		isLoggedIn() or die( "Not logged in for logging" );
		
		dbQuery( "INSERT INTO log( actorId, timestamp, subjectType, subjectId, actionType, additionalData ) VALUES( ?, ?, ?, ?, ?, ? )",
				$USER_DATA["id"],
				time(),
				$subjectType,
				$subjectId,
				$actionType,
				$data ? json_encode( $data, JSON_FORCE_OBJECT ) : null
		);
	}