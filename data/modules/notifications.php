<?php
	$INCLUDED ?? false or die;
	
	function addNotification( $targetUser, $subjectType, $subjectId, $actorId = null ) {
		if( !$actorId )
			$actorId = loggedUserId();
		
		if( $actorId == $targetUser )
			return;
		
		$q = dbQuery(
				"SELECT id FROM notifications WHERE ( targetUser = ? ) AND ( subjectType = ? ) AND ( subjectId = ? ) AND ( firstTime > ? ) LIMIT 1",
				$targetUser, $subjectType, $subjectId, time() - 6 * 3600 // Aggregate max. six hours old notifications
		);
		if( $q->rowCount() ) {
			$notificationId = $q->fetch()["id"];
			dbQuery( "UPDATE notifications SET lastTime = ?, isRead = FALSE WHERE id = ?", time(), $notificationId );
		} else
			$notificationId = dbInsert(
					"INSERT INTO notifications( targetUser, subjectType, subjectId, firstTime, lastTime ) VALUES( ?, ?, ?, ?, ?)",
					$targetUser, $subjectType, $subjectId, time(), time()
			);
		
		dbQuery( "INSERT IGNORE INTO notificationActors( notificationId, actorId ) VALUES( ?, ? )", $notificationId, $actorId );
	}
	
	function getUnreadNotificationCount() {
		return dbQuery( "SELECT 0 FROM notifications WHERE ( targetUser = ? ) AND ( isRead = FALSE )", loggedUserId() )->rowCount();
	}
	
	function showNotifications() {
		global $NOTIFICATION_TYPES;
		
		$q = dbQuery( "SELECT notifications.id AS notificationId, subjectType, subjectId, users.id AS userId, displayName, lastTime, isRead FROM notifications LEFT JOIN notificationActors ON notificationId = notifications.id LEFT JOIN users ON users.id = actorId WHERE targetUser = ? ORDER BY lastTime DESC, notificationId LIMIT 10", loggedUserId() );
		
		$r = $q->fetch();
		if( !$r )
			echo "<i>Nemáte žádná upozornění.</i>";
		
		echo "<div class='ui small feed'>";
		
		while( $r ) {
			$data = $r;
			$actors = [];
			
			$notificationId = $r["notificationId"];
			do {
				$actors[] = userLink( $r["userId"], $r["displayName"] );
				$r = $q->fetch();
			} while( $r && $r["notificationId"] == $notificationId );
			
			$actorCount = count( $actors );
			$areMoreActors = $actorCount > 1;
			
			if( !isset( $NOTIFICATION_TYPES[$data["subjectType"]] ) )
				continue;
			
			$notData = $NOTIFICATION_TYPES[$data["subjectType"]];
			
			$text = $notData["text"] ?? "";
			if( is_callable( $text ) )
				$text = $text( $data["subjectId"] );
			
			// Remove signular/plural versions
			$text = preg_replace( "/(\\\"?(?:((?<!\\\")\w+(?!\\\")|(?<=\\\")[^\\\"]*(?=\\\")))\\\"?\|\\\"?(?:((?<!\\\")\w+(?!\\\")|(?<=\\\")[^\\\"]*(?=\")))\\\"?)/u", $areMoreActors ? "$3" : "$2", $text );
			
			$text = str_replace( [
					"%actors%",
					"%id%"
			], [
					$areMoreActors ? (implode( ", ", array_slice( $actors, 0, -1 ) ) . " a " . $actors[$actorCount - 1]) : $actors[0],
					$data["subjectId"]
			], $text );
			
			$text = preg_replace_callback( "/%group:([0-9]+)%/", function( $match ) {
				return "<a href='" . subLevelUrl( [ "group", "id" => $match[1] ] ) . "'>" . dbQueryValue( "SELECT name FROM groups WHERE id = ?", $match[1] ) . "</a>";
			}, $text );
			
			$readClass = $data["isRead"] ? " readNotification" : "";
			
			echo "<div class='event$readClass'>" .
					"<div class='label'><i class='{$notData["icon"]} icon'></i></div>" .
					"<div class='content'><div class='date'>" . verbalizedIntervalToNow( $data["lastTime"] ) . "</div><div class='summary'>$text</div></div></div>";
		}
		
		echo "</div>";
	}
	
	function readNotifications() {
		dbQuery( "UPDATE notifications SET isRead = TRUE WHERE targetUser = ?", loggedUserId() );
	}
	
	require_once "$DATA_PATH/config/notifications.php";