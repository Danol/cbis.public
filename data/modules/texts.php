<?php
	function contentText( $textId ) {
		$modalId = "editTextModal-$textId";
		
		echo "<div class='markdown'>";
		
		$q = dbQuery( "SELECT rawText, formattedText FROM texts WHERE id = ?", $textId );
		$r = null;
		if( $q->rowCount() )
			$r = $q->fetch();
		
		if( $r )
			echo $r["formattedText"];
		
		echo "</div>";
		
		if( userCanEditText( $textId ) ) {
			?>
			<div class="ui basic segment">
				<button class="ui right floated tiny compact button" onclick="$('#<?= $modalId ?>').modal('show');"><i class="edit icon"></i>Upravit text</button>
			</div>
			<div class="clearing"></div>

			<div class="ui modal" id="<?= $modalId ?>">
				<div class="header"><i class="edit icon"></i>Úprava textu</div>
				<div class="content">
					<?= messagesHTML( $modalId ) ?>
					<?= formHtml( "editText", [
							"values" => [ "id" => $textId, "text" => $r ? $r["rawText"] : "" ],
							"modalID" => $modalId,
							"formAction" => "#"
					] ) ?>
				</div>
			</div>
			<?php
		}
	}
	
	require_once "$DATA_PATH/config/texts.php";