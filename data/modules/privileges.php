<?php
	$INCLUDED ?? false or die;
	
	function extraPrivilegesAssigning() {
		global $USER_PRIVILEGES_ARRAY, $USER_DATA;
		
		$q = dbQuery( "SELECT privileges FROM associations LEFT JOIN groups ON associations.arg1 = groups.id WHERE ( ( type = 'groupMember' ) OR ( type = 'groupOp' ) ) AND ( arg2 = ? )", $USER_DATA["id"] );
		while( $r = $q->fetch() )
			array_push( $USER_PRIVILEGES_ARRAY, ...explode( ";", $r["privileges"] ) );
		
		$q = dbQuery( "SELECT opPrivileges FROM associations LEFT JOIN groups ON associations.arg1 = groups.id WHERE ( type = 'groupOp' ) AND ( arg2 = ? )", $USER_DATA["id"] );
		while( $r = $q->fetch() )
			array_push( $USER_PRIVILEGES_ARRAY, ...explode( ";", $r["opPrivileges"] ) );
	}
 
