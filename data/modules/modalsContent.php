<?php
	$INCLUDED ?? false or die;
	
	call_user_func( function() {
		global $MODALS_CONTENT;
		$MODALS_CONTENT = "";
	} );
	
	function addModalsContent( $content ) {
		global $MODALS_CONTENT;
		$MODALS_CONTENT .= "\n$content";
	}