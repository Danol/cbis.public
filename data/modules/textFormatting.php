<?php
	$INCLUDED ?? false or die;
	
	function formatSmiles( $text ) {
		$smiles = [
				":-?\\)" => "smile",
				":-?\\(" => "sad",
				":-?P" => "tongue",
				":-?D" => "laugh",
				"xD" => "laugh",
				";-?\\)" => "wink",
				"8-?\\)" => "glasses",
				"8-~" => "confused",
				":'-?\\(" => "cry",
				":-?[|/]" => "neutral"
		];
		foreach( $smiles as $regex => $file )
			$text = preg_replace( "#(?<=[ \\t\\n>]|^)$regex(?=[ .\\n\\t<]|$)#s", "<img src='img/emoticon/$file.png'/>", $text );
		
		return $text;
	}
	
	function formatGeneral( $text ) {
		$text = preg_replace( "/\\b([a-z]) /", "\\1&nbsp;", $text );
		return $text;
	}
	
	function formatText( $text ) {
		$text = preg_replace( "/^ *•/m", "* ", $text );
		
		$text = (new Parsedown())->text( $text );
		$text = formatSmiles( $text );
		$text = formatGeneral( $text );
		
		return $text;
	}
	
	function formatLineText( $text ) {
		$text = (new Parsedown())->line( $text );
		$text = formatSmiles( $text );
		$text = formatGeneral( $text );
		return $text;
	}
	
	function markdownHelp() {
		ob_start();
		?>
		<div class="ui styled fluid accordion">
			<div class="title"><i class="help circle icon"></i>Formátování textu</div>
			<div class="content">
				<table class="ui very basic compact small table">
					<thead>
						<tr>
							<th>Vstup</th>
							<th>Výstup</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>první řádek(mezera)(mezera)<br>druhý řádek</td>
							<td>
								<div class="markdown">
									první řádek<br>druhý řádek
								</div>
							</td>
						</tr>
						<tr>
							<td>první odstavec<br><br>druhý odstavec</td>
							<td>
								<div class="markdown">
									<p>první odstavec</p>
									<p>druhý odstavec</p>
								</div>
							</td>
						</tr>
						<tr>
							<td>**tučné** __tučné__</td>
							<td>
								<div class="markdown"><b>tučné</b> <b>tučné</b></div>
							</td>
						</tr>
						<tr>
							<td>*kurzíva* _kurzíva_</td>
							<td>
								<div class="markdown"><i>kurzíva</i> <i>kurzíva</i></div>
							</td>
						</tr>
						<tr>
							<td>* položka 1<br>* položka 2</td>
							<td>
								<div class="markdown">
									<ul>
										<li>položka 1</li>
										<li>položka 2</li>
									</ul>
								</div>
							</td>
						</tr>
						<tr>
							<td>1. položka 1<br>1. položka 2</td>
							<td>
								<div class="markdown">
									<ol>
										<li>položka 1</li>
										<li>položka 2</li>
									</ol>
								</div>
							</td>
						</tr>
						<tr>
							<td># Nadpis 1</td>
							<td>
								<div class="markdown"><h1>Nadpis 1</h1></div>
							</td>
						</tr>
						<tr>
							<td>## Nadpis 2</td>
							<td>
								<div class="markdown"><h2>Nadpis 2</h2></div>
							</td>
						</tr>
						<tr>
							<td>### Nadpis 3</td>
							<td>
								<div class="markdown"><h3>Nadpis 3</h3></div>
							</td>
						</tr>
						<tr>
							<td>#### Nadpis 4</td>
							<td>
								<div class="markdown"><h4>Nadpis 4</h4></div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<?php
		return ob_get_clean();
	}