<?php
	$INCLUDED ?? false or die;
	
	function monthName( $month ) {
		return [
				null, "leden", "únor", "březen", "duben", "květen", "červen", "červenec", "srpen", "září", "říjen", "listopad", "prosinec"
		][$month];
	}
	
	date_default_timezone_set( "Europe/Prague" );
	
	function weekStartTime( $time ) {
		$time = dayStartTime( $time );
		$d = date_create( "@$time" );
		$d->modify( "-" . (date( "N", $time ) - 1) . " days" );
		return $d->getTimestamp();
	}
	
	function dayStartTime( $time ) {
		$d = date_create( "@$time" );
		$d->modify( "-" . date( "G", $time ) . " hours" ); // GMT correction?
		$d->modify( "-" . date( "i", $time ) . " minutes" ); // GMT correction?
		$d->modify( "-" . date( "s", $time ) . " seconds" ); // GMT correction?
		return $d->getTimestamp();
	}
	
	function diffDates( $time1, $time2 ) {
		return date_create( "@$time1" )->diff( date_create( "@$time2" ) );
	}
	
	function modifiedDate( $time, $modifier ) {
		return date_create( "@$time" )->modify( $modifier )->getTimestamp();
	}
	
	function weekDayShortName( $i ) {
		return [
				null, "po", "út", "st", "čt", "pá", "so", "ne"
		][$i];
	}
	
	function eventDateToString( $startTime, $endTime = null ) {
		$startTimeIsMidnight = ($startTime == strtotime( "midnight", $startTime ));
		
		$result = weekDayShortName( date( "N", $startTime ) ) . date( " j. n.", $startTime );
		if( !$startTimeIsMidnight )
			$result .= date( " G:i", $startTime );
		
		if( $endTime && $endTime != $startTime ) {
			$endTimeIsMidnight = ($endTime == strtotime( "midnight", $endTime ));
			if( $endTimeIsMidnight )
				$endTime -= 1;
			
			$endIsSameDate = date( "j.n.Y", $startTime ) == date( "j.n.Y", $endTime );
			
			if( !$endIsSameDate )
				$result .= " – " . weekDayShortName( date( "N", $endTime ) ) . date( " j. n.", $endTime );
			else
				$result .= "–";
			
			
			if( !$endTimeIsMidnight )
				$result .= ($endIsSameDate ? "" : " ") . date( "G:i", $endTime );
		}
		
		return $result;
	}
	
	function verbalizedIntervalToNow( $time ) {
		$diff = time() - $time;
		
		if( $diff < 0 )
			return "";
		
		if( $diff < 60 )
			return "Před okamžikem";
		
		if( $diff < 120 )
			return "Před minutou";
		
		if( $diff < 3600 )
			return "Před " . round( $diff / 60 ) . " minutami";
		
		if( $diff < 3600 * 2 )
			return "Před hodinou";
		
		if( $diff < 3600 * 13 )
			return "Před " . round( $diff / 3600 ) . " hodinami";
		
		$todayStart = date_create( "@" . time() );
		$todayStart->setTime( 0, 0, 0 );
		$todayStart = $todayStart->getTimestamp();
		if( $time >= $todayStart - 24 * 3600 && $time < $todayStart )
			return "Včera v " . date( "G:i", $time );
		
		if( date( "Y", $time ) != date( "Y" ) )
			return date( "j. n. Y G:i", $time );
		
		return date( "j. n G:i", $time );
	}
	
	// DayTime = secs offset from day start
	function dayTimeToStr( $dayTime ) {
		return sprintf( "%d:%'.02d", $dayTime / 60, $dayTime % 60 );
	}