<?php
require_once "$DATA_PATH/config/events.php";
require_once "$DATA_PATH/config/eventAttributes.php";

function eventsList($startTime, $endTime, $data = []) {
	$eventCountPerDay = [];
	$singleDayEventCountPerDay = [];
	$simpleView = $data["simpleView"] ?? false;
	$excludeCancelled = $data["excludeCancelled"] ?? false;

	if (isset($data["events"])) {
		$eventsFilter = "%id% IN ((?))";
		$eventsFilterArgs = [$data["events"]];
	} else {
		$eventsFilter = "TRUE";
		$eventsFilterArgs = [];
	}

	if (isset($data["eventFilter"])) {
		$filterFunction = $data["eventFilter"];
	} elseif (isset($data["room"])) {
		$room = $data["room"];
		$filterFunction = function ($event, $regularEvent) use ($room) {
			return strpos(eventAttributeValue($event, $regularEvent, "rooms"), ";$room;") !== false;
		};
	} else {
		$filterFunction = function ($event, $regularEvent) {
			return true;
		};
	}

	$regularEvents = [];
	$regularEventsOpCnt = 0;
	$q = dbQuery("SELECT * FROM regularEvents WHERE ( NOT deleted ) AND ( " . str_replace("%id%", "id", $eventsFilter) . ")", ...$eventsFilterArgs);
	while ($r = $q->fetch()) {
		$r["isOp"] = currentUserIsRegularEventOp($r);
		$regularEvents[$r["id"]] = $r;

		if ($r["isOp"])
			$regularEventsOpCnt++;
	}

	$events = [];
	$singleDayEvents = [];
	$multiDayEvents = [];

	// Events lookup
	$eventScheduleAssoc = [];
	$q = dbQuery("
			SELECT *
			FROM events
			WHERE ( NOT deleted ) AND ( ((start <= ?) AND (end > ?)) OR ((regularStart < ?) AND (regularStart >= ?)) ) AND ( " . str_replace("%id%", "regularEvent", $eventsFilter) . ")
			ORDER BY start ASC
		", ...array_merge([$endTime, $startTime, $endTime, $startTime], $eventsFilterArgs));
	while ($r = $q->fetch()) {
		$regularEvent = $regularEvents[$r["regularEvent"]] ?? null;

		if ($r["schedule"]) {
			$eventScheduleAssoc["{$r["regularEvent"]};{$r["regularStart"]}"] = $r;
			$r["uniqueId"] = "{$r["regularEvent"]};{$r["regularStart"]}";
		} else {
			$r["uniqueId"] = $r["id"];
		}

		if ($r["status"] && $excludeCancelled)
			continue;

		if (!$filterFunction($r, $regularEvent))
			continue;

		if ($r["regularEvent"]) {
			$r["name"] = $regularEvent["name"];
			$r["commonId"] = $regularEvent["commonId"];
		}

		$startMidnight = strtotime("midnight", $r["start"]);
		$endMidnight = strtotime("midnight", $r["end"]);

		if ($r["start"] <= $endTime && $r["end"] > $startTime) {
			$r["time"] = $r["start"];
			$events[] = $r;

			if ($startMidnight != $endMidnight || ($r["start"] == $startMidnight && $r["end"] == $r["start"])) {
				$multiDayEvents[] = $r;
			} else {
				$singleDayEvents[] = $r;
				$singleDayEventCountPerDay[$startMidnight] = ($singleDayEventCountPerDay[$startMidnight] ?? 0) + 1;
			}

			$eventCountPerDay[$startMidnight] = ($eventCountPerDay[$startMidnight] ?? 0) + 1;
		}

		// If not simple view and the event has been moved from regular start, add regular start, too
		if (
			!$simpleView
			&& $r["regularStart"]
			&& $startMidnight != strtotime("midnight", $r["regularStart"])
			&& $r["regularStart"] >= $startTime && $r["regularStart"] < $endTime
		) {
			$r["time"] = $r["regularStart"];
			$timeMidnight = strtotime("midnight", $r["time"]);

			$events[] = $r;
			$singleDayEvents[] = $r;

			$singleDayEventCountPerDay[$timeMidnight] = ($singleDayEventCountPerDay[$timeMidnight] ?? 0) + 1;
			$eventCountPerDay[$timeMidnight] = ($eventCountPerDay[$timeMidnight] ?? 0) + 1;
		}
	}

	// Regular event scheduled items
	$q = dbQuery("
			SELECT *
			FROM regularEventsSchedules
			WHERE ( NOT deleted ) AND ( since <= ? ) AND ( ( until > ? ) OR ( until IS NULL ) ) AND ( " . str_replace("%id%", "event", $eventsFilter) . ")",
		...array_merge([$endTime, $startTime], $eventsFilterArgs)
	);
	while ($r = $q->fetch()) {
		$nextTime = regimeNextEvent($r, max($r["since"], strtotime("-{$r["days"]} days", $startTime)));
		$tmp = is_null($r["until"]) ? $endTime : min($r["until"], $endTime);

		$regularEvent = $regularEvents[$r["event"]];

		while (!is_null($nextTime) && $nextTime < $tmp) {
			$time = $nextTime;
			$nextTime = regimeNextEvent($r, $time + 1);

			if (isset($eventScheduleAssoc["{$r["event"]};{$time}"]))
				continue;

			$event = eventFromSchedule($r, $time);

			if ($event["status"] && $excludeCancelled)
				continue;

			if (!$filterFunction($event, $regularEvent))
				continue;

			$event["name"] = $regularEvent["name"];
			$event["commonId"] = $regularEvent["commonId"];
			$event["uniqueId"] = "{$r["event"]};{$time}";

			$startMidnight = strtotime("midnight", $event["start"]);
			$endMidnight = strtotime("midnight", $event["end"]);

			$events[] = $event;

			if ($startMidnight != $endMidnight || ($event["start"] == $startMidnight && $event["end"] == $event["start"])) {
				$multiDayEvents[] = $event;
			} else {
				$singleDayEvents[] = $event;
				$singleDayEventCountPerDay[$startMidnight] = ($singleDayEventCountPerDay[$startMidnight] ?? 0) + 1;
			}

			$eventCountPerDay[$startMidnight] = ($eventCountPerDay[$startMidnight] ?? 0) + 1;
		}
	}

	$sortFunc = function ($a, $b) {
		if ($a["time"] < $b["time"])
			return -1;
		elseif ($a["time"] > $b["time"])
			return 1;

		if ($a["id"] < $b["id"])
			return -1;
		elseif ($a["id"] > $b["id"])
			return 1;

		if ($a["regularEvent"] < $b["regularEvent"])
			return -1;
		elseif ($a["regularEvent"] > $b["regularEvent"])
			return 1;

		else
			return 0;
	};
	usort($events, $sortFunc);
	usort($singleDayEvents, $sortFunc);
	usort($multiDayEvents, $sortFunc);

	return [
		"eventCountPerDay" => $eventCountPerDay,
		"singleDayEventCountPerDay" => $singleDayEventCountPerDay,
		"events" => $events,
		"singleDayEvents" => $singleDayEvents,
		"multiDayEvents" => $multiDayEvents,
		"regularEvents" => $regularEvents,
		"regularEventsOpCnt" => $regularEventsOpCnt // Count of regular events this user is op of
	];
}

function eventOverview($data = []) {
	addJSFile("js/eventOverview.js");

	$monthNames = [
		null, "leden", "únor", "březen", "duben", "květen", "červen", "červenec", "srpen", "září", "říjen", "listopad", "prosinec"
	];
	$dayShorts = [
		null, "po", "út", "st", "čt", "pá", "so", "ne"
	];

	$isCalendar = $data["calendar"] ?? false;
	$isWeekView = $data["weekView"] ?? false;

	$anchorName = $data["anchor"] ?? "eventOverview";
	$timeNow = time();

	if ($isWeekView) {
		$weekNow = strtotime("midnight monday this week");
		$requestedStartTime = strtotime("midnight monday this week", $_GET["week"] ?? $weekNow);
		$requestedEndTime = strtotime("+1 weeks", $requestedStartTime);
	} else {
		$monthNow = strtotime("midnight first day of this month");
		$requestedStartTime = strtotime("midnight first day of this month", $_GET["month"] ?? $monthNow);
		$requestedEndTime = strtotime("+1 months", $requestedStartTime);
	}

	if (isset($data["attributeFilter"]))
		$attributeFilterFunc = $data["attributeFilter"];
	else
		$attributeFilterFunc = function ($attribute, $event) {
			return true;
		};

	$startTime = $isCalendar ? strtotime("-3 days", $requestedStartTime) : $requestedStartTime;
	$endTime = $isCalendar ? strtotime("+3 days", $requestedEndTime) : $requestedEndTime;

	$eventsListResult = eventsList($startTime, $endTime, $data);
	$regularEventsOpCnt = $eventsListResult["regularEventsOpCnt"];
	$regularEvents = $eventsListResult["regularEvents"];
	$eventCountPerDay = $eventsListResult["eventCountPerDay"];

	$extraColumns = $data["extraColumns"] ?? [];

	$randomId = rand(0, 100000);
	?>
	<a name="<?= $anchorName ?>" id="<?= $anchorName ?>"></a>
	<div class="ui inverted blue stackable menu eventOverviewMenu" id="eventOverviewMenu-<?= $randomId ?>" data-context="#eventOverview-<?= $randomId ?>">
		<?php
		if ($isWeekView) {
			?>
			<a href="<?= currentLevelUrl(["week" => strtotime("-1 weeks", $requestedStartTime)], $anchorName) ?>" class="borderless item"><i class="left angle icon"></i> Předchozí týden</a>
			<div class="borderless fitted item">
				<?= formHTML("selectDay", [
					"elements" => [
						"day" => [
							"label" => "
												<i class='dropdown icon'></i>
												<i class='calendar outline icon'></i>
											" . date("j. ", $requestedStartTime) . $monthNames[date("n", $requestedStartTime)] . " &ndash; " . date("j. ", $requestedEndTime - 1) . $monthNames[date("n", $requestedEndTime - 1)],
							"classes" => "compact black noRightMargin"
						]
					],
					"values" => [
						"getField" => "week",
						"link" => $anchorName,
						"day" => $requestedStartTime
					]
				]) ?>
			</div>
			<a href="<?= currentLevelUrl(["week" => strtotime("+1 weeks", $requestedStartTime)], $anchorName) ?>" class="item"><i class="right angle icon"></i> Další týden</a>
			<a href="<?= currentLevelUrl(["week" => $weekNow], $anchorName) ?>" class="right floated item<?= $requestedStartTime == $weekNow ? " active" : "" ?>"><i class="home icon"></i>Jdi na aktuální týden</a>
			<?php
		} else {
			?>
			<a href="<?= currentLevelUrl(["month" => strtotime("-1 months", $requestedStartTime)], $anchorName) ?>" class="borderless item"><i class="left angle icon"></i> Předchozí měsíc</a>
			<div class="borderless fitted item">
				<?= formHTML("selectMonth", [
					"elements" => [
						"month" => [
							"label" => "
												<i class='dropdown icon'></i>
												<i class='calendar icon'></i>
											" . capitalize($monthNames[date("n", $requestedStartTime)]) . " " . date("Y", $requestedStartTime),
							"classes" => "compact black noRightMargin"
						]
					],
					"values" => [
						"getField" => "month",
						"link" => $anchorName,
						"month" => $requestedStartTime
					]
				]) ?>
			</div>
			<a href="<?= currentLevelUrl(["month" => strtotime("+1 months", $requestedStartTime)], $anchorName) ?>" class="item"><i class="right angle icon"></i> Další měsíc</a>
			<a href="<?= currentLevelUrl(["month" => $monthNow], $anchorName) ?>" class="right floated item<?= $requestedStartTime == $monthNow ? " active" : "" ?>"><i class="home icon"></i>Aktuální měsíc</a>
			<?php
		}
		?>
	</div>
	<?php
	$addNewEventButtonShown = userHasPrivilege("eventCreating") && !isset($data["events"]);
	$addButtonShown = ($regularEventsOpCnt || $addNewEventButtonShown) && ($data["showAddButton"] ?? true);

	$addEventMenu = function ($startTime) use ($addNewEventButtonShown, $regularEvents) {
		?>
		<div class="menu">
			<div class="ui icon search input">
				<i class="search icon"></i>
				<input placeholder="Najít..." type="text">
			</div>
			<div class="scrolling menu">
				<?php
				if ($addNewEventButtonShown)
					echo "<a class='item' href='" . subLevelUrl(["newEvent", "start" => $startTime]) . "'><i class='plus icon'></i>Nová událost</a>";

				foreach ($regularEvents as $r) {
					if (!$r["isOp"])
						continue;

					echo "<a class='item' href='" . subLevelUrl(["newEvent", "regularEvent" => $r["id"], "start" => $startTime]) . "'>{$r["name"]}</a>";
				}
				?>
			</div>
		</div>
		<?php
	};

	$eventRecord = function ($event, $displayStartTime, $displayEndTime) use ($regularEvents, $attributeFilterFunc) {
		$isContinuation = $displayStartTime > $event["start"];
		$linkData = ["event", "id" => $event["id"] ?? "r;{$event["schedule"]};{$event["regularStart"]}"];

		$color = "teal";
		$icon = null;
		$detail = null;

		if ($event["status"] == 1) {
			$color = "red";
			$icon = "remove";
			$detail = "Zrušeno";
		} elseif ($event["regularStart"] && $event["start"] != $event["regularStart"] && $event["time"] == $event["regularStart"]) {
			$color = "red";
			$icon = "sign out";
			$detail = "Přesunuto na " . eventDateToString($event["start"]);
		} elseif ($isContinuation) {
			$color = "grey";
			$icon = "resize horizontal";
		} elseif ($event["status"] == 2) {
			$color = "grey";
			$icon = "question";
			$detail = "Nepotvrzeno";
		} elseif ($event["status"] == 3) {
			$color = "";
			$icon = "remove";
			$detail = "Neuskuteční se";
		} elseif ($event["regularStart"] && $event["start"] != $event["regularStart"] && strtotime("midnight", $event["start"]) == strtotime("midnight", $event["regularStart"])) {
			$color = "orange";
			$icon = "time";
			$detail = "Změna začátku z " . date("G:i", $event["regularStart"]);
		} elseif ($event["regularStart"] && $event["start"] != $event["regularStart"] && $event["time"] == $event["start"]) {
			$color = "orange";
			$icon = "sign in";
			$detail = "Přesunuto z " . eventDateToString($event["regularStart"]);
		} elseif ($event["regularStart"]) {
			$color = "blue";
			$icon = "retweet";
		}

		echo "<p>";
		?>
		<a href="<?= subLevelUrl($linkData) ?>" class="ui <?= $color ?> image eventCalendarItem label" data-eventid="<?= $linkData["id"] ?>">
			<?php
			if ($icon)
				echo "<i class='$icon icon'></i>";

			echo $event["name"];

			if ($detail)
				echo "<span class=\"detail\">$detail</span>";
			?>
		</a>
		<?php
		if ($event["subheading"] && $event["time"] == $event["start"])
			echo "<span class=\"ui green image eventCalendarItem label\"><i class='info icon'></i>{$event["subheading"]}</span>";

		if ($event["time"] == $event["start"]) {
			$attributes = eventAttributes($event, $regularEvents[$event["regularEvent"]] ?? null);
			foreach ($attributes as $attr) {
				if ($attr["visibility"] < 3 || !currentUserMeetsSemicolonUsersGroupStr($attr["visibleTo"]) || !$attributeFilterFunc($attr, $event))
					continue;

				$attrData = eventAttributeData($attr["type"]);
				$displayVal = ($attrData["inlineDisplayValueTransformer"] ?? $attrData["displayValueTransformer"])($attr["deducedValue"], $attr["params"]);

				if (!$displayVal)
					continue;

				$icon = $attr["icon"] ?? "info";

				echo "<span class='ui image eventCalendarItem label'><i class='$icon icon'></i>$displayVal" . ($attr["name"] ? "<span class='detail'>{$attr["name"]}</span>" : "") . "</span>";
			}
		}

		echo "</p>";
	};

	if ($isCalendar) {
		$singleDayEvents = $eventsListResult["singleDayEvents"];
		$multiDayEvents = $eventsListResult["multiDayEvents"];
		$singleDayEventCountPerDay = $eventsListResult["singleDayEventCountPerDay"];

		?>
		<table class="ui small compact striped sticky table eventCalendar" data-stickyheader="#eventOverviewMenu-<?= $randomId ?>" id="eventOverview-<?= $randomId ?>">
			<tbody>
				<?php
				$previousDaysEvents = [];
				$previousDaysEventsI = 0;
				$singleDayEventsI = 0;
				$multiDayEventsI = 0;

				//$nextDayTime = strtotime( "+1 days", $startTime );
				while (true) {
					$multiDayEvent = $multiDayEvents[$multiDayEventsI] ?? ["time" => $endTime];
					if ($multiDayEvent["time"] >= startTime/*$nextDayTime*/)
						break;

					$previousDaysEvents["e" . ($previousDaysEventsI++)] = $multiDayEvent;
					$multiDayEventsI++;
				}


				for ($time = $startTime; $time < $endTime; $time = $nextDayTime) {
					foreach ($previousDaysEvents as $index => $event) {
						if ($event["end"] <= $time)
							unset($previousDaysEvents[$index]);
					}

					$nextDayTime = strtotime("+1 days", $time);
					//$todayEventCount = ($eventCountPerDay[$time] ?? 0) + count( $previousDaysEvents );
					$todayEventCount = max($singleDayEventCountPerDay[$time] ?? 0, ($eventCountPerDay[$time] ?? 0) - ($singleDayEventCountPerDay[$time] ?? 0) + count($previousDaysEvents));
					$todayRowspan = max($todayEventCount, 1);

					if ($time <= $timeNow && $nextDayTime > $timeNow)
						$rowClass = "positive";
					elseif ($time < $requestedStartTime || $time >= $requestedEndTime)
						$rowClass = "differentMonth";
					elseif (date("N", $time) >= 6)
						$rowClass = "weekend";
					else
						$rowClass = "";

					if (date("N", $time) == 1)
						$rowClass .= " monday";

					?>
					<tr class="<?= $rowClass ?>">
						<td class="collapsing dayName" rowspan="<?= $todayRowspan ?>">
							<?php
							echo capitalize(weekDayShortName(date("N", $time))) . date(" j.&#8198;n.", $time);
							?>
						</td>
						<?php
						if ($addButtonShown) {
							?>
							<td class="collapsing" rowspan="<?= $todayRowspan ?>">
								<div class="ui compact icon basic mini button dropdown left pointing noSelect eventOverviewEventMenu">
									<i class="plus icon"></i>
									<?php
									$addEventMenu($time);
									?>
								</div>
							</td>
							<?php
						}
						?>
						<?php
						$rEventRecord = function ($event, $time, $nextDayTime) use ($eventRecord, $rowClass) {
							echo "<td class='collapsing timeCell'>";
							if ($event["time"] != strtotime("midnight", $event["time"]) && $time <= $event["time"]) {
								echo "<span class='ui right pointing label eventCalendarItem'><i class='time icon'></i>" . date("G:i", $event["time"]) . "</span>";
							}
							echo "</td>";

							echo "<td class='dataCell'>";
							$eventRecord($event, $time, $nextDayTime);
							echo "</td>";
						};


						/*foreach( $previousDaysEvents as $index => $event )
							$rEventRecord( $event, $time, $nextDayTime );*/

						$previousDayEventI = 0;
						$previousDaysEventsKeys = array_keys($previousDaysEvents);

						$i = 0;
						while (true) {
							$singleDayEvent = $singleDayEvents[$singleDayEventsI] ?? ["time" => $nextDayTime];
							$multiDayEvent = $multiDayEvents[$multiDayEventsI] ?? ["time" => $nextDayTime];

							//echo "<td colspan='4'>".($previousDayEventI>= count( $previousDaysEventsKeys )) ." && ". ($singleDayEvent["time"] >= $nextDayTime) ." && ". ($multiDayEvent["time"] >= $nextDayTime) ."</td></tr><tr>";
							if ($previousDayEventI >= count($previousDaysEventsKeys) && $singleDayEvent["time"] >= $nextDayTime && $multiDayEvent["time"] >= $nextDayTime)
								break;

							if ($i++)
								echo "</tr><tr class='extraDayRow $rowClass'>";

							if ($singleDayEvent["time"] < $nextDayTime) {
								$rEventRecord($singleDayEvent, $time, $nextDayTime);
								$singleDayEventsI++;
							} else
								echo "<td class='collapsing' colspan='2'></td>";

							if ($previousDayEventI < count($previousDaysEventsKeys)) {
								$event = $previousDaysEvents[$previousDaysEventsKeys[$previousDayEventI]];
								$rEventRecord($event, $time, $nextDayTime);
								$previousDayEventI++;

							} elseif ($multiDayEvent["time"] < $nextDayTime) {
								$rEventRecord($multiDayEvent, $time, $nextDayTime);
								$previousDaysEvents["e" . ($previousDaysEventsI++)] = $multiDayEvent;

								$multiDayEventsI++;
							} else
								echo "<td class='collapsing' colspan='2'></td>";
						}

						if (!$i)
							echo "<td class='collapsing' colspan='4'></td>";
						?>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
		<?php
	} else {
		$events = $eventsListResult["events"];

		?>
		<table class="ui very compact striped table eventOverviewList" data-stickyheader="#eventOverviewMenu-<?= $randomId ?>" id="eventOverview-<?= $randomId ?>">
			<thead>
				<tr>
					<th class="collapsing">Datum</th>
					<th>Událost</th>
					<?php
					foreach ($extraColumns as $col)
						echo "<th>{$col["title"]}</th>";
					?>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($events as $event) {
					$regularEvent = $regularEvents[$event["regularEvent"]] ?? null;
					?>
					<tr class="<?= $event["start"] <= $timeNow && $event["end"] > $timeNow ? "positive" : "" ?>">
						<td class="collapsing">
							<span class="ui label eventCalendarItem">
								<i class="calendar icon"></i><?= eventDateToString($event["time"], $event["start"] == $event["time"] ? $event["end"] : null) ?>
							</span>
						</td>
						<td>
							<?php
							$eventRecord($event, $event["time"], $event["end"]);
							?>
						</td>
						<?php
						foreach ($extraColumns as $col) {
							echo "<td>";
							echo callIfCallable($col["content"], $event, $regularEvent);
							echo "</td>";
						}
						?>
					</tr>
					<?php
				}
				?>
			</tbody>
			<?php
			if ($addButtonShown) {
				?>
				<tfoot>
					<tr>
						<th colspan="<?= 2 + count($extraColumns) ?>">
							<div class="ui compact green right floated button dropdown pointing eventOverviewEventMenu">
								<i class="plus icon"></i> Vytvořit událost
								<?php
								$addEventMenu($requestedStartTime);
								?>
							</div>
						</th>
					</tr>
				</tfoot>
				<?php
			}
			?>
		</table>
		<?php
	}
}