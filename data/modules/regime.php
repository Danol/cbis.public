<?php
	$INCLUDED ?? false or die;
	
	function regimeNextEvent( $r, $originTime ) {
		$data = explode( ";", $r["regime"] );
		$dayNames = [
				null, "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"
		];
		
		$timeSince = $r["timeSince"] ?? 0;
		$originTime = max( $originTime, $r["since"] );
		
		switch( $data[0] ) {
			
			case "dateInMonth":
				$result = strtotime( "midnight first day of this month", $originTime );
				$result = strtotime( "+" . ($data[1] - 1) . " days +$timeSince minutes", $result );
				
				if( $result < $originTime )
					$result = strtotime( "+1 months", $result );
				
				break;
			
			case "weekDay":
				$sinceDate = strtotime( "{$dayNames[$data[1]]} midnight +$timeSince minutes", $r["since"] );
				
				if( $originTime < $sinceDate ) {
					$result = $sinceDate;
					
				} else {
					$days = diffDates( $originTime, $sinceDate )->days;
					
					$div = 7 * $data[2];
					$days = ceil( $days / $div ) * $div;
					
					$result = strtotime( "+$days days", $sinceDate );
					
					if( $result < $originTime )
						$result = strtotime( "+{$data[2]} weeks", $result );
				}
				
				break;
			
			case "monthWeekDay":
				$days = ($data[2] - 1) * 7;
				$result = strtotime( "midnight first day of this month", $originTime );
				$result = strtotime( "{$dayNames[$data[1]]} +$days days +$timeSince minutes", $result );
				
				if( $result < $originTime ) {
					$result = strtotime( "midnight first day of next month", $originTime );
					$result = strtotime( "{$dayNames[$data[1]]} +$days days +$timeSince minutes", $result );
				}
				
				break;
			
			case "lastMonthWeekDay":
				$result = strtotime( "midnight last day of this month", $originTime );
				$result = strtotime( "+1 days", $result );
				$result = strtotime( "last {$dayNames[$data[1]]} +$timeSince minutes", $result );
				
				if( $result < $originTime ) {
					$result = strtotime( "midnight last day of next month", $originTime );
					$result = strtotime( "last {$dayNames[$data[1]]} +$timeSince minutes", $result );
				}
				
				break;
		}
		
		//echo eventDateToString($r["since"] ), eventDateToString($result ) . " vs " . eventDateToString(strtotime( "+$timeSince minutes", $r["since"] ))."<br>";
		if( !is_null( $r["until"] ) && $result >= strtotime( "+" . ($r["timeUntil"] ?? 0) . " minutes", $r["until"] ) )
			return null;
		
		return $result;
	}
	
	function regimeStr( $r ) {
		$result = regimeItemsAssocArray()[$r["regime"]];
		
		if( !is_null( $r["timeSince"] ) )
			$result .= ", " . dayTimeToStr( $r["timeSince"] );
		
		if( !is_null( $r["timeUntil"] ) )
			$result .= " &ndash; " . dayTimeToStr( $r["timeUntil"] );
		
		if( $r["days"] )
			$result .= " (trvá {$r["days"]} dní)";
		
		return $result;
	}
	
	function regimeItemsAssocArray() {
		global $REGIME_ITEMS_ASSOC_ARRAY;
		
		if( isset( $REGIME_ITEMS_ASSOC_ARRAY ) )
			return $REGIME_ITEMS_ASSOC_ARRAY;
		
		$result = [];
		
		$days = [
				null,
				[
						"4p" => "pondělí",
						"every" => "každé",
						"form" => "s"
				],
				[
						"4p" => "úterý",
						"every" => "každé",
						"form" => "s"
				],
				[
						"4p" => "středu",
						"every" => "každou",
						"form" => "f"
				],
				[
						"4p" => "čtvrtek",
						"every" => "každý",
						"form" => "m"
				],
				[
						"4p" => "pátek",
						"every" => "každý",
						"form" => "m"
				],
				[
						"4p" => "sobotu",
						"every" => "každou",
						"form" => "f"
				],
				[
						"4p" => "neděli",
						"every" => "každou",
						"form" => "f"
				]
		];
		$ords = [
				"m" => [ "nultý", "první", "druhý", "třetí", "čtvrtý", "pátý", "šestý", "sedmý", "osmý" ],
				"f" => [ "nultou", "první", "druhou", "třetí", "čtvrtou", "pátou", "šestou", "sedmou", "osmou" ],
				"s" => [ "nulté", "první", "druhé", "třetí", "čtvrté", "páté", "šesté", "sedmé", "osmé" ]
		];
		
		for( $i = 1; $i <= 7; $i++ ) {
			$day = $days[$i];
			$result["weekDay;$i;1"] = capitalize( $day["every"] ) . " {$day["4p"]}";
		}
		
		for( $j = 2; $j <= 8; $j++ ) {
			for( $i = 1; $i <= 7; $i++ ) {
				$day = $days[$i];
				$result["weekDay;$i;$j"] = capitalize( $day["every"] ) . " $j. {$day["4p"]}";
			}
		}
		
		for( $j = 1; $j <= 5; $j++ ) {
			for( $i = 1; $i <= 7; $i++ ) {
				$day = $days[$i];
				$result["monthWeekDay;$i;$j"] = "$j. {$day["4p"]} v měsíci";
			}
		}
		
		for( $i = 1; $i <= 7; $i++ ) {
			$day = $days[$i];
			$result["lastMonthWeekDay;$i"] = "Poslední {$day["4p"]} v měsicí";
		}
		
		for( $j = 1; $j <= 31; $j++ )
			$result["dateInMonth;$j"] = "$j. každý měsíc";
		
		$REGIME_ITEMS_ASSOC_ARRAY = $result;
		return $result;
	}