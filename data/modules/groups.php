<?php
	function currentUserIsGroupOperator( $groupId ) {
		return in_array( $groupId, currentUserGroupOperatorArray() );
	}
	
	function currentUserIsAnyGroupOperator() {
		return count( currentUserGroupOperatorArray() ) > 0;
	}
	
	/// Returns assoc array (id => name) of groups the current user is operator of
	function currentUserGroupOperatorAssocArray() {
		global $USER_GROUP_OPERATOR_ASSOC_ARRAY;
		if( isset( $USER_GROUP_OPERATOR_ASSOC_ARRAY ) )
			return $USER_GROUP_OPERATOR_ASSOC_ARRAY;
		
		currentUserGroupMemberAssocArray();
		return $USER_GROUP_OPERATOR_ASSOC_ARRAY;
	}
	
	/// Returns id array of groups the current user is operator of
	function currentUserGroupOperatorArray() {
		global $USER_GROUP_OPERATOR_ARRAY;
		if( isset( $USER_GROUP_OPERATOR_ARRAY ) )
			return $USER_GROUP_OPERATOR_ARRAY;
		
		$USER_GROUP_OPERATOR_ARRAY = array_keys( currentUserGroupOperatorAssocArray() );
		return $USER_GROUP_OPERATOR_ARRAY;
	}
	
	/// Returns id array of groups the current user is member of
	function currentUserGroupMemberAssocArray() {
		global $USER_GROUP_MEBMER_ASSOC_ARRAY, $USER_GROUP_OPERATOR_ASSOC_ARRAY;
		
		if( isset( $USER_GROUP_MEBMER_ASSOC_ARRAY ) )
			return $USER_GROUP_MEBMER_ASSOC_ARRAY;
		
		if( userHasPrivilege("groupMgmt") ) {
			$q = dbQuery( "
					SELECT id, name, 1 AS isOp
					FROM groups
					ORDER BY name ASC
				");
		} else {
			$q = dbQuery( "
					SELECT id, name, isOp
					FROM groups
					INNER JOIN (
						SELECT arg1 AS groupId, MAX(type = 'groupOp') AS isOp
						FROM associations
						WHERE ( type IN ('groupMember','groupOp') ) AND ( arg2 = ? )
						GROUP BY arg1
					) t2 ON groupId = id
					ORDER BY name ASC
				", loggedUserId() );
		}
		
		$USER_GROUP_MEBMER_ASSOC_ARRAY = [];
		$USER_GROUP_OPERATOR_ASSOC_ARRAY = [];
		
		while( $r = $q->fetch() ) {
			$USER_GROUP_MEBMER_ASSOC_ARRAY[$r["id"]] = $r["name"];
			
			if( $r["isOp"] )
				$USER_GROUP_OPERATOR_ASSOC_ARRAY[$r["id"]] = $r["name"];
		}

		return $USER_GROUP_MEBMER_ASSOC_ARRAY;
	}
	
	/// Returns if the current user is operator of all groups passed
	function currentUserIsGroupsOperator( $groups ) {
		return count( array_intersect( $groups, currentUserGroupOperatorArray() ) ) == count( $groups );
	}
	
	/// Returns if passed parameter is valid user id
	function isGroupId( int $group ) {
		return dbQueryValue( "SELECT COUNT(*) FROM groups WHERE ( id = ? ) AND ( NOT deleted )", $group ) == 1;
	}
	
	/// Returns if all ids in the array are valid user ids
	function isGroupIdArray( array $groups ) {
		$groups = array_unique( $groups );
		return dbQueryValue( "SELECT COUNT(*) FROM groups WHERE ( id IN ((?)) ) AND ( NOT deleted )", $groups ) == count( $groups );
	}
	
	function groupLinkHTML( $data ) {
		$result = "<a href='" . subLevelUrl( [ "group", "id" => $data["id"] ] ) . "'>";
		
		if( $data["icon"] ?? null )
			$result .= "<i class='{$data["icon"]} icon'></i>";
		
		$result .= $data["name"] . "</a>";
		return $result;
	}
	
	function groupLabelHTML( $data, $settings = [] ) {
		$icon = $data["icon"] ?? null;
		$result = "<a href='" . subLevelUrl( [ "group", "id" => $data["id"] ] ) . "' class='ui " . ($icon ? " image" : "") . " " . ($settings["classes"] ?? "") . " label'>";
		
		if( $icon )
			$result .= "<i class='$icon icon'></i>";
		
		$result .= $data["name"];
		
		$details = [];
		if( $data["isOp"] ?? false )
			$details[] = "<i class='spy icon'></i>Správce";
		elseif( !($data["publicMembership"] ?? false) )
			$details[] = "<i class='hide icon'></i>";
		
		if( $details )
			$result .= "<div class='detail'>" . implode( "", $details ) . "</div>";
		
		$result .= "</a>";
		return $result;
	}
	
	
	function groupsAssocArray() {
		$result = [];
		$q = dbQuery( "SELECT id, name FROM groups WHERE NOT deleted ORDER BY name ASC" );
		
		while( $r = $q->fetch() )
			$result[$r["id"]] = $r["name"];
		
		return $result;
	}