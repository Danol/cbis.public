<?php
	$INCLUDED ?? false or die;
	
	/// Returns if passed parameter is valid user id
	function isUserId( int $user ) {
		return isset( usersAssocArray()[$user] );
	}
	
	/// Returns if all ids in the array are valid user ids
	function isUserIdArray( array $users ) {
		$users = array_unique( $users );
		return dbQueryValue( "SELECT COUNT(*) FROM users WHERE ( id IN ((?)) ) AND ( banState = 'none' )", $users ) == count( $users );
	}
	
	function usersGroupsSemicolonStrLabels( $semiArray ) {
		return usersGroupsArrayLabels( usersGroupsSemicolonStrToArray( $semiArray ) );
	}
	
	function usersSemicolonStrLabels( $semiArray ) {
		return usersArrayLabels( usersSemicolonStrToArray( $semiArray ) );
	}
	
	function usersGroupsArrayLabels( array $array ) {
		$suppArr = usersGroupsIconAssocArray();
		$result = "";
		
		foreach( $array as $item ) {
			$classifier = substr( $item, 0, 1 );
			$id = substr( $item, 1 );
			
			$result .= "<a href='" . subLevelUrl( [ $classifier == "u" ? "user" : "group", "id" => $id ] ) . "' class='ui compact label'>{$suppArr[$item]}</a>";
		}
		
		return $result;
	}
	
	function usersArrayLabels( array $array ) {
		$suppArr = usersAssocArray();
		$result = "";
		
		foreach( $array as $item )
			$result .= "<a href='" . subLevelUrl( [ "user", "id" => $item ] ) . "' class='ui compact label'>{$suppArr[$item]}</a>";
		
		return $result;
	}
	
	function usersGroupsSemicolonStrToArray( $semiArray ) {
		if( $semiArray == ";;" || !$semiArray )
			return [];
		
		return array_intersect( explode( ";", $semiArray ), usersGroupsIdArray() );
	}
	
	function usersSemicolonStrToArray( $semiArray ) {
		if( $semiArray == ";;" || !$semiArray )
			return [];
		
		return array_intersect( explode( ";", $semiArray ), usersIdArray() );
	}
	
	function arrayToSemicolonStr( array $array ) {
		return ";" . implode( ";", $array ) . ";";
	}
	
	function currentUserMeetsSemicolonUsersGroupStr( $str ) {
		$myId = loggedUserId();
		$grpArr = currentUserGroupMemberAssocArray();
		$opArr = currentUserGroupOperatorAssocArray();
		
		foreach( explode( ";", $str ) as $item ) {
			$classifier = substr( $item, 0, 1 );
			$id = substr( $item, 1 );
			
			if( !$item )
				continue;
			
			elseif( $classifier == "u" && $id == $myId )
				return true;
			elseif( $classifier == "g" && isset( $grpArr[$id] ) )
				return true;
			elseif( $classifier == "o" && isset( $opArr[$id] ) )
				return true;
			elseif( $classifier == "a" )
				return true;
		}
		
		return false;
	}

	function semicolonUsersGroupStrFilteredUserAssocArray( $str ) {
		$users = [];
		$groupMembers = [];
		$groupOps = [];

		foreach( explode( ";", $str ) as $item ) {
			$classifier = substr( $item, 0, 1 );
			$id = substr( $item, 1 );
			
			if( !$item )
				continue;
			
			elseif( $classifier == "u" )
				$users[] = $id;
			elseif( $classifier == "g" )
				$groupMembers[] = $id;
			elseif( $classifier == "o" )
				$groupOps[] = $id;
			elseif( $classifier == "a" )
				return usersAssocArray();
		}

		$result = [];

		if( !empty($users) ) {
			$q = dbQuery("SELECT id, displayName FROM users WHERE id IN ((?))", $users);
			while( $r = $q->fetch() )
				$result[$r["id"]] = $r["displayName"];
		}

		if( !empty($groupMembers) ) {
			$q = dbQuery("SELECT id, displayName FROM associations JOIN users ON (arg2 = users.id) WHERE ((type = 'groupOp') OR (type = 'groupMember')) AND (arg1 IN ((?)))", $groupMembers);
			while( $r = $q->fetch() )
				$result[$r["id"]] = $r["displayName"];
		}

		if( !empty($groupOps) ) {
			$q = dbQuery("SELECT id, displayName FROM associations JOIN users ON (arg2 = users.id) WHERE (type = 'groupOp') AND (arg1 IN ((?)))", $groupOps);
			while( $r = $q->fetch() )
				$result[$r["id"]] = $r["displayName"];
		}

		return $result;
	}
	
	function avatarImgSrc( $userId ) {
		//return "https://robohash.org/cbis_$userId.png";
		//return "https://robohash.org/cbis_$userId.png?bgset=bg1";
		return "https://api.adorable.io/avatars/64/$userId.png";
		//return "http://flathash.com/" . md5( "cbis_" . $userId );
	}
	
	function avatarHtml( $userId, $classes = "ui bordered avatar image" ) {
		return "<img class='$classes' src='" . avatarImgSrc( $userId ) . "'/>";
	}
	
	function userLink( $userId, $displayName = null ) {
		if( !$displayName )
			$displayName = usersAssocArray()[$userId];
		
		return "<a href='" . subLevelUrl( [ "user", "id" => $userId ] ) . "' class='user'>$displayName</a>";
	}
	
	function usersWithAvatarsAssocArray() {
		$result = [];
		$q = dbQuery( "SELECT id, displayName FROM users WHERE banState = 'none' ORDER BY displayName ASC" );
		
		while( $r = $q->fetch() )
			$result[$r["id"]] = avatarHtml( $r["id"], "ui mini avatar image" ) . " " . $r["displayName"];
		
		return $result;
	}
	
	function usersAssocArray() {
		global $USERS_ASSOC_ARRAY, $USERS_ID_ARRAY, $USERS_NAME_ARRAY;
		if( isset( $USERS_ASSOC_ARRAY ) )
			return $USERS_ASSOC_ARRAY;
		
		$USERS_ASSOC_ARRAY = [];
		$USERS_ID_ARRAY = [];
		$USERS_NAME_ARRAY = [];
		$q = dbQuery( "SELECT id, displayName FROM users WHERE banState = 'none' ORDER BY displayName ASC" );
		
		while( $r = $q->fetch() ) {
			$USERS_ASSOC_ARRAY[$r["id"]] = $r["displayName"];
			$USERS_ID_ARRAY[] = $r["id"];
			$USERS_NAME_ARRAY[] = $r["displayName"];
		}
		
		return $USERS_ASSOC_ARRAY;
	}

	function usersNameArray() {
		global $USERS_NAME_ARRAY;
		if( !isset( $USERS_NAME_ARRAY ) )
			usersAssocArray();
		
		return $USERS_NAME_ARRAY;
	}
	
	function usersIdArray() {
		global $USERS_ID_ARRAY;
		if( !isset( $USERS_ID_ARRAY ) )
			usersAssocArray();
		
		return $USERS_ID_ARRAY;
	}
	
	function usersGroupsAssocArray() {
		global $USERS_GROUPS_ASSOC_ARRAY;
		if( !isset( $USERS_GROUPS_ASSOC_ARRAY ) )
			usersGroupsIconAssocArray();
		
		return $USERS_GROUPS_ASSOC_ARRAY;
	}
	
	function usersGroupsIconAssocArray() {
		global $USERS_GROUPS_ICON_ASSOC_ARRAY, $USERS_GROUPS_ASSOC_ARRAY, $USERS_GROUPS_ID_ARRAY;
		if( isset( $USERS_GROUPS_ICON_ASSOC_ARRAY ) )
			return $USERS_GROUPS_ICON_ASSOC_ARRAY;
		
		$USERS_GROUPS_ICON_ASSOC_ARRAY = [];
		$USERS_GROUPS_ASSOC_ARRAY = [];
		$USERS_GROUPS_ID_ARRAY = [];
		
		$q = dbQuery( "
			SELECT CONCAT('g', id) AS id, CONCAT('Členové skupiny ', name) AS name, icon, FALSE AS isUser
			FROM groups
			WHERE NOT deleted
			UNION ALL
			SELECT CONCAT('o', id) AS id, CONCAT('Správci skupiny ', name) AS name, icon, FALSE AS isUser
			FROM groups
			WHERE NOT deleted
			UNION ALL
			SELECT CONCAT('u', id) AS id, displayName AS name, '' AS icon, TRUE AS isUser
			FROM users
			WHERE banState = 'none'
			UNION ALL
			SELECT 'a' AS id, 'Všichni' AS name, 'asterisk' AS icon, FALSE AS isUser
			ORDER BY isUser ASC, name ASC
			" );
		
		while( $r = $q->fetch() ) {
			$USERS_GROUPS_ASSOC_ARRAY[$r["id"]] = $r["name"];
			$USERS_GROUPS_ICON_ASSOC_ARRAY[$r["id"]] = ($r["icon"] ? "<i class='{$r["icon"]} icon'></i>" : "") . $r["name"];
			$USERS_GROUPS_ID_ARRAY[] = $r["id"];
		}
		
		return $USERS_GROUPS_ICON_ASSOC_ARRAY;
	}
	
	function usersGroupsIdArray() {
		global $USERS_GROUPS_ID_ARRAY;
		if( !isset( $USERS_GROUPS_ID_ARRAY ) )
			usersGroupsIconAssocArray();
		
		return $USERS_GROUPS_ID_ARRAY;
	}
	
	function generatePasswordAndSendEmail( $email, $isNewUser ) {
		$passwdLen = 12;
		$pwd = substr( str_shuffle( str_repeat( "abcdefghijklmnopqrstuvwxyz0123456789", $passwdLen ) ), 0, $passwdLen );
		
		mail(
				$email,
				"CBIS | Uživatelské údaje",
				"<html><body>" .
				($isNewUser ? "Byl vám vytvořen účet v informačním systému CBIS." : "Bylo vám administrátorem změněno heslo v informačním systému CBIS.") .
				"<br><br>" .
				"Adresa webu: <a href='http://cbis.straw-solutions.cz'>http://cbis.straw-solutions.cz</a> <br>" .
				"Přihlašovací jméno: " . $email . " <br>" .
				"Heslo: " . $pwd .
				"</body></html>",
				"From: \"=?utf-8?B?" . base64_encode( "CBIS Systém" ) . "?=\" <cbis@straw-solutions.cz>" .
				//"\r\nReply-To: danol@straw-solutions.cz" .
				"\r\nX-Mailer: PHP/" . phpversion() .
				"\r\nMIME-Version: 1.0" .
				"\r\nContent-Type: text/html;charset=utf-8"
		);
		
		return $pwd;
	}