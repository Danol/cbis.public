<?php
	$INCLUDED ?? false or die;
	
	addJSFile( "js/comments.js" );
	
	function commentItem( $r, $topicType, $topicId, $data, $instanceId, $isTopLevel, $replyId ) {
		$interval = verbalizedIntervalToNow( $r["timeSent"] );
		
		if( $isTopLevel )
			$itemCount = dbQuery( "SELECT 0 FROM comments WHERE ( topicType = ? ) AND ( topicId = ? ) AND ( replyTo = ? ) AND ( NOT deleted )", $topicType, $topicId, $r["id"] )->rowCount();
		else
			$itemCount = 0;
		
		if( $r["deleted"] /*&& $itemCount == 0*/ )
			return;
		?>
		<div class="comment" data-id="<?= $r["id"] ?>">
			<a name="comment-<?= $r["id"] ?>"></a>
			<div class="avatar">
				<?= avatarHtml( $r["user"] ) ?>
			</div>
			<div class="content">
				<span class="author"><?= userLink( $r["user"], $r["displayName"] ) ?></span>
				<span class="metadata"><?= date( "j.n.Y G:i", $r["timeSent"] ) ?><?= $interval ? " | $interval" : "" ?></span>
				<div class="text">
					<?php
						if( $r["deleted"] ) {
							echo "<span class='ui red text'><i class='delete icon'></i>Komentář byl smazán</span>";
							
						} else {
							
							
							if( isset( $r["deepReplyTo"] ) && $r["deepReplyTo"] != $r["replyTo"] )
								echo "<a href='#comment-" . $r["deepReplyTo"] . "' class='replyToPrefix' data-sourceid='" . $r["deepReplyTo"] . "''>" . $r["replyDisplayName"] . "</a>: ";
							
							echo "<div class='markdown'>";
							echo "{$r["formattedText"]}</div>";
						}
					?>
				</div>
				<?php
					if( !$r["deleted"] ) {
						?>
						<div class="actions">
							<a class="reply commentReplyButton" data-instanceid="<?= $instanceId ?>" data-replyid="<?= $replyId ?>" data-deepreplyid="<?= $r["id"] ?>" data-action="<?= currentLevelUrl( [ "$instanceId-replyComment" => $r["id"] ], "comment-$replyId" ) ?>"><i class="reply icon"></i>Odpovědět</a>
							<?php
								if( $r["user"] == loggedUserId() || canModerateComments( $topicType, $topicId ) )
									echo "<a class='' onclick='formSubmitConfirm(\"" . formCSSID( "deleteComment", $instanceId ) . "\", \"Smazání komentáře\", \"Opravdu smazat komentář?\", {id: \"{$r["id"]}\"})'><i class='delete icon'></i>Smazat</a>";
							?>
						</div>
						<?php
					}
					
					if( ($_GET["$instanceId-replyComment"] ?? "") == $r["id"] ) {
						echo messagesHTML( "replyComments-$instanceId" );
						echo formHTML( "comment", [
								"values" => [ "topicType" => $topicType, "topicId" => $topicId, "replyTo" => $replyId ],
								"elements" => [ "submit" => [ "label" => "Odpovědět" ] ],
								"id" => $instanceId . "-replyForm",
								"formClass" => "reply",
								"msgGroup" => "replyComments-$instanceId",
								"formAction" => "#comment-" . $r["id"]
						] );
					}
				?>
			</div>
			<?php
				if( $isTopLevel ) {
					if( $itemCount ) {
						echo "<div class=\"comments\">";
						
						$maxItemCount = 3;
						$limit = "";
						
						if( $itemCount > $maxItemCount && ($_GET["expandComments"] ?? "") != $r["id"] ) {
							$limit = "LIMIT " . ($itemCount - $maxItemCount) . ", " . $maxItemCount;
							?>
							<a href="<?= currentLevelUrl( [ "expandComments" => $r["id"] ], "comment-" . $r["id"] ) ?>" class="ui tiny compact fluid button"><i class="history icon"></i>Zobrazit starší příspěvky (<?= $itemCount - $maxItemCount ?>)</a>
							<?php
						}
						
						/*$q2 = dbQuery( "SELECT comments.id AS id, text, user, displayName, timeSent FROM comments LEFT JOIN users on users.id = user WHERE ( topicType = ? ) AND ( topicId = ? ) AND ( replyTo = ? ) ORDER BY timeSent ASC $limit", $topicType, $topicId, $r["id"] );*/
						
						$q2 = dbQuery( "
							SELECT
								comment.id AS id, comment.formattedText AS formattedText, comment.user AS user, comment.timeSent AS timeSent, comment.replyTo AS replyTo, comment.deepReplyTo AS deepReplyTo, comment.deleted AS deleted,
								users.displayName AS displayName,
								replyUser.displayName AS replyDisplayName
							FROM comments comment
								LEFT JOIN users on users.id = user
								LEFT JOIN comments replyComment ON comment.deepReplyTo = replyComment.id
								LEFT JOIN users replyUser ON replyUser.id = replyComment.user
							WHERE ( comment.topicType = ? ) AND ( comment.topicId = ? ) AND ( comment.replyTo = ? ) AND ( NOT comment.deleted )
							ORDER BY comment.timeSent ASC
							$limit
							", $topicType, $topicId, $r["id"] );
						
						while( $r2 = $q2->fetch() )
							commentItem( $r2, $topicType, $topicId, $data, $instanceId, false, $replyId );
						
						echo "</div>";
					}
					
				}
			?>
		</div>
		<?php
	}
	
	function commentsSection( $topicType, $topicId, $data = [] ) {
		if( !canComment( $topicType, $topicId ) ) {
			reportMessage( [ "type" => "error", "text" => "Nemáte oprávnění číst tyto komentáře." ] );
			return;
		}
		
		$header = $data["header"] ?? "Komentáře";
		$instanceId = $topicType . "-" . $topicId . "-" . ($data["instanceId"] ?? "");
		
		echo formHTML( "deleteComment", [ "id" => $instanceId ] );
		?>
		<div class="ui container commentsSection" id="commentsSection-<?= $instanceId ?>">
			<?php
				if( $header ) {
					?>
					<h3 class="ui dividing header">
						<i class="comments icon"></i>
						<div class="content">
							<?= $header ?>
						</div>
					</h3>
					<?php
				}
			?>
			<div class="ui threaded comments">
				<?php
					$itemsPerPage = 20;
					$pageCount = ceil( dbQuery( "SELECT 0 FROM comments WHERE ( topicType = ? ) AND ( topicId = ? ) AND ( replyTo IS NULL )", $topicType, $topicId )->rowCount() / $itemsPerPage );
					$currentPage = paginationCurrentPage( $pageCount, "-" . $instanceId, $pageCount );
					
					$q = dbQuery( "
							SELECT
								comments.id AS id, formattedText, user, timeSent, deleted,
								displayName
							FROM comments
								LEFT JOIN users on users.id = user
							WHERE ( topicType = ? ) AND ( topicId = ? ) AND ( replyTo IS NULL )
							ORDER BY timeSent ASC
							LIMIT ?, ?
							", $topicType, $topicId, ($currentPage - 1) * $itemsPerPage, $itemsPerPage );
					
					while( $r = $q->fetch() )
						commentItem( $r, $topicType, $topicId, $data, $instanceId, true, $r["id"] );
				?>
			</div>
			<?php
				if( $pageCount > 1 )
					echo paginationHTML( $pageCount, "-" . $instanceId, [ "classes" => "ui basic compact segment" ] );
				
				echo messagesHTML( "comments-$instanceId" );
				echo messagesHTML( "replyComments-$instanceId" );
				
				if( !isset( $_GET["$instanceId-replyComment"] ) ) {
					echo formHTML( "comment", [
							"values" => [ "topicType" => $topicType, "topicId" => $topicId ],
							"elements" => [ "submit" => [ "label" => "Odpovědět" ] ],
							"id" => $instanceId . "-replyForm",
							"formClass" => "reply",
							"hidden" => true,
							"msgGroup" => "replyComments-$instanceId"
					] );
				}
				
				echo formHTML( "comment", [ "values" => [ "topicType" => $topicType, "topicId" => $topicId ], "id" => $instanceId, "msgGroup" => "comments-$instanceId" ] );
			?>
		</div>
		<?php
	}
	
	require_once "$DATA_PATH/config/comments.php";