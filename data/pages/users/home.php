<?php
	$INCLUDED ?? false or die;
	
	showAnnouncements( "homePage" );
?>
<h1 class="ui center aligned icon header">
	<i class="home icon"></i>
	<div class="content">
		Vítejte v CBIS
	</div>
</h1>
<div class="ui one column center aligned page grid">
	<div class="column sixteen wide">
		<p>
			CBIS je informační systém pro CB Náchod. Tento informační systém aktuálně umožňuje spravovat sborový kalendář a obsahuje sekci modlitební podpory.
		</p>
    <h4 class="ui dividing header"><i class="id card icon"></i> Kontaktní osoba</h4>
    <p><b>Daniel Čejchan</b><br>mail: cbna.oznameni@gmail.com<br>tel.: +420 774 414 730<br>skype: czdanol</p>
		<h4 class="ui dividing header"><i class="bug icon"></i> Chyby, problémy, nápady</h4>
		<p>
			Pokud narazíte na nějaké chyby, nebo máte nějaké podněty/nápady, napište je prosím do komentáře v sekci "Zpětná vazba".
		</p>
	</div>
</div>