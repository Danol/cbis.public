<?php
	$INCLUDED ?? false or die;

	$doEdit = false;

	$q = dbQuery( "SELECT id, displayName, privileges, email FROM users WHERE ( id = ? ) AND ( banState = 'none' )", $_GET["id"] ?? "" );
	if ( !$q->rowCount() ) {
		reportMessage( ["type" => "error", "text" => "Uživatel neexistuje"] );
		return;
	}

	$values = $q->fetch();

	echo formHtml( "removeGroupMem", ["hidden" => true, "values" => ["user" => $values["id"]]] );

?>
<table class="ui definition table">
	<thead>
		<tr>
			<th colspan="2">
				<h1 class='ui header'>
					<?=avatarHtml( $values["id"] )?>
					<div class="content">
						<?=$values["displayName"]?>
						<div class="sub header">Profil uživatele</div>
					</div>
				</h1>
			</th>
		</tr>
	</thead>
	<tbody>
		<?php
			if ( userHasPrivilege( "userMgmt" ) ) {
			?>
		<tr>
			<td class="collapsing"><i class="mail icon"></i>E-mail:</td>
			<td><a href="mailto: <?=$values["email"]?>"><?=$values["email"]?></a></td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="collapsing"><i class="users icon"></i>Skupiny:</td>
			<td><?php
			    	$isInGroup = [];

			    	$q = dbQuery( "
								SELECT DISTINCT id, icon, name, publicMembership, isOp
								FROM groups
								INNER JOIN (
									SELECT arg1 AS groupId, MAX(type = 'groupOp') AS isOp
									FROM associations
									WHERE ( type IN('groupOp','groupMember' ) ) AND ( arg2 = ? )
									GROUP BY groupId
								) t2 ON groups.id = t2.groupId
								WHERE ( publicMembership OR ( groups.id IN ((?)) ) OR isOp OR ? )
								ORDER BY name
								", $values["id"], currentUserGroupOperatorArray(), loggedUserId() == $values["id"] );

			    	while ( $r = $q->fetch() ) {
			    		$isInGroup[] = $r["id"];
			    		echo groupLabelHTML( $r );

			    		if ( currentUserIsGroupOperator( $r["id"] ) ) {
			    			echo "<a onclick='formSubmitConfirm(\"" . formCSSID( "removeGroupMem" ) . "\", \"Zrušení členství\", \"Opravdu zrušit členství uživatele \\\"{$values["displayName"]}\\\" ve skupině \\\"{$r["name"]}\\\"?\", {group: \"{$r["id"]}\"})' class='ui tiny red compact left pointing basic label'><i class='fitted ban icon'></i></a><span style='display: inline-block; margin-right: 8px;'></span>";
			    		}
			    	}

			    	$groups = currentUserGroupOperatorAssocArray();
			    	foreach ( $isInGroup as $gid ) {
			    		unset( $groups[$gid] );
			    	}

			    	if ( count( $groups ) ) {
			    		echo formHTML( "addUserToGroup", ["values" => ["user" => $values["id"]], "elements" => ["group" => ["items" => $groups]]] );
			    }
			    ?></td>
		</tr>
		<?php
			if ( userHasPrivilege( "userMgmt" ) && $values["privileges"] ) {
			?>
				<tr>
					<td class="collapsing"><i class="legal icon"></i>Privilegia:</td>
					<td>
						<?php
							echo implode( "", array_map( function ( $i ) {
									global $PRIVILEGE_DESCS;
									return "<div class='ui small compact label'>{$PRIVILEGE_DESCS[$i]}</div>";
								}, explode( ";", $values["privileges"] ) ) );
							?>
					</td>
				</tr>
				<?php
					}

					$regularEventsOp = dbQuery( "SELECT * FROM regularEvents WHERE ( NOT deleted ) AND ( ops LIKE ? )", "%;u{$values["id"]};%" );
					if ( $regularEventsOp->rowCount() ) {
					?>
				<tr>
					<td class="collapsing"><i class="spy icon"></i>Správcovství:</td>
					<td>
						<?php
							while ( $r = $regularEventsOp->fetch() ) {
									echo "<a href='" . subLevelUrl( ["regularEvent", "id" => $r["id"]] ) . "' class='ui compact label'><i class='retweet icon'></i>{$r["name"]}</a>";
								}

							?>
					</td>
				</tr>
				<?php
					}
				?>
	</tbody>
</table>

<?php
	if ( userHasPrivilege( "userMgmt" ) ) {
	?>
		<div class="ui styled fluid accordion">
			<div class="title"><i class="write icon"></i>Upravit uživatele</div>
			<div class="content">
				<?php
					echo formHTML( "editUser", ["values" => $values] );

						echo "<h2 class='ui header'>Vygenerování nového hesla</h2>";
						echo formHTML( "generateNewPassword", ["values" => ["userId" => $values["id"]]] );
						echo "<button onclick='formSubmitConfirm(\"" . formCSSID( "generateNewPassword" ) . "\", \"Vygenerování nového hesla\", \"Opravdu vygenerovat nové heslo uživateli \\\"{$values["displayName"]}\\\"?\" )' class='ui orange button'><i class='recycle icon'></i>Vygenerovat nové heslo</button>";
					?>
			</div>
		</div>
		<?php
			}
		?>
<div class="clearing"></div>
