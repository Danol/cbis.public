<?php $INCLUDED ?? false or die; ?>
<h1 class="ui header"><i class="user icon"></i>Uživatelský panel</h1>
<button onclick="$('#changePwdModal').modal('show');" class="ui button"><i class="lock icon"></i>Změnit heslo</button>
<div class="ui modal" id="changePwdModal">
	<div class="header"><i class="lock icon"></i>Změna hesla</div>
	<div class="content">
		<?= messagesHTML( "changePwdModal" ) ?>
		<?= formHtml( "changePassword", [ "modalID" => "changePwdModal", "formAction" => "#" ] ) ?>
	</div>
</div>