<?php $INCLUDED ?? false or die; ?>
<h1 class="ui header"><i class="user icon"></i>Uživatelé</h1>
<?php
	if( userHasPrivilege( "userMgmt" ) ) {
		echo formHtml( "deleteUser", [ "hidden" => "true" ] );
		?>
		<div class="ui modal" id="addUserModal">
			<div class="header"><i class="plus icon"></i>Nový uživatel</div>
			<div class="content">
				<?= messagesHTML( "addUserModal" ) ?>
				<?= formHtml( "editUser", [
						"modalID" => "addUserModal",
						"formAction" => "#",
						"redirect" => subLevelUrl( [ "user", "id" => "%resultData%", "edit" => true ] )
				] ) ?>
			</div>
		</div>
		<?php
	}
	
	dataTableHTML( [
			"query" => "SELECT id, email, displayName FROM users WHERE banState = 'none'",
			"columns" => [
					[
							"data" => function( $r ) {
								return avatarHtml( $r["id"] );
							},
							"collapsing" => true
					],
					[
							"label" => "Jméno",
							"data" => function( $r ) {
								return userLink( $r["id"], $r["displayName"] );
							},
							"sortColumn" => "displayName"
					],
					//[ "label" => "E-mail", "data" => "%email%", "sortColumn" => "email" ],
					[
							"data" => function( $r ) {
								$result = "<a href='" . subLevelUrl( [ "user", "id" => $r["id"] ] ) . "' class='ui tiny basic blue compact button'><i class='user icon'></i>Profil</a>";
								
								if( userHasPrivilege( "userMgmt" ) )
								$result .=
										"<button onclick='formSubmitConfirm(\"" . formCSSID( "deleteUser" ) . "\", \"Smazání uživatele\", \"Opravdu smazat uživatele \\\"%displayName%\\\"?\", {id: \"%id%\"} )' class='ui tiny compact basic red button'><i class='ban icon'></i>Smazat</button>";
								
								return $result;
							},
							"collapsing" => true
					]
			],
			"actionRow" => function() {
				if( !userHasPrivilege( "userMgmt" ) )
					return "";
				
				return "<a onclick=\"$('#addUserModal').modal('show');\" class=\"ui small compact right floated green button\"><i class=\"plus icon\"></i>Nový uživatel</a>";
			},
			"defaultSortColumn" => "displayName"
	] ); ?>
