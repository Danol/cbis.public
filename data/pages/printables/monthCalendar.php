<?php
	$INCLUDED ?? false or die;
	
	require_once "$DATA_PATH/config/eventAttributes.php";
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">

		<link rel="stylesheet" href="css/printables.css">

		<title><?= callIfCallable( $CURRENT_PAGE_DATA["label"], $_GET ) ?> – CBIS</title>
	</head>
	<body>
		<?php
			$startTime = strtotime( "midnight first day of this month", $_GET["month"] ?? time() );
			$endTime = strtotime( "+1 months", $startTime );
			
			$eventsListResult = eventsList( $startTime, $endTime, [
					"simpleView" => true,
					"excludeCancelled" => true
			] );
			$regularEventsOpCnt = $eventsListResult["regularEventsOpCnt"];
			$regularEvents = $eventsListResult["regularEvents"];
			$eventCountPerDay = $eventsListResult["eventCountPerDay"];
			$singleDayEvents = $eventsListResult["singleDayEvents"];
			$multiDayEvents = $eventsListResult["multiDayEvents"];
			$singleDayEventCountPerDay = $eventsListResult["singleDayEventCountPerDay"];
			
			$roomsAssoc = [];
			$roomsIdList = [];
			$q = dbQuery( "SELECT id, shortName FROM rooms WHERE NOT deleted ORDER BY name ASC" );
			while( $r = $q->fetch() ) {
				$roomsAssoc[$r["id"]] = $r["shortName"];
				$roomsIdList[] = $r["id"];
			}
			
			$eventCells = "<td class='eventTime'></td><td class='eventName'></td><td class='eventRoom'></td>";
			$multidayEventCells = "<td class='eventTime multidayEvent'></td><td class='eventName multidayEvent'></td>";
		?>
		<table class="printableCalendarTable">
			<thead>
				<tr>
					<th colspan="7" class="title">
						<?= capitalize( monthName( date( "n", $startTime ) ) ) . " " . date( "Y", $startTime ) ?>
					</th>
				</tr>
				<tr>
					<th class="dayName" colspan="2">Datum</th>
					<th class="eventTime"></th>
					<th class="eventName"></th>
					<th class="eventRoom"></th>
					<th class="eventTime multidayEvent"></th>
					<th class="eventName multidayEvent"></th>
				</tr>
			</thead>
			<tbody>
				<?php
					$previousDaysEvents = [];
					$previousDaysEventsI = 0;
					$singleDayEventsI = 0;
					$multiDayEventsI = 0;
					
					$nextDayTime = strtotime( "+1 days", $startTime );
					while( true ) {
						$multiDayEvent = $multiDayEvents[$multiDayEventsI] ?? [ "time" => $endTime ];
						if( $multiDayEvent["time"] >= $startTime )
							break;
						
						$previousDaysEvents["e" . ($previousDaysEventsI++)] = $multiDayEvent;
						$multiDayEventsI++;
					}
					
					
					for( $time = $startTime; $time < $endTime; $time = $nextDayTime ) {
						foreach( $previousDaysEvents as $index => $event ) {
							if( $event["end"] <= $time )
								unset( $previousDaysEvents[$index] );
						}
						
						$nextDayTime = strtotime( "+1 days", $time );
						$todayEventCount = max( $singleDayEventCountPerDay[$time] ?? 0, ($eventCountPerDay[$time] ?? 0) - ($singleDayEventCountPerDay[$time] ?? 0) + count( $previousDaysEvents ) );
						$todayRowspan = max( $todayEventCount, 1 );
						
						$rowClass = date( "N", $time ) >= 6 ? "weekend" : "";
						?>
						<tr class="baseDayRow <?= $rowClass ?>">
							<td class="dayNumber" rowspan="<?= $todayRowspan ?>">
								<?php
									echo date( "j.", $time );
								?>
							</td>
							<td class="dayName" rowspan="<?= $todayRowspan ?>">
								<?php
									echo capitalize( weekDayShortName( date( "N", $time ) ) );
								?>
							</td>
							<?php
								if( $todayEventCount == 0 )
									echo $eventCells . $multidayEventCells;
								
								$rEventRecord = function( $event, $time, $nextDayTime, $isMultiday ) use ( $rowClass, $eventCells, $regularEvents, $roomsAssoc, $roomsIdList ) {
									$multidayClass = ($isMultiday ? "multidayEvent" : "");
									$attrs = eventAttributes( $event, $regularEvents[$event["regularEvent"]] ?? null );
									
									$displayAttrs = [];
									foreach( $attrs as $attr ) {
										if( $attr["visibility"] < 4 || strpos( $attr["visibleTo"], ";a;" ) === false )
											continue;
										
										$attrData = eventAttributeData( $attr["type"] );
										$displayVal = ($attrData["inlineDisplayValueTransformer"] ?? $attrData["displayValueTransformer"])( $attr["deducedValue"], $attr["params"] );
										
										if( !$displayVal )
											continue;
										
										$displayAttrs[] = decapitalize($attr["name"] ? "{$attr["name"]}: " : "") . $displayVal;
									}

									if($event["subheading"])
										$displayAttrs[] = $event["subheading"];
									
									echo "<td class='eventTime $multidayClass'>";
									if( $event["time"] != strtotime( "midnight", $event["time"] ) && $time <= $event["time"] )
										echo date( "G:i", $event["time"] );
									echo "</td>";
									
									echo "<td class='eventName $multidayClass'>";
									echo $event["name"];
									if( $displayAttrs )
										echo " (" . implode( ", ", $displayAttrs ) . ")";
									echo "</td>";
									
									if( !$isMultiday ) {
										echo "<td class='eventRoom $multidayClass'>";
										$eventRooms = eventAttributeValueFromAttributes( $attrs, "rooms" );
										$eventRooms = array_intersect( explode( ";", $eventRooms ), $roomsIdList );
										echo implode( ", ", array_map( function( $x ) use ( $roomsAssoc ) {
											return $roomsAssoc[$x];
										}, $eventRooms ) );
										echo "</td>";
									}
								};
								
								$previousDayEventI = 0;
								$previousDaysEventsKeys = array_keys( $previousDaysEvents );
								
								$i = 0;
								while( true ) {
									if( $i++ )
										echo "</tr><tr class='extraDayRow $rowClass'>";
									
									$singleDayEvent = $singleDayEvents[$singleDayEventsI] ?? [ "time" => $nextDayTime ];
									$multiDayEvent = $multiDayEvents[$multiDayEventsI] ?? [ "time" => $nextDayTime ];
									
									if( $previousDayEventI >= count( $previousDaysEventsKeys ) && $singleDayEvent["time"] >= $nextDayTime && $multiDayEvent["time"] >= $nextDayTime )
										break;
									
									if( $singleDayEvent["time"] < $nextDayTime ) {
										$rEventRecord( $singleDayEvent, $time, $nextDayTime, false );
										$singleDayEventsI++;
									} else
										echo $eventCells;
									
									if( $previousDayEventI < count( $previousDaysEventsKeys ) ) {
										$event = $previousDaysEvents[$previousDaysEventsKeys[$previousDayEventI]];
										$rEventRecord( $event, $time, $nextDayTime, true );
										$previousDayEventI++;
										
									} elseif( $multiDayEvent["time"] < $nextDayTime ) {
										$rEventRecord( $multiDayEvent, $time, $nextDayTime, true );
										$previousDaysEvents["e" . ($previousDaysEventsI++)] = $multiDayEvent;
										
										$multiDayEventsI++;
									} else
										echo $multidayEventCells;
								}
							?>
						</tr>
						<?php
					}
				?>
			</tbody>
		</table>
	</body>
</html>