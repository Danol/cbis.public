<?php $INCLUDED ?? false or die; ?>

	<h1 class="ui header"><i class="print icon"></i>K tisku</h1>
	<p>
		Tiskoviny doporučujeme otevírat v Google Chrome, protože ten na papír nepřidává záhlaví a zápatí.
	</p>

<?= formHTML( "selectMonth", [
		"elements" => [
				"month" => [
						"label" => "Měsíční kalendář",
				]
		],
		"values" => [
				"redirect" => subLevelUrl( [ "printable-monthCalendar", "month" => "%month%" ] )
		]
] ) ?>