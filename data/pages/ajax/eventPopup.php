<?php
	$INCLUDED ?? false or die;
	
	require_once "$DATA_PATH/config/events.php";
	require_once "$DATA_PATH/config/eventAttributes.php";
	
	$eventData = getEventData( $_GET["id"] ?? ";;;;;" );
	if( is_string( $eventData ) ) {
		reportMessage( [ "type" => "error", "text" => $eventData ] );
		return;
	}
	
	$event = $eventData["event"];
	$schedule = $eventData["schedule"];
	$regularEvent = $eventData["regularEvent"];
	$attributes = eventAttributes( $event, $regularEvent );
	
	$isEventOp = currentUserIsEventOp( $event, $regularEvent );
?>
<h4 class="ui center aligned header" style="width: 200px;"><?= $event["name"] ?? $regularEvent["name"] ?></h4>
<table class="ui very basic compact table">
	<tr>
		<th> Datum</th>
		<th class="collapsing">
			<i class='calendar icon'></i>
		</th>
	</tr>
	<tr>
		<td colspan="2"><?php
				if( $event["status"] == 1 )
					echo "<span class='ui tiny compact red label'><i class='remove icon'></i> Zrušeno</span><br>";
				elseif( $event["status"] == 2 )
					echo "<span class='ui tiny compact grey label'><i class='question icon'></i> Nepotvrzeno</span><br>";
				elseif( $event["status"] == 3 )
					echo "<span class='ui tiny compact label'><i class='remove icon'></i> Neuskuteční se</span><br>";
				elseif( $event["regularStart"] && $event["start"] != $event["regularStart"] ) {
					if( strtotime( "midnight", $event["start"] ) == strtotime( "midnight", $event["end"] ) )
						echo "<span class='ui tiny compact orange label'><i class='time icon'></i> Změna začátku z " . eventDateToString( $event["regularStart"] ) . "</span><br>";
					else
						echo "<span class='ui tiny compact orange label'><i class='right arrow icon'></i> Přesunuto z " . eventDateToString( $event["regularStart"] ) . "</span><br>";
				}
				
				echo eventDateToString( $event["start"], $event["end"] );
			?></td>
	</tr>
	
	<?php
		foreach( $attributes AS $r ) {
			$attrData = eventAttributeData( $r["type"] );
			$displayVal = $attrData["displayValueTransformer"]( $r["deducedValue"], $r["params"] );
			if( $displayVal == "" || $r["visibility"] < 2 || !currentUserMeetsSemicolonUsersGroupStr( $r["visibleTo"] ) )
				continue;
			
			?>
			<tr>
				<th><?= $r["name"] ?></th>
				<th class="collapsing"><?= $r["icon"] ? "<i class='{$r["icon"]} icon'></i>" : "" ?></th>
			</tr>
			<tr>
				<td colspan="2"><?= $displayVal ?></td>
			</tr>
			<?php
		}
		
		if( $event["htmlNotes"] ) {
			?>
			<tr>
				<th>Poznámky</th>
				<th class="collapsing"><i class='sticky note icon'></i></th>
			</tr>
			<tr>
				<td colspan="2"><?= $event["htmlNotes"] ?></td>
			</tr>
			<?php
		}
	?>
</table>
