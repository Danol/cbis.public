<?php
require_once "$DATA_PATH/google_calendar/vendor/autoload.php";

require_once "$DATA_PATH/modules/eventOverview.php";
require_once "$DATA_PATH/config/eventAttributes.php";

if($UPDATE_GOOGLE_CALENDAR) {
	// Setup Google API
	{
		$client = new Google_Client();
		$client->setApplicationName("CBIS");
		$client->setScopes(Google_Service_Calendar::CALENDAR);
		$client->setAuthConfig("$DATA_PATH/calendar_key.json");

		$service = new Google_Service_Calendar($client);
		$calendarId = $GOOGLE_CALENDAR_ID;
	}

	$start = (time() - 3600 * 24 * 7);
	$end = (time() + 3600 * 24 * 60);

	// Load Google events cache
	{
		$googleEvents = [];
		$q = dbQuery("SELECT googleEventId, localEventId FROM googleEventSync WHERE (start < ?) AND (end >= ?)", $end, $start);
		while ($r = $q->fetch())
			$googleEvents[$r["localEventId"]] = $r["googleEventId"];
	}

	$data = eventsList($start, $end, [
		"simpleView" => true,
		"excludeCancelled" => true
	]);

	foreach ($data["events"] as $event) {
		$uniqueId = $event["uniqueId"];

		$googleEvent = new Google_Service_Calendar_Event();

		$name = $event["name"];
		if($event["subheading"])
			$name .= " - " . $event["subheading"];
		$googleEvent->setSummary($name);

		$start = new Google_Service_Calendar_EventDateTime();
		$start->setDateTime(date("c", $event["start"]));
		$googleEvent->setStart($start);

		$end = new Google_Service_Calendar_EventDateTime();
		$end->setDateTime(date("c", $event["end"]));
		$googleEvent->setEnd($end);

		$description = "";

		$attrs = eventAttributes($event, $data["regularEvents"][$event["regularEvent"]] ?? null);
		foreach ($attrs as $attr) {
			if ($attr["visibility"] < 4 || strpos($attr["visibleTo"], ";a;") === false)
				continue;

			$attrData = eventAttributeData($attr["type"]);
			$displayVal = ($attrData["inlineDisplayValueTransformer"] ?? $attrData["displayValueTransformer"])($attr["deducedValue"], $attr["params"]);

			if (!$displayVal)
				continue;

			$description .= "{$attr["name"]}: {$displayVal}\n";
		}

		if($description)
			$description .= "\n";

		$description .= $event["notes"];
		$googleEvent->setDescription($description);

		if(isset($googleEvents[$uniqueId])) {
			$service->events->patch($calendarId, $googleEvents[$uniqueId], $googleEvent);
			unset($googleEvents[$uniqueId]);
		} else {
			$googleEventId = $service->events->insert($calendarId, $googleEvent)->getId();
			dbQuery("INSERT INTO googleEventSync(googleEventId, localEventId, start, end) VALUES(?, ?, ?, ?)", $googleEventId, $uniqueId, $event["start"], $event["end"]);
		}
	}

	foreach ($googleEvents as $googleEventId) {
		try {
			$service->events->delete($calendarId, $googleEventId);
		} catch(Exception $e) {

		}
		dbQuery("DELETE FROM googleEventSync WHERE googleEventId = ?", $googleEventId);
	}
}