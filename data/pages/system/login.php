<?php $INCLUDED ?? false or die; ?>

<div class="ui middle aligned centered grid" style="margin-top: 100px;">
	<div class="column" style="max-width: 600px;">
		<h1 class="ui header">Přihlášení</h1>
		<?php
			echo messagesHTML();
			echo formHTML( "login" );
		?>
	</div>
</div>