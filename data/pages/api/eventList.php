<?php
require_once "$DATA_PATH/modules/eventOverview.php";
require_once "$DATA_PATH/config/eventAttributes.php";

header("content-type: application/json");

$start = $_GET["start"] ?? time();
$end = $_GET["end"] ?? ($start + 3600 * 24 * 14);

if($end - $start > 3600 * 24 * 100) {
	echo json_encode([
		"status" => "error",
		"error" => "Cannot ask for more than 100 days."
		]);
	return;
}

$extended = $_GET["extended"] ?? false;
$data = eventsList($start, $end, [
	"simpleView" => !$extended,
	"excludeCancelled" => !$extended
]);

$result = [];
foreach ($data["events"] as $event) {
	$row = [
		"id" => $event["uniqueId"],
		"commonId" => $event["commonId"],
		"start" => $event["start"],
		"end" => $event["end"],
		"name" => $event["name"],
		"notes" => $event["notes"],
		"subheading" => $event["subheading"],
	];

	if($extended) {
		$row["regularStart"] = $event["regularStart"];
		$row["status"] = $event["status"];
	}

	$rowAttrs = [];

	$attrs = eventAttributes($event, $data["regularEvents"][$event["regularEvent"]] ?? null);
	foreach ($attrs as $attr) {
		if ($attr["visibility"] < 4 || strpos($attr["visibleTo"], ";a;") === false)
			continue;

		$attrData = eventAttributeData($attr["type"]);
		$displayVal = ($attrData["inlineDisplayValueTransformer"] ?? $attrData["displayValueTransformer"])($attr["deducedValue"], $attr["params"]);

		if (!$displayVal)
			continue;

		$rowAttrs[] = [
			"name" => $attr["name"],
			"commonId" => $attr["commonId"],
			"value" => $displayVal
		];
	}

	$row["attributes"] = $rowAttrs;
	$result[] = $row;
}

echo json_encode([
	"status" => "ok",
	"result" => $result
]);