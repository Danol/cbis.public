<?php $INCLUDED ?? false or die; ?>
<h1 class="ui header"><i class="users icon"></i>Skupiny</h1>
<?php
	if( userHasPrivilege( "groupMgmt" ) ) {
		echo formHtml( "deleteGroup", [ "hidden" => "true" ] );
		
		?>
		<div class="ui modal" id="addGroupModal">
			<div class="header"><i class="plus icon"></i>Nová skupina</div>
			<div class="content">
				<?= messagesHTML( "addGroupModal" ) ?>
				<?= formHtml( "editGroup", [
						"modalID" => "addGroupModal",
						"formAction" => "#",
						"redirect" => subLevelUrl( [ "group", "id" => "%resultData%", "edit" => true ] )
				] ) ?>
			</div>
		</div>
		<?php
	}

	dataTableHTML( [
			"query" => "SELECT id, name, icon FROM groups WHERE NOT deleted",
			"columns" => [
					[
							"data" => "<i class='large %icon% icon'></i>",
							"collapsing" => true
					],
					[
							"label" => "Název",
							"data" => function( $r ) {
								return "<a href='" . subLevelUrl( [ "group", "id" => "%id%" ] ) . "'>%name%</a>";
							},
							"sortColumn" => "name"
					],
					[
							"data" => function( $r ) {
								$result = "";
								
								$result .= " <a href = '" . subLevelUrl( [ "group", "id" => $r["id"] ] ) . "' class='ui tiny basic blue compact button' ><i class='info icon' ></i>Informace</a > ";
								
								if( userHasPrivilege( "groupMgmt" ) ) {
									$result .= " <button onclick = 'formSubmitConfirm(\"" . formCSSID( "deleteGroup" ) . "\", \"Smazání skupiny\", \"Opravdu smazat skupinu \\\"%name%\\\"?\", {id: \"%id%\"} )' class='ui tiny basic compact red button' ><i class='ban icon' ></i>Smazat</button > ";
								}
								
								return $result;
							},
							"collapsing" => true
					]
			],
			"actionRow" => function() {
				if( !userHasPrivilege( "groupMgmt" ) )
					return "";
				
				return " <a onclick = \"$('#addGroupModal').modal('show');\" class=\"ui small compact right floated green button\"><i class=\"plus icon\"></i>Nová skupina</a>";
			},
			"defaultSortColumn" => "name"
	] ); ?>
