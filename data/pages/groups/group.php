<?php
	$INCLUDED ?? false or die;
	
	$q = dbQuery( "SELECT id, name, privileges, opPrivileges, icon, publicMembership FROM groups WHERE ( id = ? ) AND ( NOT deleted )", $_GET["id"] ?? "" );
	if( !$q->rowCount() ) {
		reportMessage( [ "type" => "error", "text" => "Skupina neexistuje" ] );
		return;
	}
	
	$values = $q->fetch();
	$isGroupOp = currentUserIsGroupOperator( $values["id"] );
	$canViewMembers = $values["publicMembership"] || $isGroupOp;
?>
<table class="ui definition table">
	<thead>
		<tr>
			<th colspan="2">
				<h1 class='ui header'>
					<i class='<?= $values["icon"] ?> icon'></i>
					<div class="content">
						<?= $values["name"] ?>
						<div class="sub header">Skupina</div>
					</div>
				</h1>
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="collapsing"><i class="protect icon"></i>Členství je veřejné:</td>
			<td><?= $values["publicMembership"] ? "ano" : "ne" ?></td>
		</tr>
		<?php
			if( $values["privileges"] && (userHasPrivilege( "groupMgmt" ) || isset( currentUserGroupMemberAssocArray()[$values["id"]] )) ) {
				?>
				<tr>
					<td class="collapsing"><i class="legal icon"></i>Privilegia členů:</td>
					<td>
						<?php
							echo implode( "", array_map( function( $i ) {
								global $PRIVILEGE_DESCS;
								return "<div class='ui small compact label'>{$PRIVILEGE_DESCS[$i]}</div>";
							}, explode( ";", $values["privileges"] ) ) );
						?>
					</td>
				</tr>
				<?php
			}
			if( $values["opPrivileges"] && (userHasPrivilege( "groupMgmt" ) || isset( currentUserGroupOperatorAssocArray()[$values["id"]] )) ) {
				?>
				<tr>
					<td class="collapsing">
						<i class="legal icon"></i>
						Privilegia správců:
					</td>
					<td>
						<?php
							echo implode( "", array_map( function( $i ) {
								global $PRIVILEGE_DESCS;
								return "<div class='ui small compact label'>{$PRIVILEGE_DESCS[$i]}</div>";
							}, explode( ";", $values["opPrivileges"] ) ) );
						?>
					</td>
				</tr>
				<?php
			}
			
			$regularEventsOp = dbQuery( "SELECT * FROM regularEvents WHERE ( NOT deleted ) AND ( ops LIKE ? )", "%;g{$values["id"]};%" );
			if( $regularEventsOp->rowCount() ) {
				?>
				<tr>
					<td class="collapsing"><i class="spy icon"></i>Správcovství:</td>
					<td>
						<?php
							while( $r = $regularEventsOp->fetch() )
								echo "<a href='" . subLevelUrl( [ "regularEvent", "id" => $r["id"] ] ) . "' class='ui compact label'><i class='retweet icon'></i>{$r["name"]}</a>";
						?>
					</td>
				</tr>
				<?php
			}
		?>
	</tbody>
</table>
<?php
	if( userHasPrivilege( "groupMgmt" ) ) {
		?>
		<div class="ui styled fluid accordion">
			<div class="title"><i class="write icon"></i>Upravit skupinu</div>
			<div class="content">
				<?= formHTML( "editGroup", [ "values" => $values ] ) ?>
			</div>
		</div>
		<?php
	}

?>
<h2 class="ui dividing header">
	<?php
		if( $canViewMembers ) {
			?>
			<i class="user icon"></i>
			<div class="content">
				Členové
			</div>
			<?php
		} else {
			?>
			<i class="spy icon"></i>
			<div class="content">
				Správci
			</div>
			<?php
		}
	?>
</h2>

<?php
	if( $isGroupOp ) {
		echo formHtml( "removeGroupMem", [ "hidden" => true, "values" => [ "group" => $values["id"] ] ] );
		echo formHtml( "addGroupOp", [ "hidden" => true, "values" => [ "group" => $values["id"] ] ] );
		echo formHtml( "removeGroupOp", [ "hidden" => true, "values" => [ "group" => $values["id"] ] ] );
		
		echo formHtml( "addRemoveGroupMembers", [
				"values" => [
						"groupId" => $values["id"]
				]
		] );
	}
?>

<div class="ui middle aligned divided list">
	<?php
		$q = dbQuery( "
					SELECT DISTINCT users.id AS id, displayName, isOp
					FROM (
						SELECT arg2, MAX(type='groupOp') AS isOp
						FROM associations
						WHERE ( type IN ('groupOp'" . ($canViewMembers ? ",'groupMember'" : "") . ") ) AND ( arg1 = ? )
						GROUP BY arg2
					) t1
					LEFT JOIN users ON arg2 = users.id
					ORDER BY isOp DESC, displayName
					", $values["id"] );
		
		if( !$q->rowCount() ) {
			echo "<div class='item'><i>Žádní členové.</i></div>";
		}
		
		while( $r = $q->fetch() ) {
			echo "<div class='item'>";
			
			echo avatarHtml( $r["id"] );
			echo "<div class='content'>";
			
			if( $r["isOp"] )
				echo "<span class='ui image label'><i class='spy icon'></i>Správce</span> ";
			
			echo "<a href='" . subLevelUrl( [ "user", "id" => $r["id"] ] ) . "'>{$r["displayName"]}</a>";
			echo "</div>";
			
			if( $isGroupOp ) {
				echo "<div class='right floated content'>";
				
				if( $r["isOp"] )
					echo "<button onclick='formSubmit(\"" . formCSSID( "removeGroupOp" ) . "\", {user: \"{$r["id"]}\"})' class='ui tiny basic orange compact button'><i class='down arrow icon'></i>Odebrat správcovství</button>";
				else
					echo "<button onclick='formSubmit(\"" . formCSSID( "addGroupOp" ) . "\", {user: \"{$r["id"]}\"})' class='ui tiny basic green compact button'><i class='up arrow icon'></i>Ustanovit správcem</button>";
				
				echo "<button onclick='formSubmit(\"" . formCSSID( "removeGroupMem" ) . "\", {user: \"{$r["id"]}\"})' class='ui tiny red basic compact button'><i class='ban icon'></i>Odebrat členství</button>";
				echo "</div>";
			}
			echo "</div>";
		}
	?>
</div>