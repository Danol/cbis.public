<?php
	$INCLUDED ?? false or die;
	
	$q = dbQuery( "SELECT id, name, shortName FROM rooms WHERE ( id = ? ) AND ( NOT deleted )", $_GET["id"] ?? "" );
	if( !$q->rowCount() ) {
		reportMessage( [ "type" => "error", "text" => "Místnost neexistuje" ] );
		return;
	}
	
	$room = $q->fetch();
	
	echo "<h1 class=\"ui header\"><i class=\"building icon\"></i>Rozvrh místnosti '{$room["name"]}' ({$room["shortName"]})</h1>";
	eventOverview( [ "room" => $room["id"], "calendar" => true ] );