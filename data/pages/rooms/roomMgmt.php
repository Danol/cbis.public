<?php $INCLUDED ?? false or die; ?>
<h1 class="ui header"><i class="building icon"></i>Místnosti</h1>
<div class="ui modal" id="newRoomModal">
	<div class="header"><i class="plus icon"></i>Nová místnost</div>
	<div class="content">
		<?= messagesHTML( "newRoomModal" ) ?>
		<?= formHtml( "editRoom", [
				"modalID" => "newRoomModal"
		] ) ?>
	</div>
</div>
<?php
	echo formHtml( "deleteRoom", [ "hidden" => "true" ] );
	
	dataTableHTML( [
			"query" => "
				SELECT id, name, shortName
				FROM rooms",
			"columns" => [
					[
							"label" => "Název",
							"data" => "%name%",
							"sortColumn" => "name"
					],
					[
							"label" => "Zkratka",
							"data" => "%shortName%",
							"sortColumn" => "shortName"
					],
					[
							"data" => function( $r ) {
								$result = "";
								
								$result .= "<a href='" . subLevelUrl( [ "roomCalendar", "id" => $r["id"] ] ) . "' class='ui tiny basic compact green button'><i class='calendar icon'></i> Rozvrh</a>";
								
								if( userHasPrivilege( "roomMgmt" ) ) {
									$editModalId = "modal_editRoom_" . $r["id"];
									
									$result .= " <a onclick='$(\"#$editModalId\").modal(\"show\")' class='ui tiny basic compact blue button'><i class='write icon'></i>Upravit</a>";
									$result .= " <button onclick = 'formSubmitConfirm(\"" . formCSSID( "deleteRoom" ) . "\", \"Smazání místnosti\", \"Opravdu smazat místnost \\\"%name%\\\"?\", {id: \"%id%\"} )' class='ui tiny basic compact red button' ><i class='ban icon' ></i> Smazat</button > ";
									
									$result .= formModalHTML( "editRoom", [ "values" => $r, "id" => $r["id"], "modalID" => $editModalId, "modalHeader" => "<i class='write icon'></i>Úprava místnosti" ] );
								}
								
								return $result;
							},
							"collapsing" => true
					]
			],
			"actionRow" => function() {
				if( !userHasPrivilege( "roomMgmt" ) )
					return "";
				
				return "<a onclick=\"$('#newRoomModal').modal('show');\" class=\"ui small compact right floated green button\"><i class=\"plus icon\"></i>Nová místnost</a>";
			},
			"defaultSortColumn" => "name",
	] );
?>
