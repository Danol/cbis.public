<?php
$INCLUDED ?? false or die;

echo "<h1 class=\"ui header\"><i class=\"video icon\"></i>Rozvrh promítačů</h1>";
eventOverview([
	"eventFilter" => function ($event, $regularEvent) {
		return eventHasAttribute($event, $regularEvent, "projection");
	},
	"attributeFilter" => function ($attr, $event) {
		return in_array($attr["commonId"], ["responsiblePerson", "preacher"]);
	},
	"showAddButton" => false,
	"extraColumns" => [
		[
			"title" => "Vedení chval",
			"content" => function ($event, $regularEvent) {
				$attr = eventAttribute($event, $regularEvent, "projection");
				$attrData = eventAttributeData($attr["type"]);

				$displayVal =
					isset($attrData["interactiveDisplayValueTransformer"]) ?
						$attrData["interactiveDisplayValueTransformer"]($attr, $event["id"]) :
						$attrData["displayValueTransformer"]($attr["deducedValue"], $attr["params"]);

				return $displayVal;
			}
		]
	]
]);