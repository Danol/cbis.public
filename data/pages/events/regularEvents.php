<?php
	$INCLUDED ?? false or die;
	
	require_once "$DATA_PATH/config/events.php";
?>
	<h1 class="ui header"><i class="retweet icon"></i>Pravidelné události</h1>

<?php
	echo formHtml( "deleteRegularEvent", [ "formStyle" => "display: none;" ] );
	
	dataTableHTML( [
			"query" => "SELECT id, name FROM regularEvents WHERE NOT deleted",
			"columns" => [
					[ "label" => "Název", "data" => "%name%", "sortColumn" => "name" ],
					[
							"label" => "Pravidelnost",
							"data" => function( $r ) {
								$q = dbQuery( "SELECT id, since, until, regime, timeSince, timeUntil, days FROM regularEventsSchedules WHERE ( NOT deleted ) AND ( event = ? ) AND ( since <= ? ) AND ( ( until IS NULL ) OR ( until > ? ) )", $r["id"], time(), time() );
								
								$result = "";
								while( $r = $q->fetch() )
									$result .= "<span class='ui label'>" . regimeStr( $r ) . "</span>";
								
								return $result;
							}
					],
					[
							"data" => function( $r ) {
								$result = "<a href='" . subLevelUrl( [ "regularEvent", "id" => $r["id"] ] ) . "' class='ui tiny compact blue basic button'><i class='info icon'></i>Stránka</a>";
								
								if( canEditRegularEvent( $r["id"] ) )
									$result .=
											"<button onclick='formSubmitConfirm(\"" . formCSSID( "deleteRegularEvent" ) . "\", \"Smazání události\", \"Opravdu smazat pravidelnou událost \\\"%name%\\\"?\", {id: \"%id%\"} )' class='ui tiny compact basic red button'><i class='ban icon'></i>Smazat</button>";
								
								return $result;
							},
							"collapsing" => true
					]
			],
			"defaultSortColumn" => "name",
			"actionRow" => userHasPrivilege( "eventCreating" ) ? "<a onclick=\"$('#addRegularEventModal').modal('show');\" class=\"ui small compact right floated green button\"><i class=\"plus icon\"></i>Nová událost</a>" : "",
			"emptyText" => "Žádné pravidelné události."
	] );
	
	if( userHasPrivilege( "eventCreating" ) ) {
		?>
		<div class="ui modal" id="addRegularEventModal">
			<div class="header"><i class="plus icon"></i>Nová pravidelná událost</div>
			<div class="content">
				<?= messagesHTML( "addRegularEventModal" ) ?>
				<?= formHtml( "editRegularEvent", [
						"modalID" => "addRegularEventModal",
						"formAction" => "#",
						"redirect" => subLevelUrl( [ "regularEvent", "id" => "%resultData%" ] )
				] ) ?>
			</div>
		</div>
		<?php
	}
?>