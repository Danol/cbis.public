<?php
	$INCLUDED ?? false or die;
	
	require_once "$DATA_PATH/config/events.php";
	require_once "$DATA_PATH/config/eventAttributes.php";
	
	$q = dbQuery( "SELECT id, name, defaultStartTime, defaultEndTime, ops, commonId FROM regularEvents WHERE ( id = ? ) AND ( NOT deleted )", $_GET["id"] ?? "" );
	if( !$q->rowCount() ) {
		reportMessage( [ "type" => "error", "text" => "Záznam neexistuje" ] );
		return;
	}
	
	$values = $q->fetch();
	$isEventOp = currentUserIsRegularEventOp( $values );
?>

<table class="ui definition table">
	<thead>
		<tr>
			<th colspan="2">
				<h1 class='ui header'>
					<i class='retweet icon'></i>
					<div class="content">
						<?= $values["name"] ?>
						<div class="sub header">Pravidelná událost</div>
					</div>
				</h1>
			</th>
		</tr>
	</thead>
	<tbody>
		<?php
			if( true ) {
				?>
				<tr>
					<td class="collapsing"><i class="spy icon"></i>Správci:</td>
					<td>
						<?= usersGroupsSemicolonStrLabels( $values["ops"] ) ?>
					</td>
				</tr>
				<?php
			}
			
			$q = dbQuery( "SELECT id, since, until, regime, timeSince, timeUntil, days FROM regularEventsSchedules WHERE ( NOT deleted ) AND ( event = ? ) AND ( since <= ? ) AND ( ( until IS NULL ) OR ( until > ? ) )", $values["id"], time(), time() );
			if( $q->rowCount() ) {
				?>
				<tr>
					<td class="collapsing"><i class="history icon"></i>Režim:</td>
					<td>
						<?php
							
							while( $r = $q->fetch() ) {
								echo "<span class='ui label'>" . regimeStr( $r ) . "</span>";
							}
						?>
					</td>
				</tr>
				<?php
			}
		?>
	</tbody>
</table>

<div class="ui styled fluid accordion">
	<div class="title"><i class="calendar icon"></i>Kalendář</div>
	<div class="content">
		<?php
			eventOverview( [
					"events" => [ $values["id"] ],
					"calendar" => true,
					"anchor" => "calendar"
			] );
		?>
	</div>
	<div class="title"><i class="list icon"></i>Přehled událostí</div>
	<div class="content">
		<?php
			eventOverview( [
					"events" => [ $values["id"] ]
			] );
		?>
	</div>
	<?php
		if( userHasPrivilege( "eventMgmt" ) ) {
			?>

			<div class="title"><i class="write icon"></i>Upravit událost</div>
			<div class="content">
				<?= formHTML( "editRegularEvent", [
						"values" => $values,
						"suffixHTML" => "<a href=\"" . upLevelUrl() . "\" class=\"ui right floated button\">Zpět</a>"
				] ) ?>
			</div>
			<div class="title"><i class="cubes icon"></i>Atributy</div>
			<div class="content">
				<a name="attributes" id="attributes"></a>
				<?php
					echo formHtml( "deleteRegularEventAttribute", [ "hidden" => "true" ] );
					
					dataTableHTML( [
							"query" => "
								SELECT *
								FROM regularEventsAttributes
								INNER JOIN regularEventsAttributesHistory
								ON ( regularEventsAttributes.id = regularEventsAttributesHistory.id ) AND ( validUntil IS NULL )
								WHERE ( regularEvent = ? )",
							"queryArgs" => [ $values["id"] ],
							"columns" => [
									[
											"label" => "Název",
											"data" => "%name%",
											"sortColumn" => "name",
									],
									[
											"label" => "Typ",
											"data" => function( $r ) {
												global $EVENT_ATTRIBUTE_TYPES;
												return $EVENT_ATTRIBUTE_TYPES[$r["type"]];
											},
											"collapsing" => true
									],
									[
											"label" => "Výchozí hodnota",
											"data" => function( $r ) {
												return eventAttributeData( $r["type"] )["displayValueTransformer"]( $r["defaultValue"], $r["params"] );
											}
									],
									[
											"data" => function( $r ) use ( $isEventOp ) {
												$modalId = "editAttribute-{$r["id"]}";
												
												$result = formModalHTML( regularEventAttributeForm( $r["type"], $r ), [
														"modalID" => $modalId,
														"modalHeader" => "<i class='cubes icon'></i>Úprava atributu"
												] );
												$result .= "<a onclick='$(\"#$modalId\").modal(\"show\")' class='ui tiny basic compact blue button'><i class='write icon'></i>Upravit</a>";
												$result .= " <button onclick = 'formSubmitConfirm(\"" . formCSSID( "deleteRegularEventAttribute" ) . "\", \"Smazání atributu\", \"Opravdu smazat atribut \\\"%name%\\\"?\", {id: \"%id%\"} )' class='ui tiny basic compact red button' ><i class='remove icon'></i>Smazat</button >";
												
												return $result;
											},
											"collapsing" => true
									]
							],
							"link" => "attributes",
							"emptyText" => "Žádné atributy.",
							"defaultSortColumn" => "name",
							"actionRow" => function() use ( $isEventOp, $values ) {
								global $EVENT_ATTRIBUTE_TYPES;
								//return "<a onclick=\"$('#addRegularEventScheduleModal').modal('show');\" class=\"ui small compact right floated green button\"><i class=\"plus icon\"></i>Nový atribut</a>";
								$items = "";
								$forms = "";
								foreach( $EVENT_ATTRIBUTE_TYPES as $type => $desc ) {
									$modalId = "addEventAttribute-$type";
									$items .= "<a class='item' onclick='$(\"#$modalId\").modal(\"show\");'>$desc</a>";
									$forms .= formModalHTML( regularEventAttributeForm( $type, [] ), [
											"modalID" => $modalId,
											"modalHeader" => "<i class='cubes icon'></i>Přidat atribut – $desc",
											"values" => [ "regularEvent" => $values["id"] ] ] );
								}
								
								return "
									$forms
									<div class='ui right floated green pointing dropdown button noSelect'>
										<i class='plus icon'></i> Přidat atribut
										<div class='menu'>
											$items
										</div>
									</div>
									";
							},
					] );
				?>
			</div>
			<?php
		}
		if( $isEventOp ) {
			?>
			<div class="title"><i class="history icon"></i>Režim</div>
			<div class="content">
				<a name="schedule" id="schedule"></a>
				<div class="ui modal" id="addRegularEventScheduleModal">
					<div class="header"><i class="history icon"></i>Nová položka režimu</div>
					<div class="content">
						<?= messagesHTML( "addRegularEventScheduleModal" ) ?>
						<?= formHtml( "editRegularEventSchedule", [
								"modalID" => "addRegularEventScheduleModal",
								"values" => [
										"event" => $values["id"],
										"since" => time(),
								]
						] ) ?>
					</div>
				</div>
				<?php
					
					dataTableHTML( [
							"query" => "SELECT * FROM regularEventsSchedules WHERE ( deleted = 0 ) AND ( event = ? )",
							"queryArgs" => [ $values["id"] ],
							"columns" => [
									[
											"label" => "Platí od",
											"data" => function( $r ) {
												return date( "j. n. Y", $r["since"] );
											},
											"sortColumn" => "since",
											"collapsing" => true
									],
									[
											"label" => "Platí do",
											"data" => function( $r ) {
												return $r["until"] ? date( "j. n. Y", $r["until"] ) : "";
											},
											"sortColumn" => "until IS NULL %ORDER%, until",
											"collapsing" => true
									],
									[
										"label" => "Výchozí stav",
										"data" => function( $r ) {
											return [
												0 => "<span style='color: green;'><i class='check icon'></i> Uskuteční se</span>",
												1 => "<span style='color: red;'><i class='remove icon'></i> Zrušeno</span>",
												2 => "<span style='color: gray;'><i class='question icon'></i> Nepotvrzeno</span>",
												3 => "<span style='color: gray;'><i class='remove icon'></i> Neuskuteční se</span>"
											][$r["defaultStatus"]];
										},
										"collapsing" => true
									],
									[
											"label" => "Režim",
											"data" => function( $r ) {
												return regimeStr( $r );
											}
									],
									[
											"data" => function( $r ) use ( $isEventOp ) {
												if( !$isEventOp )
													return "";
												
												$result = "";
												
												$moveModalId = "modal_moveScheduleUntil_{$r["id"]}";
												$result .= formModalHTML( "moveRegularEventScheduleUntil", [ "values" => $r, "id" => $r["id"], "modalID" => $moveModalId, "modalHeader" => "<i class='resize horizontal icon'></i> Posunutí konce platnosti" ] );
												$result .= "<a onclick='$(\"#$moveModalId\").modal(\"show\")' class='ui tiny basic compact orange button'><i class='resize horizontal icon'></i>Platnost</a>";
												
												if( !$r["until"] ) {
													$editModalId = "modal_editSchedule_{$r["id"]}";
													$form = getForm(
															"editRegularEventSchedule",
															[
																	"values" => $r,
																	"id" => $r["id"],
																	"modalID" => $editModalId,
																	"modalHeader" => "<i class='write icon'></i> Úprava režimu",
																	"elements" => [
																			"since" => [ "fieldClasses" => "disabled" ],
																			"submit" => [ "label" => "Upravit" ]
																	]
															] );
													
													array_splice($form["elements"],0,0,"<div class='ui yellow message'>Úpravou režimu ztratíte všechny data z ještě neuskutečněných událostí tohoto režimu! (veškeré úpravy, poznámky, komentáře, ...)</div>");
													$result .= formModalHTML( $form );
													$result .= "<a onclick='$(\"#$editModalId\").modal(\"show\")' class='ui tiny basic compact blue button'><i class='write icon'></i>Upravit</a>";
												}
												
												return $result;
											},
											"collapsing" => true
									]
							],
							"rowPositive" => function( $r ) {
								return $r["since"] <= time() && (is_null( $r["until"] ) || $r["until"] > time());
							},
							"emptyText" => "Žádné položky rozvrhu.",
							"defaultSortColumn" => "until IS NULL %ORDER%, until",
							"defaultSortDir" => "desc",
							"link" => "schedule",
							"actionRow" => function() use ( $isEventOp ) {
								if( !$isEventOp )
									return "";
								
								return "<a onclick=\"$('#addRegularEventScheduleModal').modal('show');\" class=\"ui small compact right floated green button\"><i class=\"plus icon\"></i>Nová položka</a>";
							},
					] );
				?>
			</div>
			<?php
		}
	?>
</div>