<?php
$INCLUDED ?? false or die;

require_once "$DATA_PATH/config/events.php";
require_once "$DATA_PATH/config/eventAttributes.php";

$eventData = getEventData($_GET["id"] ?? ";;;;;");
if (is_string($eventData)) {
	reportMessage(["type" => "error", "text" => $eventData]);
	return;
}

$event = $eventData["event"];
$schedule = $eventData["schedule"];
$regularEvent = $eventData["regularEvent"];
$attributes = eventAttributes($event, $regularEvent);

$isEventOp = currentUserIsEventOp($event, $regularEvent);
?>
	<table class="ui definition table">
		<thead>
			<tr>
				<th colspan="2">
					<h1 class='ui header'>
						<i class='checked calendar icon'></i>
						<div class="content">
							<?= $event["name"] ?? $regularEvent["name"] ?>
							<div class="sub header">Událost</div>
						</div>
					</h1>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php
			if ($event["subheading"]) {
				?>
				<tr class="positive">
					<td class="collapsing"><i class="info icon"></i>Podtitul:</td>
					<td>
						<?= $event["subheading"] ?>
					</td>
				</tr>
				<?php
			}
			if ($regularEvent) {
				?>
				<tr>
					<td class="collapsing"><i class="retweet icon"></i>Pravidelná událost:</td>
					<td>
						<a href="<?= subLevelUrl(["regularEvent", "id" => $regularEvent["id"]]) ?>" class="ui gray small compact button"><?= $regularEvent["name"] ?></a>
					</td>
				</tr>
				<?php
			}
			?>
			<tr>
				<td class="collapsing"><i class="calendar icon"></i>Datum:</td>
				<td>
					<?php
					if ($event["status"] == 1)
						echo "<span class='ui red label'><i class='remove icon'></i> Zrušeno</span>";
					elseif ($event["status"] == 2)
						echo "<span class='ui grey label'><i class='question icon'></i> Nepotvrzeno</span>";
					elseif ($event["status"] == 3)
						echo "<span class='ui label'><i class='remove icon'></i> Neuskuteční se</span>";
					elseif ($event["regularStart"] && $event["start"] != $event["regularStart"]) {
						if (strtotime("midnight", $event["start"]) == strtotime("midnight", $event["regularStart"]))
							echo "<span class='ui orange label'><i class='time icon'></i> Změna začátku z " . date("G:i", $event["regularStart"]) . "</span>";
						else
							echo "<span class='ui orange label'><i class='right arrow icon'></i> Přesunuto z " . eventDateToString($event["regularStart"]) . "</span>";
					}

					echo "<span class='ui " . ($schedule ? "blue" : "teal") . " label'>" . eventDateToString($event["start"], $event["end"]) . "</span>";

					if ($regularEvent) {
						$q = dbQuery("SELECT id, since, until, regime, timeSince, timeUntil, days FROM regularEventsSchedules WHERE ( NOT deleted ) AND ( event = ? ) AND ( since <= ? ) AND ( ( until IS NULL ) OR ( until > ? ) )", $regularEvent["id"], $event["start"], $event["start"]);
						while ($r = $q->fetch())
							echo "<span class='ui tiny compact label'><i class=\"history icon\"></i>" . regimeStr($r) . "</span>";
					}

					if ($isEventOp && $event["status"] != 1 && $event["status"] != 3) {
						echo "<p>";
						$formData = getForm("changeEventStatus", [values => ["event" => $_GET["id"]]]);
						switch ($event["status"]) {

						case 0:
							unset($formData["elements"]["confirm"]);
							unset($formData["elements"]["decline"]);
							break;

						case 2:
							unset($formData["elements"]["cancel"]);
							break;

						}

						echo formHTML($formData);
						echo "</p>";
					}
					?>
				</td>
			</tr>
			<?php if (true) { ?>
				<tr>
					<td class="collapsing"><i class="spy icon"></i>Správci:</td>
					<td>
						<?= usersGroupsArrayLabels(array_unique(array_merge(usersGroupsSemicolonStrToArray($event["ops"]), usersGroupsSemicolonStrToArray($regularEvent["ops"])))) ?>
					</td>
				</tr>
				<?php
			}

			foreach ($attributes AS $r) {
				if ($r["visibility"] < 1 || !currentUserMeetsSemicolonUsersGroupStr($r["visibleTo"]))
					continue;

				$attrData = eventAttributeData($r["type"]);
				$displayVal =
					isset($attrData["interactiveDisplayValueTransformer"]) ?
						$attrData["interactiveDisplayValueTransformer"]($r, $_GET["id"]) :
						$attrData["displayValueTransformer"]($r["deducedValue"], $r["params"]);

				if ($displayVal == "")
					continue;
				?>
				<tr>
					<td class="collapsing">
						<?= ($r["icon"] ? "<i class='{$r["icon"]} icon'></i>" : "") . ($r["name"] ? $r["name"] . ":" : "") ?>
					</td>
					<td>
						<?= $displayVal ?>
					</td>
				</tr>
				<?php
			}

			if ($event["htmlNotes"]) {
				?>
				<tr>
					<td class="collapsing"><i class="sticky note icon"></i>Poznámky:</td>
					<td>
						<?= $event["htmlNotes"] ?>
					</td>
				</tr>
				<?php
			}
			?>
		</tbody>
	</table>

<?php
if ($isEventOp) {
	?>
	<div class="ui styled fluid accordion">
		<div class="title"><i class="write icon"></i>Upravit událost</div>
		<div class="content">
			<?php
			$form = eventEditForm($event, $regularEvent, $schedule, $attributes);
			$form["values"]["id"] = $_GET["id"];
			$form["redirect"] = currentLevelUrl_clearGET(["id" => "%resultData%"]);
			echo formHTML($form);
			?>
		</div>
		<?php
		if (!$event["schedule"]) {
			echo formHtml("deleteEvent", ["hidden" => "true", "values" => ["id" => $event["id"]], "redirect" => upLevelUrl()]);
			?>
			<div class="title"><i class="remove icon"></i>Smazání události</div>
			<div class="content">
				<button onclick='formSubmitConfirm("<?= formCSSID("deleteEvent") ?>", "Smazání události", "Opravdu smazat událost \"<?= $event["name"] ?? $regularEvent["name"] ?>\" <?= eventDateToString($event["start"], $event["end"]) ?>?", {id: "<?= $event["id"] ?>"} )' class='ui red button'>
					<i class='ban icon'></i>Smazat událost
				</button>
			</div>
			<?php
		}
		?>
		<div class="title"><i class="cubes icon"></i>Atributy</div>
		<div class="content">
			<a name="attributes" id="attributes"></a>
			<?php
			echo formHtml("deleteEventAttribute", ["hidden" => "true"]);

			dataTableHTML([
				"sourceArray" => $attributes,
				"columns" => [
					[
						"label" => "Název",
						"data" => "%name%",
						"sortColumn" => "name",
					],
					[
						"label" => "Typ",
						"data" => function ($r) {
							global $EVENT_ATTRIBUTE_TYPES;
							return $EVENT_ATTRIBUTE_TYPES[$r["type"]];
						},
						"collapsing" => true
					],
					[
						"label" => "Výchozí hodnota",
						"data" => function ($r) {
							return eventAttributeData($r["type"])["displayValueTransformer"]($r["defaultValue"], $r["params"]);
						}
					],
					[
						"data" => function ($r) {
							if ($r["isRegularEventAttribute"])
								return "<i>z pravidelné události</i>";

							$modalId = "editAttribute-{$r["id"]}";
							$r["event"] = $_GET["id"];

							$result = formModalHTML(eventAttributeForm($r["type"], $r), [
								"modalID" => $modalId,
								"modalHeader" => "<i class='cubes icon'></i>Úprava atributu",
								"id" => $r["id"]
							]);
							$result .= "<a onclick='$(\"#$modalId\").modal(\"show\")' class='ui tiny basic compact blue button'><i class='write icon'></i>Upravit</a>";
							$result .= " <button onclick = 'formSubmitConfirm(\"" . formCSSID("deleteEventAttribute") . "\", \"Smazání atributu\", \"Opravdu smazat atribut \\\"%name%\\\"?\", {id: \"%id%\"} )' class='ui tiny basic compact red button' ><i class='remove icon'></i>Smazat</button >";

							return $result;
						},
						"collapsing" => true
					]
				],
				"link" => "attributes",
				"emptyText" => "Žádné atributy.",
				"defaultSortColumn" => "name",
				"actionRow" => function () use ($isEventOp, $event) {
					global $EVENT_ATTRIBUTE_TYPES;
					$items = "";
					$forms = "";
					foreach ($EVENT_ATTRIBUTE_TYPES as $type => $desc) {
						$modalId = "addEventAttribute-$type";
						$items .= "<a class='item' onclick='$(\"#$modalId\").modal(\"show\");'>$desc</a>";
						$forms .= formModalHTML(eventAttributeForm($type, []), [
							"modalID" => $modalId,
							"modalHeader" => "<i class='cubes icon'></i>Přidat atribut – $desc",
							"values" => [
								"event" => $_GET["id"]
							]]);
					}

					return "
									$forms
									<div class='ui right floated green pointing dropdown button noSelect'>
										<i class='plus icon'></i> Přidat atribut
										<div class='menu'>
											$items
										</div>
									</div>
									";
				},
			]);
			?>
		</div>
	</div>
	<?php
}
?>

<?php
commentsSection("event", $event["schedule"] ? "{$event["regularEvent"]}__{$event["regularStart"]}" : "{$event["id"]}", ["header" => "Komentáře k události"]);