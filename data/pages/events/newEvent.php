<h1 class="ui header"><i class="calendar icon"></i>Nová událost</h1>
<?php
	require_once "$DATA_PATH/config/events.php";
	require_once "$DATA_PATH/config/eventAttributes.php";
	
	$start = $_GET["start"] ?? "xx";
	if( !is_numeric( $start ) ) {
		reportMessage( [ "type" => "error", "text" => "Neplatné počáteční datum." ] );
		return;
	}
	
	$identification = "fc;" . ($_GET["regularEvent"] ?? null) . ";$start";
	$eventData = getEventData( $identification );
	if( is_string( $eventData ) ) {
		reportMessage( [ "type" => "error", "text" => $eventData ] );
		return;
	}
	
	$event = $eventData["event"];
	$regularEvent = $eventData["regularEvent"];
	
	$attributes = eventAttributes( $event, $regularEvent );
	$form = eventEditForm(
			[
					"id" => $identification,
					"startDate" => $start,
					"startTime" => $regularEvent["defaultStartTime"] ?? null,
					"endTime" => $regularEvent["defaultEndTime"] ?? null,
					"ops" => $regularEvent ? "" : ";u" . loggedUserId() . ";"
			], $regularEvent, null, $attributes );
	
	if( $regularEvent ) {
		array_splice( $form["elements"], 0, 0, [ [
				"type" => "textLine",
				"label" => "Pravidelná událost",
				"defaultValue" => $regularEvent["name"]
		] ] );
	}
	
	$form["redirect"] = currentLevelUrl_clearAll( [ "event", "id" => "%resultData%" ] );
	echo formHTML( $form );