<?php
	$INCLUDED ?? false or die;
	
	require_once "$DATA_PATH/config/events.php";
	require_once "$DATA_PATH/config/eventAttributes.php";
?>
	<h1 class="ui header"><i class="sign language icon"></i>Přímluvné modlitby</h1>

	<div class="ui styled fluid accordion">
		<div class="title">
			<h5 class="ui header"><i class="help circle icon"></i>K čemu tato sekce slouží?</h5>
		</div>
		<div class="content">
			<?php
				contentText( "prayerSupport-longInfo" );
			?>
		</div>
	</div>

	<div class="ui vertically padded three column stackable equal width grid prayerSupportAnnouncements">
		<?php
			showAnnouncements( "prayerSupport", 0, [ "htmlBeforeItem" => "<div class='column'>", "htmlAfterItem" => "</div>", "itemStyle" => "height: 100%;", "showAddButton" => false ] );
		?>
	</div>

<?php
	if( userHasPrivilege( "prayerSupport" ) ) {
		addJSFile( "js/prayerSupport.js" );
		
		require_once "$DATA_PATH/config/prayerSupport.php";
		global $PRAYER_SUPPORT_FUTURE_WEEK_LIMIT, $PRAYER_SUPPORT_CALENDAR_EXTRA_DAYS;
		
		$thisWeek = weekStartTime( time() );
		$displayedWeek = weekStartTime( $_GET["week"] ?? time() );
		$displayedWeekEnd = modifiedDate( $displayedWeek, "+" . (7 + $PRAYER_SUPPORT_CALENDAR_EXTRA_DAYS) . " days" );
		$displayedWeekStart = modifiedDate( $displayedWeek, "-$PRAYER_SUPPORT_CALENDAR_EXTRA_DAYS days" );
		
		$mySchedule = dbQuery(
				"SELECT startTime, endTime FROM prayerSupportRecords WHERE ( startTime < ? ) AND ( endTime > ? ) AND ( user = ? ) ORDER BY startTime ASC",
				$displayedWeekEnd, $displayedWeekStart, loggedUserId()
		);
		$myScheduleItem = $mySchedule->fetch();
		$todayTime = dayStartTime( time() );
		
		echo formHTML( "updatePrayerSupportCalendar", [ "values" => [ "week" => $displayedWeek ] ] );
		?>

		<div class="ui modal" id="prayerSupportAddAnnouncementModal">
			<div class="header"><i class="announcement icon"></i>Přidat oznámení</div>
			<div class="content">
				<?= messagesHTML( "prayerSupportAddAnnouncementModal" ) ?>
				<?= formHtml( "editAnnouncement", [ "modalID" => "prayerSupportAddAnnouncementModal", "formAction" => "#", "elements" => [ "submit" => [ "label" => "Přidat oznámení" ] ], "values" => [ "sectionType" => "prayerSupport", "sectionId" => 0 ] ] ) ?>
			</div>
		</div>

		<a name="calendar"></a>
		<div class="ui inverted blue stackable menu">
			<a href="<?= currentLevelUrl( [ "week" => modifiedDate( $displayedWeek, "-14 days" ) ], "calendar" ) ?>" class="item"><i class="left double angle icon"></i></a>
			<a href="<?= currentLevelUrl( [ "week" => modifiedDate( $displayedWeek, "-7 days" ) ], "calendar" ) ?>" class="item"><i class="left angle icon"></i></a>
			<span class="item header"><?= "Po " . date( "j.n.", $displayedWeek ) . " &ndash; Ne " . date( "j.n.Y", modifiedDate( $displayedWeek, "+6 days" ) ) ?></span>
			<a href="<?= currentLevelUrl( [ "week" => modifiedDate( $displayedWeek, "+7 days" ) ], "calendar" ) ?>" class="item"><i class="right angle icon"></i></a>
			<a href="<?= currentLevelUrl( [ "week" => modifiedDate( $displayedWeek, "+14 days" ) ], "calendar" ) ?>" class="item"><i class="right double angle icon"></i></a>
			<a href="<?= currentLevelUrl( [ "week" => $thisWeek ], "calendar" ) ?>" class="item<?= $displayedWeek == $thisWeek ? " active" : "" ?>"><i class="home icon"></i>Aktuální týden</a>
			<?php
				if( userHasPrivilege( "prayerSupportMgmt" ) ) {
					echo "<div class='right menu'>";
					echo "<a onclick=\"$('#prayerSupportAddAnnouncementModal').modal('show');\" class=\"item\"><i class=\"announcement icon\"></i>Přidat oznámení</a>";
					echo "</div>";
				}
			?>
		</div>

		<div id="prayerSupportWrapper">
			<table class="ui small unstackable celled structured compact table" id="prayerSupportTable">
				<thead>
					<tr>
						<th class="center aligned collapsing"><i class="calendar icon"></i><i class="clock icon"></i></th>
						<?php
							$dayName = [ -2 => "So", -1 => "Ne", "Po", "Út", "St", "Čt", "Pá", "So", "Ne", "Po", "Út" ];
							
							for( $hour = 0; $hour < 24; $hour++ ) {
								echo "<th colspan='2'>" . str_pad( $hour, 2, "0", STR_PAD_LEFT ) . "</th>";
							}
							/*for( $i = 0; $i < 7; $i++ ) {
								echo "<th>" . $dayName[$i] . " " . date( "j.n.", $startTime + $i * 24 * 3600 ) . "</th>";
							}*/
						?>
					</tr>
				</thead>
				<tbody>
					<?php
						$records = dbQuery( "
							SELECT prayerSupportRecords.id AS id, user, startTime, endTime, showPseudonymInPrayerSupport, prayerSupportPseudonym
							FROM prayerSupportRecords
							LEFT JOIN users ON users.id = user
							WHERE ( startTime < ? ) AND ( endTime > ? )
							ORDER BY startTime ASC, user
							", $displayedWeekEnd, $displayedWeekStart )->fetchAll();
						
						$eventsData = eventsList( $displayedWeekStart, $displayedWeekEnd );
						$regularEvents = $eventsData["regularEvents"];
						$events = $eventsData["events"];
						foreach( $events as $event ) {
							if( !eventAttributeValue( $event, $regularEvents[$event["regularEvent"]], "showInPrayerSupp" ) )
								continue;
							
							$start = $event["start"];
							while( true ) {
								$end = min( $event["end"], strtotime( "midnight +1 days", $start ) );
								if( $end == $start )
									break;
								
								$startMidnight = strtotime( "midnight", $start );
								$endMidnight = strtotime( "midnight", $end );
								$rec = [
										"startTime" => $startMidnight + ceil( ($start - $startMidnight) / 1800 ) * 1800,
										"endTime" => $endMidnight + ceil( ($end - $endMidnight) / 1800 ) * 1800,
										"eventName" => $event["name"]
								];
								$records[] = $rec;
								
								$start = $end;
							}
						}
						
						usort( $records, function( $a, $b ) {
							if( $a["startTime"] == $b["startTime"] )
								return 0;
							elseif( $a["startTime"] < $b["startTime"] )
								return -1;
							else
								return 1;
						} );
						
						$recordI = 0;
						$recordCount = count( $records );
						$r = $records[0] ?? null;
						
						// Prepare row data
						for( $day = -$PRAYER_SUPPORT_CALENDAR_EXTRA_DAYS; $day < 7 + $PRAYER_SUPPORT_CALENDAR_EXTRA_DAYS; $day++ ) {
							$dayStart = modifiedDate( $displayedWeek, "+$day days" );
							$dayEnd = modifiedDate( $dayStart, "+1 days" );
							
							$rows = [ [] ];
							$rowEnds = [ 0 ];
							
							$isToday = $dayStart == $todayTime;
							$rowClasses = "";
							$rowClasses .= $isToday ? " today" : (($day < 0 || $day >= 7) ? " otherWeek" : "");
							
							$baseRowClasses = $rowClasses;
							if( $dayStart < $thisWeek )
								$baseRowClasses .= " -disabled";
							
							while( $recordI < $recordCount && $r["startTime"] < $dayEnd ) {
								$found = false;
								
								foreach( $rowEnds as $i => &$rowEnd ) {
									if( $rowEnd <= $r["startTime"] ) {
										$rowEnd = $r["endTime"];
										$rows[$i][] = $r;
										$found = true;
										break;
									}
								}
								
								if( !$found ) {
									$rowEnds[] = $r["endTime"];
									$rows[] = [ $r ];
								}
								
								$r = $records[++$recordI] ?? null;
							}
							
							{
								echo "<tr class='baseRow$baseRowClasses'>";
								echo "<td class='clockColumn collapsing' rowspan='" . (count( $rows ) + 1) . "'>" . $dayName[$day] . " " . date( "j.n.", $dayStart ) . "</td>";
								
								for( $hour = 0; $hour < 24; $hour += 0.5 ) {
									$hourVal = modifiedDate( $dayStart, "+" . ($hour * 60) . " minutes" );
									if( $myScheduleItem and $myScheduleItem["startTime"] <= $hourVal ) {
										echo "<td data-time='" . ($day * 24 + $hour) . "' class='selected'></td>";
										
										if( $myScheduleItem["endTime"] <= $hourVal + 3600 / 2 )
											$myScheduleItem = $mySchedule->fetch();
										
									} else
										echo "<td data-time='" . ($day * 24 + $hour) . "'></td>";
								}
								
								echo "</tr>";
							}
							
							foreach( $rows as $i => $row ) {
								$row[] = [ "startTime" => $dayEnd, "user" => 0, "endTime" => 0 ];
								
								if( $i != 0 )
									$rowClasses .= " -additionalRow";
								
								echo "<tr class='recordRow$rowClasses'>";
								
								$i = 0;
								for( $hour = 0; $hour < 24; $hour += 0.5 ) {
									$rec = $row[$i];
									$start = $rec["startTime"];
									$end = $rec["endTime"];
									
									$hourVal = date_create( "@$dayStart" )->modify( "+" . ($hour * 60) . " minutes" )->getTimestamp();
									
									if( $hourVal < $start )
										echo "<td></td>";
									elseif( $hourVal == $start ) {
										$timeText = $dayName[$day] . " " . date( "j.n. G:i", $start ) . "&ndash;" . date( "G:i", $rec["endTime"] );
										
										if( isset( $rec["eventName"] ) ) {
											echo "<td class='record' style='background-image: url(\"" . avatarImgSrc( $rec["eventName"] ) . "\");' colspan='" . floor( ($end - $start) / 3600 * 2 ) . "' data-time='{$timeText}' data-pseudonym='{$rec["eventName"]}' data-avatar=''><div></div></td>";
											
										} else {
											$timeText = $dayName[$day] . " " . date( "j.n. G:i", $start ) . "&ndash;" . date( "G:i", $rec["endTime"] );
											$pseudonym = $rec["showPseudonymInPrayerSupport"] ? $rec["prayerSupportPseudonym"] : "";
											$avatar = $pseudonym ? avatarImgSrc( preg_replace( "[^a-zA-Z]", "", $pseudonym ) ) : "";
											
											echo "<td class='record' style='background-image: url(\"" . avatarImgSrc( $pseudonym ? $pseudonym : crc32( $rec["user"] . "prayerSupport" ) ) . "\");' colspan='" . floor( ($end - $start) / 3600 * 2 ) . "' data-time='{$timeText}' data-pseudonym='$pseudonym' data-avatar='$avatar'><div></div></td>";
										}
									}
									
									if( $hourVal >= $start && $hourVal >= $end - 3600 / 2 )
										$i++;
								}
								
								echo "</tr>";
							}
						}
					
					?>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="49">
							<?php
								if( $displayedWeek >= $thisWeek && $displayedWeek <= date_create( "@$thisWeek" )->modify( "+$PRAYER_SUPPORT_FUTURE_WEEK_LIMIT weeks" )->getTimestamp() ) {
									echo "<button class=\"right floated ui primary submit button\" onclick=\"prayerSupportSaveChanges();\">Uložit změny</button>";
									echo "<button class=\"right floated ui submit button\" onclick='formSubmitConfirm(\"" . formCSSID( "updatePrayerSupportCalendar" ) . "\", \"Vyčištění kalendáře\", \"Opravdu vymazat modlitební program pro aktuální týden?\", {data: \"\"} )'>Vyčistit</button>";
								}
							?>
						</th>
					</tr>
				</tfoot>
			</table>
		</div>

		<div class="ui segment">
			<div class="ui fluid accordion">
				<div class="title">
					<h5 class="ui header"><i class="address card outline icon"></i>Nastavení pseudonymu</h5>
				</div>
				<div class="content">
					<?php
						global $USER_DATA;
						echo messagesHTML( "setupPrayerSupportPseudonym" );
						echo formHtml( "setupPrayerSupportPseudonym", [ "values" => $USER_DATA, "msgGroup" => "setupPrayerSupportPseudonym" ] );
					?>
				</div>
			</div>
		</div>

		<div class="ui green piled segment">
			<?php
				contentText( "prayerSupport-shortInfo" );
			?>
		</div>
		<?php
		commentsSection( "prayerSupport", $displayedWeek, [ "header" => "Komentáře k tomuto týdnu" ] );
	}
	
	addModalsContent( "
		<div class=\"ui popup\" id=\"prayerSupportPopup\">
			<h4 class=\"ui header\">
				" . avatarHtml( 0 ) . "
				<div class=\"content\">
					<span class=\"displayName\"></span>
					<div class=\"sub header displayTime\"></div>
				</div>
			</h4>
		</div>"
	);
?>