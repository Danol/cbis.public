<?php $INCLUDED ?? false or die; ?>
	<h1 class="ui dividing header">
		<i class="bug icon"></i>
		<div class="content">
			Zpětná vazba
			<div class="sub header">Zde můžete sdělovat své nápady, poznatky a hlásit chyby.</div>
		</div>
	</h1>
<?php
	commentsSection( "feedback", 0, [ "header" => "" ] );
?>