-- phpMyAdmin SQL Dump
-- version 4.4.15.1
-- http://www.phpmyadmin.net
--
-- Počítač: wm92.wedos.net:3306
-- Vytvořeno: Čtv 27. pro 2018, 18:30
-- Verze serveru: 5.6.23
-- Verze PHP: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `d109558_cbis`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `announcements`
--

CREATE TABLE IF NOT EXISTS `announcements` (
  `id` int(11) NOT NULL,
  `lastEdit` bigint(20) NOT NULL,
  `orderId` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `formattedText` text COLLATE utf8_unicode_ci NOT NULL,
  `rawText` text COLLATE utf8_unicode_ci NOT NULL,
  `sectionType` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `sectionId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `associations`
--

CREATE TABLE IF NOT EXISTS `associations` (
  `type` enum('groupOp','groupMember') COLLATE utf8_unicode_ci NOT NULL,
  `arg1` bigint(20) NOT NULL,
  `arg2` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL,
  `topicType` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `topicId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `replyTo` int(11) DEFAULT NULL,
  `deepReplyTo` int(11) DEFAULT NULL,
  `user` int(11) NOT NULL,
  `timeSent` bigint(20) NOT NULL,
  `formattedText` text COLLATE utf8_unicode_ci NOT NULL,
  `rawText` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL,
  `regularEvent` int(11) DEFAULT NULL,
  `commonId` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `schedule` int(11) DEFAULT NULL,
  `start` bigint(20) NOT NULL,
  `end` bigint(20) NOT NULL,
  `regularStart` int(11) DEFAULT NULL,
  `attributesVersion` bigint(20) NOT NULL COMMENT 'For regular events, it is equal to regularStart; for non-regular ones, it is equal to creation time',
  `name` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subheading` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ops` text COLLATE utf8_unicode_ci,
  `rawNotes` text COLLATE utf8_unicode_ci,
  `htmlNotes` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - confirmed, 1 - cancelled, 2 - unsure, 3 - confirmed not happening',
  `deleted` int(11) NOT NULL DEFAULT '0' COMMENT '&1 = event deleted, &2 = schedule shortened, &3 = regular event deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `eventsAttributes`
--

CREATE TABLE IF NOT EXISTS `eventsAttributes` (
  `id` int(11) NOT NULL,
  `event` int(11) NOT NULL,
  `commonId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `visibility` int(11) NOT NULL DEFAULT '2' COMMENT '0 = nowhere; 1 - event page; 2 - event page + popup; 3 - event page + popup + calendar ',
  `visibleTo` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Struktura tabulky `eventsRegularAttributes`
--

CREATE TABLE IF NOT EXISTS `eventsRegularAttributes` (
  `id` int(11) NOT NULL,
  `event` int(11) NOT NULL,
  `attribute` int(11) NOT NULL,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `googleEventSync`
--

CREATE TABLE IF NOT EXISTS `googleEventSync` (
  `googleEventId` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `localEventId` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `start` bigint(20) NOT NULL,
  `end` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `privileges` text COLLATE utf8_unicode_ci NOT NULL,
  `opPrivileges` text COLLATE utf8_unicode_ci NOT NULL,
  `publicMembership` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL,
  `actorId` int(11) NOT NULL,
  `timestamp` bigint(20) NOT NULL,
  `subjectType` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subjectId` int(11) DEFAULT NULL,
  `actionType` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `additionalData` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `notificationActors`
--

CREATE TABLE IF NOT EXISTS `notificationActors` (
  `notificationId` int(11) NOT NULL,
  `actorId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL,
  `targetUser` int(11) NOT NULL,
  `subjectType` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `subjectId` bigint(20) NOT NULL,
  `firstTime` bigint(20) NOT NULL,
  `lastTime` bigint(20) NOT NULL,
  `isRead` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `prayerSupportRecords`
--

CREATE TABLE IF NOT EXISTS `prayerSupportRecords` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `startTime` bigint(20) NOT NULL,
  `endTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `regularEvents`
--

CREATE TABLE IF NOT EXISTS `regularEvents` (
  `id` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `commonId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ops` text COLLATE utf8_unicode_ci NOT NULL,
  `defaultStartTime` int(11) DEFAULT NULL,
  `defaultEndTime` int(11) DEFAULT NULL,
  `created` bigint(20) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `regularEventsAttributes`
--

CREATE TABLE IF NOT EXISTS `regularEventsAttributes` (
  `id` int(11) NOT NULL,
  `regularEvent` int(11) NOT NULL,
  `type` varchar(16) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `regularEventsAttributesHistory`
--

CREATE TABLE IF NOT EXISTS `regularEventsAttributesHistory` (
  `id` int(11) NOT NULL,
  `validSince` bigint(20) NOT NULL,
  `validUntil` bigint(20) DEFAULT NULL,
  `commonId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `defaultValue` text COLLATE utf8_unicode_ci NOT NULL,
  `visibility` int(11) NOT NULL DEFAULT '2' COMMENT '0 = nowhere; 1 - event page; 2 - event page + popup; 3 - event page + popup + calendar ',
  `visibleTo` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Struktura tabulky `regularEventsSchedules`
--

CREATE TABLE IF NOT EXISTS `regularEventsSchedules` (
  `id` int(11) NOT NULL,
  `event` int(11) NOT NULL,
  `since` bigint(20) NOT NULL,
  `until` bigint(20) DEFAULT NULL,
  `regime` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `timeSince` int(11) DEFAULT NULL,
  `timeUntil` int(11) DEFAULT NULL,
  `days` int(11) NOT NULL,
  `defaultStatus` tinyint(4) NOT NULL COMMENT 'Same values as events.status',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'bits; 0 = not deleted, &1 = schedule deleted, &2 = event deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `shortName` varchar(16) COLLATE utf8_czech_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `texts`
--

CREATE TABLE IF NOT EXISTS `texts` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `rawText` text COLLATE utf8_unicode_ci NOT NULL,
  `formattedText` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `displayName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `privileges` text COLLATE utf8_unicode_ci NOT NULL,
  `registered` bigint(20) NOT NULL,
  `lastActive` bigint(20) DEFAULT NULL,
  `banState` enum('none','deleted') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  `showPseudonymInPrayerSupport` tinyint(1) NOT NULL DEFAULT '0',
  `prayerSupportPseudonym` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectionType` (`sectionType`,`sectionId`,`orderId`) USING BTREE;

--
-- Klíče pro tabulku `associations`
--
ALTER TABLE `associations`
  ADD PRIMARY KEY (`type`,`arg1`,`arg2`) USING BTREE,
  ADD KEY `type_2` (`type`,`arg2`) USING BTREE;

--
-- Klíče pro tabulku `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `replyTo` (`replyTo`) USING BTREE,
  ADD KEY `topicType` (`topicType`,`topicId`) USING BTREE,
  ADD KEY `deepReplyTo` (`deepReplyTo`);

--
-- Klíče pro tabulku `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deleted` (`deleted`,`start`,`end`,`regularEvent`),
  ADD KEY `regularEvent` (`regularEvent`,`regularStart`) USING BTREE;

--
-- Klíče pro tabulku `eventsAttributes`
--
ALTER TABLE `eventsAttributes`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `eventsRegularAttributes`
--
ALTER TABLE `eventsRegularAttributes`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `googleEventSync`
--
ALTER TABLE `googleEventSync`
  ADD PRIMARY KEY (`googleEventId`),
  ADD KEY `start` (`start`),
  ADD KEY `end` (`end`);

--
-- Klíče pro tabulku `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `actionType` (`actionType`),
  ADD KEY `actorId` (`actorId`);

--
-- Klíče pro tabulku `notificationActors`
--
ALTER TABLE `notificationActors`
  ADD UNIQUE KEY `notificationId` (`notificationId`,`actorId`);

--
-- Klíče pro tabulku `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `targetUser` (`targetUser`,`lastTime`) USING BTREE;

--
-- Klíče pro tabulku `prayerSupportRecords`
--
ALTER TABLE `prayerSupportRecords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `startTime` (`startTime`),
  ADD KEY `user` (`user`);

--
-- Klíče pro tabulku `regularEvents`
--
ALTER TABLE `regularEvents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deleted` (`deleted`);

--
-- Klíče pro tabulku `regularEventsAttributes`
--
ALTER TABLE `regularEventsAttributes`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `regularEventsAttributesHistory`
--
ALTER TABLE `regularEventsAttributesHistory`
  ADD KEY `id` (`id`,`validSince`,`validUntil`);

--
-- Klíče pro tabulku `regularEventsSchedules`
--
ALTER TABLE `regularEventsSchedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deleted` (`deleted`,`event`,`since`,`until`) USING BTREE;

--
-- Klíče pro tabulku `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `texts`
--
ALTER TABLE `texts`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`) USING BTREE;

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `eventsAttributes`
--
ALTER TABLE `eventsAttributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `eventsRegularAttributes`
--
ALTER TABLE `eventsRegularAttributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `prayerSupportRecords`
--
ALTER TABLE `prayerSupportRecords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `regularEvents`
--
ALTER TABLE `regularEvents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `regularEventsAttributes`
--
ALTER TABLE `regularEventsAttributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `regularEventsSchedules`
--
ALTER TABLE `regularEventsSchedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
