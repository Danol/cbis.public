<?php
	$INCLUDED = true;
	$DATA_PATH = __DIR__ . "/data";
	$PAGE_LOAD_TIME_START = microtime( true );
	
	require_once "ext/engine/engine.php";
	
	if( $CURRENT_PAGE_DATA["ajax"] ?? false ) {
		// Remove all url data because of links
		global $URL, $URL_DATA, $TOP_URL;
		$URL = [];
		$URL_DATA = [];
		$TOP_URL = "";
		
		include "$DATA_PATH/pages/{$CURRENT_PAGE_DATA["file"]}.php";
		return;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">

		<title><?= callIfCallable( $CURRENT_PAGE_DATA["label"], $_GET ) ?> – CBIS</title>

		<link rel="stylesheet" type="text/css" href="ext/semantic-ui/semantic.css">
		<link rel="stylesheet" type="text/css" href="ext/calendar/calendar.min.css">
		<link rel="stylesheet" type="text/css" href="ext/engine/css/main.css">
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="stylesheet" type="text/css" href="css/admin.css">

		<script src="ext/jquery/jquery-3.2.1.js"></script>
		<script src="ext/engine/js/init.js"></script>
		<script src="ext/semantic-ui/semantic.min.js"></script>
	</head>
	<body>
		<div class="ui stackable menu" id="mainMenu">
			<div class="header item">CBIS</div>
			<?php
				function mainMenuHasPrivilegesForItem( $item ) {
					global $PAGES;
					
					if( is_string( $item ) )
						return userHasPrivileges( $PAGES[$item]["privileges"] ?? [] );
					else {
						foreach( $item["items"] as $subitem )
							if( mainMenuHasPrivilegesForItem( $subitem ) )
								return true;
						
						return false;
					}
				}
				
				function mainMenuItemIsSelected( $item ) {
					global $URL;
					
					if( is_string( $item ) ) {
						return ($URL[0] ?? 0) == $item || ($_GET[0] ?? "") == $item;
					} else {
						/*foreach( $item["items"] as $subitem )
							if( mainMenuItemIsSelected( $subitem ) )
								return true;*/
						
						return false;
					}
				}
				
				function mainMenu( $items ) {
					global $PAGES, $URL;
					
					foreach( $items as $item ) {
						if( !mainMenuHasPrivilegesForItem( $item ) )
							continue;
						
						if( is_string( $item ) ) {
							$data = $PAGES[$item];
							$showLabel = $data["showLabel"] ?? true;
							?>
							<a class="item<?= (mainMenuItemIsSelected( $item ) ? " active" : "") . ($showLabel ? "" : " icon") ?>" href="<?= absoluteUrl( $item ) ?>"><?php
									if( $data["icon"] ?? "" )
										echo "<i class='{$data["icon"]}'></i>\n";
									
									if( $showLabel )
										echo $data["label"];
								?></a>
							<?php
						} elseif( is_array( $item ) ) {
							?>
							<div class="ui simple dropdown<?= mainMenuItemIsSelected( $item ) ? " active" : "" ?> item">
								<?php
									if( $item["icon"] ?? "" )
										echo "<i class='{$item["icon"]}'></i>\n";
									
									echo $item["label"];
								?>
								<i class="dropdown icon"></i>
								<div class="menu">
									<?php mainMenu( $item["items"] ); ?>
								</div>
							</div>
							<?php
						}
					}
				}
				
				mainMenu( $MENU_PAGES );
				
				if( isLoggedIn() ) {
					echo formHTML( "logout", [ "formStyle" => "display: none;" ] );
					?>
					<div class="right menu">
						<a class="ui item" id="menuNotificationsButton">
							<i class="alarm icon"></i>
							<?php
								$unreadNotificationCount = getUnreadNotificationCount();
								if( $unreadNotificationCount )
									echo "<span class=\"ui left pointing red label\" id=\"menuNotificationsCount\">$unreadNotificationCount</span>";
							?>
						</a>
						<div class="ui very wide popup" id="menuNotificationsMenu">
							<div class="ui dimmer">
								<div class='ui loader'></div>
							</div>
						</div>
						<div class="ui simple dropdown item">
							<?= avatarHtml( loggedUserId(), "ui avatar image" ) ?>
							<b><?= $USER_DATA["displayName"] ?></b>
							<i class="dropdown icon"></i>

							<div class="ui menu">
								<a href="<?= absoluteUrl( [ "user", "id" => loggedUserId() ] ) ?>" class="item"><i class="id card outline icon"></i>Profil</a>
								<a href="<?= absoluteUrl( "userPanel" ) ?>" class="item"><i class="user icon"></i>Uživatelský panel</a>
								<a class="item" onclick="$('#<?= formCSSID( "logout" ) ?>').submit();"><i class="sign out icon"></i>Odhlásit se</a>
							</div>
						</div>
					</div>
					<?php
				}
			?>
		</div>
		
		<?php
			if($CURRENT_PAGE_DATA["uiContainer"] ?? true) {
		?>
		<div class="main ui container" id="contentContainer">
			<?php
				if( count( $URL_DATA ) > 1 ) {
					?>
					<a href="<?= upLevelUrl() ?>" class="ui small compact button"><i class="arrow left icon"></i>Zpět</a>
					<div class="ui breadcrumb">
						<?php
							$i = 0;
							foreach( $PAGES_DATA as $page ) {
								if( $i > 0 )
									echo "<span class='divider'>/</span>";
								
								$label = callIfCallable( $page["label"] ?? "", $URL_DATA[$i] );
								
								if( $i == count( $PAGES_DATA ) - 1 )
									echo "<div class='active section'>$label</div>";
								else
									echo "<a href='" . absoluteUrl( ...array_slice( $URL_DATA, 0, $i + 1 ) ) . "' class='section'>$label</a>";
								
								$i++;
							}
						?>
					</div>
					<?php
				}
				}
				
				
				if( $CURRENT_PAGE_DATA["defaultShowMessages"] ?? true )
					echo messagesHTML();
				
				call_user_func( function() {
					global $DATA_PATH, $CURRENT_PAGE_DATA;
					
					$INCLUDED = true;
					
					try {
						include "$DATA_PATH/pages/{$CURRENT_PAGE_DATA["file"]}.php";
					} catch( Exception $e ) {
						reportMessage( [ "type" => "error", "text" => $e->getMessage() /*. "<br>" . str_replace( "\n", "<br>", $e->getTraceAsString() )*/ ] );
					}
				} );
				
				if($CURRENT_PAGE_DATA["uiContainer"] ?? true) {
				echo messagesHTML();
			?>
		</div>
	<?php
		}
		
		echo messagesHTML();
	?>

		<div class="ui tiny modal" id="confirmModal">
			<div class="header"></div>
			<div class="content">
				<p></p>
			</div>
			<div class="actions">
				<div class="ui cancel button">Storno</div>
				<div class="ui orange button" onclick="formSubmitConfirmAction()">Potvrdit</div>
			</div>
		</div>
		
		<?php
			global $MODALS_CONTENT;
			echo $MODALS_CONTENT;
		?>

		<script src="js/main.js"></script>
		<script src="ext/calendar/calendar.min.js"></script>
		<script src="ext/engine/js/main.js"></script>
		<script src="ext/engine/js/submitModals.js"></script>
		<?php
			foreach( $JS_FILES as $file )
				echo "<script src='$file'></script>";
		?>
		<script>$(document).ready(function () {<?= $JS_ONREADY?>});</script>

		<div style="position: fixed; right: 5px; bottom: 5px; color: rgba(0,0,0,0.3); font-size: 10px;">Stránka byla vygenerována za <?= round( microtime( true ) - $PAGE_LOAD_TIME_START, 4 ); ?> s</div>
	</body>
</html>