<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"fromPOST" => function( $data, $formData, $elementName, $skipErrors ) {
				$elementData = $formData["elements"][$elementName];
				
				if( !isset( $elementData["items"][$data] ) )
					throw new InvalidValueException( [
							"errorElement" => $elementName,
							"error" => "Neplatná hodnota pole '{$elementData["label"]}'."
					] );
				
				return $data;
			},
	];