<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"fromPOST" => function( $data, $formData, $elementName, $skipErrors ) {
				if( !is_numeric( $data ) )
					throw new InvalidValueException( [
							"errorElement" => $elementName,
							"error" => "Hodnota pole '{$formData["elements"][$elementName]["label"]}' není číslo."
					] );
				
				$elementData = $formData["elements"][$elementName];
				if( !$skipErrors && isset( $elementData["minValue"] ) && $data < $elementData["minValue"] )
					throw new InvalidValueException( [
							"errorElement" => $elementName,
							"error" => "Hodnota pole '{$formData["elements"][$elementName]["label"]}' nesmí být menší jak {$elementData["minValue"]}."
					] );
				
				if( !$skipErrors && isset( $elementData["maxValue"] ) && $data > $elementData["maxValue"] )
					throw new InvalidValueException( [
							"errorElement" => $elementName,
							"error" => "Hodnota pole '{$formData["elements"][$elementName]["label"]}' nesmí být větší jak {$elementData["maxValue"]}."
					] );
				
				return $data;
			},
	];