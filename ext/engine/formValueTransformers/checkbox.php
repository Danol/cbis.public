<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"fromPOST" => function( $data ) {
				return empty( $data ) ? 0 : 1;
			},
			"toPOST" => function( $data ) {
				return $data ? "on" : "";
			}
	];