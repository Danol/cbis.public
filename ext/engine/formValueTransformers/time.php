<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"fromPOST" => function( $data, $formData, $elementName ) {
				if( !trim( $data ) && ($formData["elements"][$elementName]["allowNull"] ?? false) )
					return null;
				
				$m = [];
				if( !preg_match( "/^(0?[0-9]|1[0-9]|2[0123]):(0?[0-9]|[1-5][0-9])$/", $data, $m ) )
					throw new InvalidValueException( [
							"errorElement" => $elementName,
							"error" => "Hodnota pole '{$formData["elements"][$elementName]["label"]}' není platný čas."
					] );
				
				return $m[1] * 60 + $m[2];
			},
			"toPOST" => function( $data ) {
				if( is_null( $data ) )
					return null;
				
				return dayTimeToStr( $data );
			}
	];