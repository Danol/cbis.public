<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"fromPOST" => function( $data, $formData, $elementName ) {
				if( !trim( $data ) && ($formData["elements"][$elementName]["allowNull"] ?? false) )
					return null;
				
				$result = DateTime::createFromFormat( 'j. n. Y', $data );
				if( !$result )
					throw new InvalidValueException( [
							"errorElement" => $elementName,
							"error" => "Hodnota pole '{$formData["elements"][$elementName]["label"]}' není platné datum."
					] );
				
				$result->modify( "midnight" );
				return $result->getTimestamp();
			},
			"toPOST" => function( $data ) {
				if( !trim( $data ) )
					return null;
				
				return date( "c", $data );
			}
	];