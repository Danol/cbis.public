<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"fromPOST" => function( $data ) {
				return htmlspecialchars( $data, ENT_HTML401 | ENT_QUOTES );
			}
	];