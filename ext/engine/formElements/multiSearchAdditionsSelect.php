<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => function( $elementData, $value ) {
				if( is_string( $value ) )
					$value = [];
				
				$result = "
					<div class='field %fieldClasses%'>
						%label%
						<div class='ui fluid multiple search selection dropdown %classes%' data-allowadditions='true'>
							<input type='hidden' name='%name%' value='" . implode( ",", $value ) . "'>
							<i class='dropdown icon'></i>
							<div class='default text'></div>
							<div class='menu'>";
				
				$items = $elementData["items"];
				if( is_callable( $items ) )
					$items = $items();
				
				foreach( $items as $item )
					$result .= "<div data-value='$item' class='item'>$item</div>";
				
				$result .= "</div></div></div>";
				
				return $result;
			},
			"defaultValueTransformers" => [
					[
							"fromPOST" => function( $data, $formData, $elementName ) {
								if( trim( $data ) == "" )
									return [];
								
								return explode( ",", $data );
							}
					]
			]
	];