<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => "
				<div class=\"field %fieldClasses%\">
					%label%
					<div class='ui time calendar'>
						<div class='ui input left icon'>
							<i class='time icon'></i>
							<input type='text' placeholder='%placeholder%' value='%value%' name='%name%' />
						</div>
					</div>
				</div>",
			"defaultValueTransformers" => [ "time" ],
			"defaultClasses" => ""
	];