<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => function( $elementData, $value ) {
				$result = "<div class='field %fieldClasses%'>%label%<div class='ui selection dropdown %classes%'><input name='%name%' type='hidden' value='$value'><i class='dropdown icon'></i><div class='default text'>%placeholder%</div><div class='menu'>";
				
				$items = $elementData["items"];
				if( is_callable( $items ) )
					$items = $items();
				
				foreach( $items as $key => $desc )
					$result .= "<div class='item' data-value='$key'>$desc</div>";
				
				$result .= "</div></div></div>";
				
				return $result;
			},
			"defaultValueTransformers" => [ "valueInItems" ]
	];