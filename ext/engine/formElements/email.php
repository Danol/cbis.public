<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => "<div class=\"field %fieldClasses%\">%label%<input type='email' name='%name%' placeholder='%placeholder%' value='%value%' id='%id%' class='%classes%'/></div>",
			"defaultValueTransformers" => "htmlSpecialChars",
			"defaultClasses" => "ui input"
	];