<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => "<div class=\"field %fieldClasses%\">%label%<div class='ui input %classes%'><input type='text' name='%name%' placeholder='%placeholder%' value='%value%' id='%id%'/></div></div>",
			"defaultValueTransformers" => [ "htmlSpecialChars", "trim" ],
			"defaultClasses" => ""
	];