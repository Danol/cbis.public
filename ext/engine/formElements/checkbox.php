<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => function( $elementData, $value ) {
				return "<div class=\"field %fieldClasses%\"><div class='ui checkbox %classes%' id='%id%'><input type='checkbox' class='hidden' tabindex='0' name='%name%'" . ($value ? " checked=''" : "") . "/>%label%</div></div>";
			},
			"defaultValueTransformers" => [ "checkbox" ],
			"defaultClasses" => ""
	];