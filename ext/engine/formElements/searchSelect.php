<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => function( $elementData, $value ) {
				$result = "
					<div class='field %fieldClasses%'>
						%label%
						<div class='ui fluid search selection dropdown %classes%'>
							<input type='hidden' name='%name%' value='%value%'>
							<i class='dropdown icon'></i>
							<div class='default text'></div>
							<div class='menu'>";
				
				$items = $elementData["items"];
				if( is_callable( $items ) )
					$items = $items();
				
				foreach( $items as $val => $desc )
					$result .= "<div class='item' data-value='$val'>$desc</div>";
				
				$result .= "</div></div></div>";
				
				return $result;
			},
			"defaultValueTransformers" => [ "valueInItems" ]
	];