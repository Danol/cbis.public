<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => function( $elementData, $value ) {
				return "
					<div class=\"field %fieldClasses%\">
						<div class='ui radio checkbox %classes%' id='%id%'>
							<input type='radio' tabindex='0' name='{$elementData['group']}' value='%name%' " . ($value ? " checked=''" : "") . "/>
							%label%
						</div>
					</div>";
			},
			"defaultValueTransformers" => [ "htmlspecialchars" ],
			"defaultClasses" => ""
	];