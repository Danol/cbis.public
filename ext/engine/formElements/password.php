<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => "<div class=\"field %fieldClasses%\">%label%<input type='password' name='%name%' placeholder='%placeholder%' value='%value%' id='%id%' class='%classes%'/></div>",
			"refillOnSend" => false
	];