<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => "
				<div class=\"field %fieldClasses%\">
					%label%
					<div class='ui datetime calendar %classes%'>
						<div class='ui input left icon'>
							<i class='calendar icon'></i>
							<input type='text' placeholder='%placeholder%' value='%value%' name='%name%' />
						</div>
					</div>
				</div>",
			"defaultValueTransformers" => [ "dateTimeFormat" ],
			"defaultClasses" => ""
	];