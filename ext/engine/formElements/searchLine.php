<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => function( $elementData, $value ) {
				$result = "
					<div class='field %fieldClasses%'>
						%label%
						<div class='ui search selection dropdown %classes%' data-allowadditions='true'>
								<input name='%name%' type='hidden' value='$value'>
								<i class='dropdown icon'></i>
								<div class='text'></div>
								<div class='menu'>";
				
				$items = $elementData["items"];
				if( is_callable( $items ) )
					$items = $items();
				
				foreach( $items as $key )
					$result .= "<div class='item' data-value='$key'>$key</div>";
				
				$result .= "</div></div></div>";
				
				return $result;
			}
	];