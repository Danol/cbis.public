<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => function( $elementData, $value ) {
				$result = "
					<div class='field %fieldClasses%'>
						%label%
						<div class='ui fluid search selection dropdown %classes%' data-allowadditions='true'>
							<input type='hidden' name='%name%' value='%value%'>
							<i class='dropdown icon'></i>
							<div class='default text'></div>
							<div class='menu'>";
				
				$items = $elementData["items"];
				if( is_callable( $items ) )
					$items = $items();
				
				foreach( $items as $item )
					$result .= "<div class='item' data-value='$item'>$item</div>";
				
				if( array_search( $value, $items ) === false )
					$result .= "<div class='item' data-value='$value'>$value</div>";
				
				$result .= "</div></div></div>";
				
				return $result;
			}
	];