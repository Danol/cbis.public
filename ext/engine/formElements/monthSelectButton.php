<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => "
				<input type='hidden' name='%name%' value='%value%'/>
				<div class='ui monthSelectButton calendar'>
					<div id='%id%' class='ui button %classes% %fieldClasses%'>%labelValue%</div>
				</div>",
			"defaultClasses" => "primary"
	];