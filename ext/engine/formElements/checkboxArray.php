<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => function( $elementData, $value ) {
				$result = "<div class='grouped fields'>%label%";
				$val = explode( $elementData["itemSeparator"], $value );
				
				foreach( $elementData["items"] as $key => $desc ) {
					if( is_numeric( $key ) && ($elementData["numericKeysAsGroups"] ?? false) ) {
						$result .=
								"<h4 class=\"ui horizontal divider header\">" .
								$desc .
								"</h4>";
						continue;
					}
					
					$checked = in_array( $key, $val ) ? " checked" : "";
					$result .=
							"<div class='field'><div class='ui$checked toggle checkbox'>" .
							"<input type='checkbox' name='%name%__$key'$checked/>" .
							"<label>$desc</label>" .
							"</div></div>";
				}
				
				$result .= "</div>";
				
				return $result;
			},
			"defaultValueTransformers" => [
					[
							"fromPOST" => function( $data, $formData, $elementName ) {
								$elementData = array_merge_form( getFormElement( $formData["elements"][$elementName]["type"] ), $formData["elements"][$elementName] );
								$result = [];
								
								foreach( $elementData["items"] as $key => $desc ) {
									if( is_numeric( $key ) && ($elementData["numericKeysAsGroups"] ?? false) )
										continue;
									
									if( isset( $_POST[formElementPOSTName( $elementName ) . "__" . $key] ) )
										$result[] = $key;
								}
								
								return implode( $elementData["itemSeparator"], $result );
							}
					]
			],
			
			"itemSeparator" => ";"
	];