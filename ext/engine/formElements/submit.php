<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => "<button type='submit' id='%id%' name='%name%' class='ui submit button %classes% %fieldClasses%'>%labelValue%</button>",
			"defaultClasses" => "right floated primary",
			"defaultValueTransformers" => [
				[
						"fromPOST" => function( $data, $formData, $elementName ) {
							return isset( $_POST[formElementPOSTName( $elementName )] );
						}
				]
		],
	];