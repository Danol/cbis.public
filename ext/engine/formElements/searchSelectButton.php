<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => function( $elementData, $value ) {
				$items = $elementData["items"];
				
				$result = "
					<div class='field %fieldClasses%'>
						%label%
						<div class='ui tiny compact search labeled icon dropdown button %classes%' data-forceselection='false'>
							<input type='hidden' name='%name%' value='$value'>
							<i class='" . ($elementData["icon"] ?? "dropdown") . " icon'></i>
							<div class='default text'>%placeholder%</div>
							<div class='menu'>";
				
				if( is_callable( $items ) )
					$items = $items();
				
				foreach( $items as $key => $desc )
					$result .= "<div class='item' data-value='$key'>$desc</div>";
				
				$result .= "</div></div></div>";
				
				return $result;
			},
			"defaultValueTransformers" => [ "valueInItems" ]
	];