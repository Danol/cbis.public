<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => function( $elementData, $value ) {
				if( is_string( $value ) )
					$value = [];
				
				$items = $elementData["items"];
				$result = "
					<div class='field %fieldClasses%'>
						%label%
						<div class='ui fluid multiple search selection dropdown %classes%'>
							<input type='hidden' name='%name%' value='" . implode( ",", $value ?? [] ) . "'>
							<i class='dropdown icon'></i>
							<div class='default text'></div>
							<div class='menu'>";
				
				if( is_callable( $items ) )
					$items = $items();
				
				foreach( $items as $key => $desc )
					$result .= "<div class='item' data-value='$key'>$desc</div>";
				
				$result .= "</div></div></div>";
				
				return $result;
			},
			"defaultValueTransformers" => [
					[
							"fromPOST" => function( $data, $formData, $elementName ) {
								if( trim( $data ) == "" )
									return [];
								
								return explode( ",", $data );
							}
					]
			]
	];