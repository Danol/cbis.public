<?php
	$INCLUDED ?? false or die;
	
	$DATA = [
			"html" => "<div class=\"field %fieldClasses%\">%label%<textarea name='%name%' placeholder='%placeholder%' id='%id%' class='%classes%'>%value%</textarea>%htmlAfterInside%</div>",
			"defaultValueTransformers" => [ "htmlSpecialChars", "trim" ],
			"defaultClasses" => "ui input",
			"placeholder" => ""
	];