<?php
	$INCLUDED ?? false or die;
	
	$ENGINE_PATH = __DIR__;
	$LOADED_MODULES = [];
	
	session_start();
	
	function requireModules( ...$modules ) {
		global $LOADED_MODULES, $CURRENTLY_LOADED_MODULE;
		
		foreach( $modules as $moduleName )
			($LOADED_MODULES[$moduleName] ?? false) or die( "Module '$CURRENTLY_LOADED_MODULE' requires module '$moduleName' loaded first." );
	}
	
	function isModuleLoaded( string $module ): bool {
		global $LOADED_MODULES;
		return $LOADED_MODULES[$module] ?? false;
	}
	
	require_once "$DATA_PATH/config.php";
	
	foreach( $ENGINE_MODULES as $module ) {
		$LOADED_MODULES[$module] = true;
		$CURRENTLY_LOADED_MODULE = $module;
		
		if( substr( $module, 0, 1 ) == ":" )
			require_once "$DATA_PATH/modules/" . substr( $module, 1 ) . ".php";
		else
			require_once "$ENGINE_PATH/modules/$module.php";
	}
	
	unset( $CURRENTLY_LOADED_MODULE );
	
	class InvalidValueException extends Exception {
		public $data;
		
		public function __construct( $data ) {
			parent::__construct( $data["error"] ?? "Neplatná hodnota pole" );
			$this->data = $data;
		}
	}