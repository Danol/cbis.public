formSubmitConfirmAction = function () {
};

function formSubmitConfirm(formId, title, question, formElementValues) {
	$("#confirmModal > .header").html(title);
	$("#confirmModal > .content > p").html(question);
	$("#confirmModal").modal("show");

	for (var index in formElementValues || {})
		$("#" + formId + "__" + index).val(formElementValues[index]);

	formSubmitConfirmAction = function () {
		$("#" + formId).submit();
	}
}

function formSubmit(formId, formElementValues) {
	for (var index in formElementValues || {})
		$("#" + formId + "__" + index).val(formElementValues[index]);

	$("#" + formId).submit();
}