$(document).ready(function () {
	$('.ui.dropdown:not(.simple):not(.custom)').each(function () {
		var self = $(this);
		var settings = {
			fullTextSearch: true,
			match: "text"
		};

		if (self.attr("data-allowadditions") === "true") {
			settings["allowAdditions"] = true;
			settings["hideAdditions"] = false;
		}

		if (self.attr("data-forceselection") === "false") {
			settings["forceSelection"] = false;
		}

		if (self.hasClass("submitOnChange")) {
			settings["onChange"] = function () {
				self.closest("form").submit();
			}
		}

		if (self.hasClass("noSelect"))
			settings["action"] = "hide";

		self.dropdown(settings);
	});
	$('.ui.accordion').each(function () {
		var self = $(this);

		if (self.parent().closest(".ui.accordion").length)
			return;

		self.accordion({
			onChange: function () {
				$(this).closest(".ui.modal").modal("refresh");
			}
		});
	});
	$('.ui.checkbox').checkbox();
	$('.ui.modal').each(function () {
		this.modalParent = $(this).parent();
		$(this).modal();
	});

	mouseLeftButtonIsDown = false;

	$(document).mousedown(function (e) {
		if (e.which === 1)
			mouseLeftButtonIsDown = true;
	});
	$(document).mouseup(function (e) {
		if (e.which === 1)
			mouseLeftButtonIsDown = false;
	});

	$('textarea').keydown(function (e) {
		if (e.ctrlKey && e.keyCode == 13)
			$(this).closest("form").submit();
	});

	if (location.hash && location.hash.match(/^#[a-zA-Z0-9_]+$/)) {
		var func = function (self) {
			var acc = self.parent();
			if (acc.hasClass("modal")) {
				var p = acc[0].modalParent;

				if (p.hasClass("content"))
					func(p);

				p.parents(".content").each(function () {
					func($(this));
				});
				return;
			}

			if (!acc.hasClass("accordion"))
				return;

			console.debug(self.html());
			acc.accordion("open", acc.find(".content").index(self));
		};
		if ($(location.hash).length) {
			$(location.hash).parents(".content").each(function () {
				func($(this));
			});
		}

		if (location.hash !== "") {
			var sel = $(location.hash);
			if (sel.count)
				sel.get(0).scrollIntoView();
		}
	}

	var calendarText = {
		days: ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
		months: ['Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec'],
		monthsShort: ['Led', 'Úno', 'Bře', 'Dub', 'Kvě', 'Čer', 'Čvc', 'Srp', 'Zář', 'Říj', 'Lis', 'Pro'],
		today: 'Dnes',
		now: 'Nyní'
	};
	var calendarFormatter = {
		date: function (date, settings) {
			return date.getDate() + ". " + (date.getMonth() + 1) + ". " + date.getFullYear();
		}
	};
	$(".ui.date.calendar").each(function(){
		var data = {
			firstDayOfWeek: 1,
			ampm: false,
			text: calendarText,
			formatter: calendarFormatter,
			type: "date",
			today: true,
			selector: {
				popup: '.ui.popup',
				input: 'input',
				activator: 'input'
			}
		};

		if( $(this).hasClass("startDate") )
			data["endCalendar"] = $(this).closest("form").find(".endDate");

			if( $(this).hasClass("endDate") )
			data["startCalendar"] = $(this).closest("form").find(".startDate");

		$(this).calendar(data);
	});
	$(".ui.datetime.calendar").calendar({
		firstDayOfWeek: 1,
		ampm: false,
		text: calendarText,
		formatter: calendarFormatter,
		today: true,
		selector: {
			popup: '.ui.popup',
			input: 'input',
			activator: 'input'
		}
	});
	$(".ui.time.calendar").calendar({
		text: calendarText,
		ampm: false,
		type: "time",
		today: true
	});
	$(".ui.monthSelectButton.calendar").each(function () {
		$(this).calendar({
			ampm: false,
			text: calendarText,
			type: "month",
			today: true,
			onChange: function (date, text, mode) {
				$(this).prev().val(Math.floor(date / 1000));
				$(this).closest("form").submit();
			}
		});
		$(this).calendar("set date", new Date($(this).prev().val() * 1000), true, false);
	});
	$(".ui.daySelectButton.calendar").each(function () {
		$(this).calendar({
			firstDayOfWeek: 1,
			ampm: false,
			text: calendarText,
			type: "date",
			today: true,
			onChange: function (date, text, mode) {
				$(this).prev().val(Math.floor(date / 1000));
				$(this).closest("form").submit();
			}
		});
		$(this).calendar("set date", new Date($(this).prev().val() * 1000), true, false);
	});
});

function formCSSID(formName, id) {
	return "#" + "form__" + formName + "__" + (id || "");
	//return "#" + crc32("form__" + formName + "__" + (id || "")).toString(16);
}

function formElementsCSSID(formName, elementName, id) {
	return formCSSID(formName, id) + "__" + elementName;
}