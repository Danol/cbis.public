<?php
	function paginationItemHTML( int $page, int $currentPage, string $pageUrlIndex ) {
		?>
		<a href="<?= currentLevelUrl( [ $pageUrlIndex => $page ] ) ?>" class="ui <?= ($page == $currentPage) ? "teal " : "" ?>label"><?= $page ?></a>
		<?php
	}
	
	function paginationCurrentPage( int $pageCount, string $id, int $defaultPage = 1 ) {
		return max( min( $_GET["page$id"] ?? $defaultPage, $pageCount ), 1 );
	}
	
	function paginationHTML( int $pageCount, string $id = "", array $additionalData = [] ) {
		global $TOP_URL_DATA;
		
		$pageUrlIndex = "page$id";
		$currentPage = paginationCurrentPage( $pageCount, $id );
		?>
		<div class="<?= $additionalData["classes"] ?? "" ?>">
			<?php
				if( $currentPage > 1 ) {
					?>
					<a href="<?= currentLevelUrl( [ $pageUrlIndex => 1 ] ) ?>" class="ui label"><i class="angle double left icon"></i></a>
					<a href="<?= currentLevelUrl( [ $pageUrlIndex => $currentPage - 1 ] ) ?>" class="ui label"><i class="angle left icon"></i></a>
					<?php
				}
				
				for( $i = 1; $i <= min( $pageCount, 3 ); $i++ )
					paginationItemHTML( $i, $currentPage, $pageUrlIndex );
				
				if( $i < $currentPage - 2 )
					echo '<span class="ui label">...</span>';
				
				for( $i = max( $i, $currentPage - 2 ); $i <= min( $pageCount, $currentPage + 2 ); $i++ )
					paginationItemHTML( $i, $currentPage, $pageUrlIndex );
				
				if( $i < $pageCount - 2 )
					echo '<div class="ui label">...</div>';
				
				for( $i = max( $i, $pageCount - 2 ); $i <= $pageCount; $i++ )
					paginationItemHTML( $i, $currentPage, $pageUrlIndex );
				
				
				if( $currentPage < $pageCount ) {
					?>
					<a href="<?= currentLevelUrl( [ $pageUrlIndex => $currentPage + 1 ] ) ?>" class="ui label"><i class="angle right icon"></i></a>
					<a href="<?= currentLevelUrl( [ $pageUrlIndex => $pageCount ] ) ?>" class="ui label"><i class="angle double right icon"></i></a>
					<?php
				}
			?>
		</div>
		<?php
	}