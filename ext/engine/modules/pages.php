<?php
	$INCLUDED ?? false or die;
	requireModules( "url" );
	
	require_once "$DATA_PATH/pages.php";
	
	call_user_func( function() {
		global $URL, $PAGE_404, $PAGE_HOME, $PAGES, $CURRENT_PAGE_DATA, $PAGES_DATA;
		
		$PAGES_DATA = [];
		$CURRENT_PAGE_DATA = [];
		
		if( !($URL[0] ?? "") )
			$URL[0] = $PAGE_HOME;
		
		foreach( $URL as $i => $urlKey ) {
			$CURRENT_PAGE_DATA = $PAGES[$urlKey] ?? $PAGE_404;
			if( isset( $CURRENT_PAGE_DATA["link"] ) )
				$CURRENT_PAGE_DATA = $PAGE_404;
			
			if( !isset( $CURRENT_PAGE_DATA["file"] ) )
				$CURRENT_PAGE_DATA["file"] = $urlKey;
			
			$PAGES_DATA[] = $CURRENT_PAGE_DATA;
		}
	} );