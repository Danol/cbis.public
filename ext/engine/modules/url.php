<?php
	$INCLUDED ?? false or die;
	
	function genViewUID(): string {
		return bin2hex( random_bytes( 4 ) );
	}
	
	function subUrlFromArray( $data ) {
		if( !is_array( $data ) )
			return $data;
		
		
		$result = "";
		foreach( $data as $i => $val ) {
			if( $i === 0 )
				continue;
			
			$result .= ($result == "") ? "?" : "&";
			
			if( !is_numeric( $i ) )
				$result .= "$i=";
			
			$result .= $val;
		}
		
		return ($data[0] ?? "") . $result;
	}
	
	function absoluteUrlAdv( $args, $anchor = "" ) {
		return "?/" . implode( "/", array_map( "subUrlFromArray", $args ) ) . ($anchor ? "#$anchor" : "");
	}
	
	function absoluteUrl( ... $args ) {
		return absoluteUrlAdv( $args );
	}
	
	function currentLevelUrl( array $additionalData, $link = "" ) {
		global $URL_DATA;
		
		return absoluteUrlAdv( array_merge(
				array_slice( $URL_DATA, 0, -1 ),
				[ array_merge( $_GET, $additionalData ) ]
		), $link );
	}
	
	function currentLevelUrl_clearGET( array $additionalData, $link = "" ) {
		global $URL_DATA;
		
		return absoluteUrlAdv( array_merge(
				array_slice( $URL_DATA, 0, -1 ),
				[ array_merge( [ $_GET[0] ], $additionalData ) ]
		), $link );
	}
	
	function currentLevelUrl_clearAll( array $additionalData, $link = "" ) {
		global $URL_DATA;
		
		return absoluteUrlAdv( array_merge(
				array_slice( $URL_DATA, 0, -1 ),
				[ $additionalData ]
		), $link );
	}
	
	function subLevelUrl( $data ) {
		global $URL_DATA;
		
		if( is_string( $data ) )
			$data = [ 0 => $data ];
		
		return absoluteUrl( ...array_merge(
				$URL_DATA,
				[ $data ]
		) );
	}
	
	function upLevelUrl() {
		global $URL_DATA;
		return absoluteUrl( ...array_slice( $URL_DATA, 0, -1 ) );
	}
	
	function urlViewUID( string $url, string $uid ) {
		$matches = [];
		if( !preg_match( "/^\\?([0-9a-z]+\\:)?\\/(.*)$/", $url, $matches ) )
			return $url;
		
		return "?{$uid}:/{$matches[2]}";
	}
	
	call_user_func( function() {
		global $URL, $URL_DATA, $TOP_URL, $URL_VIEW_UID;
		
		$URL = [];
		$URL_VIEW_UID = "";
		
		$URL_DATA = [];
		
		$_GET = [];
		$TOP_URL = "";
		
		$queryString = htmlspecialchars_decode( $_SERVER["QUERY_STRING"] );
		$data = [];
		if( !preg_match( "/^(?:([0-9a-z]+)\\:)?\\/(.*)$/", $queryString, $data ) )
			return;
		
		$URL_VIEW_UID = $data[1];
		
		foreach( explode( "/", $data[2] ) as $subQueryStr ) {
			$data = explode( "?", $subQueryStr );
			$urlBase = preg_replace( "/[^a-z-A-Z]/", "", $data[0] );
			$subData = [];
			
			if( isset( $data[1] ) )
				parse_str( $data[1], $subData );
			
			$subData = array_merge( [ $urlBase ], $subData );
			
			$URL[] = $urlBase;
			$URL_DATA[] = $subData;
			
			$TOP_URL = $urlBase;
			$_GET = $subData;
		}
	} );