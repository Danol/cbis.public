<?php
	$INCLUDED ?? false or die;
	
	function callIfCallable( $arg, ...$params ) {
		return is_callable( $arg ) ? $arg( ...$params ) : $arg;
	}
	
	function capitalize( $str ) {
		return mb_convert_case( $str, MB_CASE_TITLE, 'UTF-8' );
	}
	
	function decapitalize( $str ) {
		return mb_convert_case( mb_substr( $str, 0, 1 ), MB_CASE_LOWER, 'UTF-8' ) . mb_substr( $str, 1 );
	}