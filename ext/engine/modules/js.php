<?php
	$INCLUDED ?? false or die;
	
	call_user_func( function() {
		global $JS_ONREADY, $JS_FILES;
		$JS_ONREADY = "";
		$JS_FILES = [];
	} );
	
	function addJSonready( $code ) {
		global $JS_ONREADY;
		$JS_ONREADY .= "\n$code";
	}
	
	function addJSFile( $file ) {
		global $JS_FILES;
		$JS_FILES[] = $file;
	}