<?php
	$INCLUDED ?? false or die;
	requireModules( "db" );
	
	call_user_func( function() {
		global $USER_DATA, $IS_LOGGED_IN;
		$USER_DATA = [];
		$IS_LOGGED_IN = false;
		
		if( !isset( $_SESSION["userId"] ) )
			return;
		
		$q = dbQuery( "SELECT id, displayName, email, registered, showPseudonymInPrayerSupport, prayerSupportPseudonym FROM users WHERE ( id = ? ) AND ( banState = 'none' ) LIMIT 1", $_SESSION["userId"] );
		
		if( !$q->rowCount() )
			return;
		
		$IS_LOGGED_IN = true;
		$USER_DATA = $q->fetch( PDO::FETCH_ASSOC );
		dbQuery( "UPDATE users SET lastActive = ? WHERE id = ?", time(), $USER_DATA["id"] );
	} );
	
	function isLoggedIn() {
		global $IS_LOGGED_IN;
		return $IS_LOGGED_IN;
	}
	
	function loggedUserId() {
		return $_SESSION["userId"] ?? -1;
	}
	
	/**
	 * @param $email
	 * @param $password
	 * Returns null if successfull, error message otherwise
	 */
	function tryLogin( $email, $password ) {
		$q = dbQuery( "SELECT id, password, banState FROM users WHERE LOWER( email ) = LOWER( ? )", $email );
		if( !$q->rowCount() )
			return "invalidEmail";
		
		$r = $q->fetch();
		if( $r["banState"] != "none" )
			return "accountDeleted";
		
		if( !password_verify( $password, $r["password"] ) )
			return "wrongPassword";
		
		$_SESSION["userId"] = $r["id"];
		return null;
	}
	
	function logout() {
		unset( $_SESSION["userId"] );
	}
	
	function isValidEmail( $email ) {
		return filter_var( $email, FILTER_VALIDATE_EMAIL ) != false;
	}