<?php
	$INCLUDED ?? false or die;
	requireModules( "url" );
	
	call_user_func( function() {
		global $UNREPORTED_MESSAGES, $URL_VIEW_UID;
		
		$UNREPORTED_MESSAGES = [];
		
		if( !isset( $_SESSION["messages"] ) )
			$_SESSION["messages"] = [];
		
		if( isset( $_SESSION["messages"][$URL_VIEW_UID] ) ) {
			foreach( $_SESSION["messages"][$URL_VIEW_UID] as $msg ) {
				$UNREPORTED_MESSAGES[$msg["group"]][] = $msg;
			}
			unset( $_SESSION["messages"][$URL_VIEW_UID] );
		}
	} );
	
	function reportMessage( $data, $implicitData = [] ) {
		global $UNREPORTED_MESSAGES;
		$defaultData = [ "type" => "info", "header" => "", "text" => "", "group" => "" ];
		
		if( is_string( $data ) )
			$data = array_merge( $defaultData, [ "text" => $data ], $implicitData );
		else
			$data = array_merge( $defaultData, $implicitData, $data );
		
		if( isset( $data["viewUID"] ) ) {
			if( !isset( $_SESSION["messages"][$data["viewUID"]] ) )
				$_SESSION["messages"][$data["viewUID"]] = [];
			
			$_SESSION["messages"][$data["viewUID"]][] = $data;
		} else {
			if( !isset( $UNREPORTED_MESSAGES[$data["group"]] ) )
				$UNREPORTED_MESSAGES[$data["group"]] = [];
			
			$UNREPORTED_MESSAGES[$data["group"]][] = $data;
		}
	}
	
	if( !is_callable( "messagesHTML" ) ) {
		function messagesHTML( string $group = "" ) {
			global $UNREPORTED_MESSAGES;
			
			$result = "";
			
			foreach( $UNREPORTED_MESSAGES[$group] ?? [] as $msg ) {
				$result .=
						"<div class='ui " . ($msg["type"] ?? "") . " message messageGroup-$group'>" //
					//. "<i class='close icon'></i>" //
				;
				
				if( $msg["header"] ?? "" )
					$result .= "<div class='header'>{$msg["header"]}</div>";
				
				$result .= $msg["text"] . "</div>";
			}
			
			unset( $UNREPORTED_MESSAGES[$group] );
			return $result;
		}
	}