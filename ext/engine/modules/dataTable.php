<?php
	$INCLUDED ?? false or die;
	requireModules( "pagination", "forms", "db" );
	
	function dataTableQuery( array $data, array $params ) {
		$query = $data["query"];
		
		if( $params["sortColumn"] ?? "" ) {
			$order = ($params["sortDesc"] ? "DESC" : "ASC");
			$query .= " ORDER BY " . str_replace( [ "%ORDER%", "%INVORDER%" ], [ $order, $order == "DESC" ? "ASC" : "DESC" ], $params["sortColumn"] ) . " " . $order;
		}
		
		if( isset( $params["limit"] ) )
			$query .= " LIMIT {$params["limit"][0]}, {$params["limit"][1]}";
		
		return dbQuery( $query, ...$data["queryArgs"] ?? [] );
	}
	
	function dataTableHTML( array $data ) {
		$tableId = $data["id"] ?? "";
		$sourceArray = $data["sourceArray"] ?? null;
		
		$itemsPerPage = $data["itemsPerPage"] ?? 32;
		$sortColumn = (isset( $_GET["sort$tableId"] ) ? $data["columns"][$_GET["sort$tableId"]]["sortColumn"] ?? null : null) ?? ($data["defaultSortColumn"] ?? "");
		$sortDesc = ($_GET["dir$tableId"] ?? ($data["defaultSortDir"] ?? "asc")) == "desc";
		$link = $data["link"] ?? "";
		
		$recordCount = isset( $sourceArray ) ? count( $sourceArray ) : dataTableQuery( $data, [] )->rowCount();
		$pageCount = ceil( $recordCount / $itemsPerPage );
		$currentPage = paginationCurrentPage( $pageCount, $tableId );
		
		?>
		<table class="ui very compact striped table">
			<thead>
				<tr>
					<?php
						foreach( $data["columns"] as $i => $col ) {
							$classes = "top aligned";
							
							if( $col["collapsing"] ?? false )
								$classes .= " collapsing";
							
							echo " <th class='$classes' > ";
							echo $col["label"] ?? "";
							
							if( isset( $col["sortColumn"] ) ) {
								echo "<span style='display: block; margin-top: 4px;'></span>";
								
								$scol = $col["sortColumn"];
								
								if( $sortColumn == $scol && !$sortDesc )
									echo " <span class='ui mini teal label' > &#11205;</span>";
								else
									echo "<a href='" . currentLevelUrl( [ "sort$tableId" => $i, "dir$tableId" => "asc" ], $link ) . "' class='ui basic mini label'>&#11205;</a>";
								
								if( $sortColumn == $scol && $sortDesc )
									echo "<span class='ui mini teal label'>&#11206;</span>";
								else
									echo "<a href='" . currentLevelUrl( [ "sort$tableId" => $i, "dir$tableId" => "desc" ], $link ) . "' class='ui basic mini label'>&#11206;</a>";
							}
							
							echo "</th>";
						}
					?>
				</tr>
			</thead>
			<tbody>
				<?php
					if( isset( $sourceArray ) ) {
						usort( $sourceArray, function( $a, $b ) use ( $sortDesc, $sortColumn ) {
							$descMult = $sortDesc == "desc" ? -1 : 1;
							$a = $a[$sortColumn];
							$b = $b[$sortColumn];
							
							if( $a == $b )
								return 0;
							elseif( $a > $b )
								return $descMult;
							else
								return -$descMult;
						} );
						
						
						$sourceArrayI = ($currentPage - 1) * $itemsPerPage;
						$sourceArrayIEnd = min( $sourceArrayI + $itemsPerPage, $recordCount );
						$fetchFunction = function() use ( &$sourceArrayI, $sourceArrayIEnd, $sourceArray ) {
							if( $sourceArrayI < $sourceArrayIEnd )
								return $sourceArray[$sourceArrayI++];
							else
								return null;
						};
					} else {
						$q = dataTableQuery( $data, [ "limit" => [ ($currentPage - 1) * $itemsPerPage, $itemsPerPage ],
								"sortColumn" => $sortColumn,
								"sortDesc" => $sortDesc ] );
						
						$fetchFunction = function() use ( $q ) {
							return $q->fetch();
						};
					}
					
					while( $r = $fetchFunction() ) {
						if( isset( $data["rowPositive"] ) && $data["rowPositive"]( $r ) )
							echo "<tr class='positive'>";
						else
							echo "<tr>";
						
						foreach( $data["columns"] as $col ) {
							$colData = $col["data"] ?? "";
							
							if( is_callable( $colData ) )
								$colData = $colData( $r );
							
							$colData = str_replace(
									array_map( function( $x ) {
										return "%$x%";
									}, array_keys( $r ) ),
									array_values( $r ),
									$colData
							);
							
							$classes = "";
							
							if( $col["collapsing"] ?? false )
								$classes .= "collapsing ";
							
							if( $col["centered"] ?? false )
								$classes .= "center aligned ";
							
							echo "<td class='$classes'>$colData</td>";
						}
						echo "</tr>";
					}
					if( !$recordCount ) {
						?>
						<tr>
							<td colspan="<?= count( $data["columns"] ) ?>">
								<i><?= $data["emptyText"] ?? "Žádné položky." ?></i>
							</td>
						</tr>
						<?php
					}
				?>
			</tbody>
			<?php
				$actionRow = $data["actionRow"] ?? "";
				if( is_callable( $actionRow ) )
					$actionRow = $actionRow();
				
				if( $pageCount > 1 || $actionRow ) {
					?>
					<tfoot>
						<tr>
							<th colspan="<?= count( $data["columns"] ) ?>">
								<?php
									echo $actionRow;
									
									if( $pageCount > 1 )
										paginationHTML( $pageCount, $tableId );
								?>
							</th>
						</tr>
					</tfoot>
					<?php
				}
			?>
		</table>
		<?php
	}