<?php
	$INCLUDED ?? false or die;
	requireModules( "url", "messages", "js" );
	
	function isSequentialArray( $arr ) {
		foreach( $arr as $key => $value )
			if( !is_numeric( $key ) )
				return false;
		
		return true;
	}
	
	function array_merge_form( $srcArray, ...$arrays ) {
		$result = $srcArray;
		
		foreach( $arrays as $array ) foreach( $array as $key => $value ) {
			if( isset( $result[$key] ) && is_array( $result[$key] ) && is_array( $value ) && !isSequentialArray( $value ) )
				$result[$key] = array_merge_form( $result[$key], $value );
			else
				$result[$key] = $value;
		}
		
		return $result;
	}
	
	function isValidFormName( string $name ): bool {
		return preg_match( "/^[a-zA-Z0-9_\\-\\/]+$/", $name );
	}
	
	function getForm( string $formName, array $additionalData = [] ): array {
		global $DATA_PATH, $INCLUDED;
		
		if( !isValidFormName( $formName ) )
			die( "Invalid form name: $formName" );
		
		$filePath = "$DATA_PATH/forms/$formName.php";
		if( !file_exists( $filePath ) )
			die( "Form does not exist: $formName" );
		
		$DATA = [];
		include $filePath;
		
		if( isset( $DATA["privileges"] ) and isModuleLoaded( "privileges" ) and !userHasPrivileges( $DATA["privileges"] ) ) {
			//reportMessage( [ "type" => "error", "text" => "Na použití formuláře '$formName' nemáte oprávnění." ] );
			$DATA = [ "elements" => [], "action" => function( $data ) {
				return "PERMISSIONS NEEDED";
			} ];
		}
		
		$DATA = array_merge_form( $DATA, [ "name" => $formName, "id" => "" ], $additionalData );
		return $DATA;
	}
	
	function formUID( string $formName, string $id = "" ): string {
		return "$formName::$id";
	}
	
	function formCSSID( string $formName, string $id = "" ): string {
		//return substr( bin2hex( crc32( "form__{$formName}__$id" ) ), 0, 6 );
		//return bin2hex( crc32( "form__{$formName}__$id" ) );
		return "form__{$formName}__$id";
	}
	
	function formElementCSSID( string $formName, string $elementName, string $id = "" ): string {
		return formCSSID( $formName, $id ) . "__$elementName";
	}
	
	function formElementPOSTName( string $elementName ): string {
		return "formData_$elementName";
	}
	
	function formGetElementValueTransformersFromFormData( array $formData, string $elementName ) {
		if( !isset( $formData["elements"][$elementName] ) )
			die( "Form {$formData["name"]} does not contain element $elementName" );
		
		$elementData = array_merge_form( getFormElement( $formData["elements"][$elementName]["type"] ), $formData["elements"][$elementName] );
		return formGetElementValueTransformersFromElementData( $elementData );
	}
	
	function formGetElementValueTransformersFromElementData( array $elementData ) {
		$sources = [ $elementData["defaultValueTransformers"] ?? null, $elementData["valueTransformers"] ?? null ];
		
		$transformers = [];
		
		foreach( $sources as $source ) {
			if( is_string( $source ) )
				$transformers[] = $source;
			else if( is_array( $source ) )
				$transformers = array_merge( $transformers, $source );
		}
		
		return $transformers;
	}
	
	function transformFormValueFromPOSTFromFormData( array $formData, string $elementName, $data, $skipErrors = false ) {
		$transformers = formGetElementValueTransformersFromFormData( $formData, $elementName );
		foreach( $transformers as $transformer ) {
			$transformerData = getFormValueTransformer( $transformer );
			if( isset( $transformerData["fromPOST"] ) )
				$data = $transformerData["fromPOST"]( $data, $formData, $elementName, $skipErrors );
		}
		
		return $data;
	}
	
	function transformFormValueToPOST( array $formData, string $elementName, $data, $skipErrors = false ) {
		$transformers = array_reverse( formGetElementValueTransformersFromFormData( $formData, $elementName ) );
		foreach( $transformers as $transformer ) {
			$transformerData = getFormValueTransformer( $transformer );
			if( isset( $transformerData["toPOST"] ) )
				$data = $transformerData["toPOST"]( $data, $formData, $elementName, $skipErrors );
		}
		
		return $data;
	}
	
	function getFormElement( string $elementType ) {
		global $FORM_ELEMENT_CACHE, $ENGINE_PATH, $DATA_PATH, $INCLUDED;
		
		$matches = "";
		if( !preg_match( "/^(:?)([a-zA-Z\\/0-9]+)?$/", $elementType, $matches ) )
			die( "Invalid form element type name: $elementType" );
		
		if( isset( $FORM_ELEMENT_CACHE[$elementType] ) )
			return $FORM_ELEMENT_CACHE[$elementType];
		
		$filePath = (($matches[1] == ":") ? $DATA_PATH : $ENGINE_PATH) . "/formElements/{$matches[2]}.php";
		if( !file_exists( $filePath ) )
			die( "Element form type does not exist: $elementType" );
		
		$DATA = [];
		include $filePath;
		$FORM_ELEMENT_CACHE[$elementType] = $DATA;
		return $DATA;
	}
	
	function getFormValueTransformer( $transformer ) {
		if( is_array( $transformer ) )
			return $transformer;
		
		global $FORM_VALUE_TRANSFORMER_CACHE, $ENGINE_PATH, $DATA_PATH, $INCLUDED;
		
		$m = [];
		if( !preg_match( "/^(:?)([a-zA-Z\\/0-9]+)$/", $transformer, $m ) )
			die( "Invalid form transformer type: $transformer" );
		
		if( isset( $FORM_VALUE_TRANSFORMER_CACHE[$transformer] ) )
			return $FORM_VALUE_TRANSFORMER_CACHE[$transformer];
		
		$filePath = ($m[1] ? $DATA_PATH : $ENGINE_PATH) . "/formValueTransformers/{$m[2]}.php";
		if( !file_exists( $filePath ) )
			die( "Form value transformer does not exist: $transformer" );
		
		$DATA = [];
		include $filePath;
		$FORM_ELEMENT_CACHE[$transformer] = $DATA;
		return $DATA;
	}
	
	function formBeginHTML( array $formData ): string {
		$uid = genViewUID();
		
		if( $formData["hidden"] ?? false )
			$formData["formStyle"] = ($formData["formStyle"] ?? "") . "display: none;";
		
		$redirect = $formData["redirect"] ?? null;
		if( is_callable( $redirect ) )
			$redirect = $redirect();
		
		if( is_null( $redirect ) )
			$redirectUrl = "?" . $_SERVER["QUERY_STRING"];
		elseif( $redirect == "" )
			$redirectUrl = "?/";
		else
			$redirectUrl = $redirect;
		
		$redirectUrl = urlViewUID( $redirectUrl, $uid );
		
		$suffix = "";
		if( !($formData["autocomplete"] ?? true) )
			$suffix = " autocomplete='off'";
		
		$result =
				"<form class='" . ($formData["baseFormClass"] ?? "ui form") . " " . (($formData["inline"] ?? false) ? " inline" : "") . " " . ($formData["formClass"] ?? "") . "'" . (isset( $formData["formStyle"] ) ? " style='{$formData["formStyle"]}'" : "") . " method='post' id='" . formCSSID( $formData["name"], $formData["id"] ) . "' action='" . ($formData["formAction"] ?? ("#" . formCSSID( $formData["name"], $formData["id"] ))) . "' $suffix>\n"
				. "<input type='hidden' name='form_submit' value='true'>\n"
				. "<input type='hidden' name='form_name' value='{$formData["name"]}'>\n"
				. "<input type='hidden' name='form_id' value='{$formData["id"]}'>\n"
				. "<input type='hidden' name='form_viewUID' value='{$uid}'>\n"
				. "<input type='hidden' name='form_redirect' value='{$redirectUrl}'>\n"
				. "<input type='hidden' name='form_token' value='{$_SESSION["formToken"]}'>\n";
		
		if( isset( $formData["msgGroup"] ) )
			$result .= "<input type='hidden' name='form_msgGroup' value='{$formData["msgGroup"]}'>\n";
		
		if( isset( $formData["modalID"] ) )
			$result .= "<input type='hidden' name='form_modalID' value='{$formData["modalID"]}'>\n";
		
		if( $formData["inline"] ?? false )
			$result .= "<div class='middle aligned inline fields' style='display: inline-block; margin: 0;'>\n";
		
		if( $formData["htmlInsideBegin"] ?? "" )
			$result .= $formData["htmlInsideBegin"];
		
		return $result;
	}
	
	function formEndHTML( array $formData ) {
		$result = "";
		
		$result .= $formData["suffixHTML"] ?? "";
		
		if( !($formData["hidden"] ?? false) )
			$result .= "<div style='clear: both;'></div>";
		
		$result .= "</form>\n";
		
		if( $formData["htmlInsideEnd"] ?? "" )
			$result .= $formData["htmlInsideEnd"];
		
		if( $formData["inline"] ?? false )
			$result .= "</div>\n";
		
		return $result;
	}
	
	function formElementHTML( array $formData, string $elementName ) {
		global $FORM_SUBMIT_UID, $FORM_ERROR_ELEMENTS;
		
		if( !isset( $formData["elements"][$elementName] ) )
			die( "Form {$formData["name"]} does not contain element $elementName" );
		
		// If the item is just html
		if( is_string( $formData["elements"][$elementName] ) )
			return $formData["elements"][$elementName];
		
		$elementData = array_merge_form( getFormElement( $formData["elements"][$elementName]["type"] ), $formData["elements"][$elementName] );
		$elementData["id"] = formElementCSSID( $formData["name"], $elementName, $formData["id"] );
		
		$POSTName = formElementPOSTName( $elementName );
		
		$value = $elementData["defaultValue"] ?? null;
		$moreFieldClasses = $elementData["fieldClasses"] ?? "";
		
		if( $FORM_SUBMIT_UID == formUID( $formData["name"], $formData["id"] ) ) {
			if( $elementData["refillOnSend"] ?? true ) {
				try {
					$value = transformFormValueFromPOSTFromFormData( $formData, $elementName, $_POST[$POSTName] ?? null, true );
				} catch( InvalidValueException $e ) {
					$value = null;
				}
			}
			
			if( $FORM_ERROR_ELEMENTS[$elementName] ?? false )
				$moreFieldClasses .= " error";
			
		} elseif( isset( $formData["values"][$elementName] ) ) {
			$value = $formData["values"][$elementName];
		}
		
		try {
			$value = transformFormValueToPOST( $formData, $elementName, $value, true );
		} catch( InvalidValueException $e ) {
			$value = null;
		}
		
		if( ($formData["inline"] ?? false) && isset( $elementData["inlineHtml"] ) )
			$htmlBase = $elementData["inlineHtml"];
		else
			$htmlBase = $elementData["html"];
		
		return ($elementData["htmlBefore"] ?? "") . str_replace(
						[
								"%name%",
								"%id%",
								"%label%",
								"%labelValue%",
								"%value%",
								"%classes%",
								"%fieldClasses%",
								"%placeholder%",
								"%htmlAfterInside%"
						],
						[
								$POSTName,
								$elementData["id"],
								(isset( $elementData["label"] ) && !($formData["inline"] ?? false) ? "<label>{$elementData["label"]}</label>" : "") . ($elementData["htmlAfterLabel"] ?? ""),
								$elementData["label"] ?? "",
								!is_array( $value ) ? $value : "",
								($elementData["classes"] ?? ($elementData["defaultClasses"] ?? "")) . " " . ($elementData["moreClasses"] ?? "") . " " . ($formData["additionalFieldClasses"] ?? ""),
								$moreFieldClasses,
								$elementData["placeholder"] ?? $elementData["label"] ?? "",
								$elementData["htmlAfterInside"] ?? "",
						],
						is_callable( $htmlBase ) ? $htmlBase( $elementData, $value, $formData ) : $htmlBase
				) . ($elementData["htmlAfter"] ?? "") . "\n";
	}
	
	function formElementsHTML( array &$formData, string ...$elementNames ) {
		$result = "";
		
		foreach( $elementNames as $elementId )
			$result .= formElementHTML( $formData, $elementId );
		
		return $result;
	}
	
	function formHTML( $formData, array $additionalData = [] ) {
		if( is_string( $formData ) )
			$formData = getForm( $formData );
		
		$formData = array_merge_form( $formData, $additionalData );
		
		$result = "";
		$result .= formBeginHTML( $formData );
		$result .= formElementsHTML( $formData, ...array_keys( $formData["elements"] ) );
		$result .= formEndHTML( $formData );
		return $result;
	}
	
	function formModalHTML( $formData, array $additionalData = [] ) {
		if( is_string( $formData ) )
			$formData = getForm( $formData );
		
		$formData = array_merge_form( $formData, $additionalData );
		
		$result = "<div class='ui modal' id='{$formData["modalID"]}'>";
		$result .= "<div class=\"header\">{$formData["modalHeader"]}</div>";
		$result .= "<div class=\"content\">";
		$result .= messagesHTML( $formData["modalID"] );
		$result .= formHTML( $formData );
		$result .= "</div></div>";
		return $result;
	}
	
	function getFormPostData( $formData ) {
		$data = [];
		
		foreach( $formData["elements"] as $elementId => $elementData ) {
			if( is_string( $elementData ) )
				continue;
			
			$data[$elementId] = transformFormValueFromPOSTFromFormData( $formData, $elementId, $_POST[formElementPOSTName( $elementId )] ?? null );
		}
		
		return $data;
	}
	
	call_user_func( function() {
		global $FORM_ELEMENT_CACHE, $FORM_VALUE_TRANSFORMER_CACHE, $FORM_SUBMIT_UID, $FORM_ERROR_ELEMENTS;
		
		$FORM_VALUE_TRANSFORMER_CACHE = [];
		$FORM_ELEMENT_CACHE = [];
		$FORM_ERROR_ELEMENTS = [];
		$FORM_SUBMIT_UID = null;
		
		if( !isset( $_SESSION["formToken"] ) )
			$_SESSION["formToken"] = uniqid();
		
		$result = null;
		
		// Check form send
		if( $_POST["form_submit"] ?? false ) {
			$formSubmitName = $_POST["form_name"];
			$formSubmitId = $_POST["form_id"];
			$FORM_SUBMIT_UID = formUID( $formSubmitName, $formSubmitId );
			
			$formData = getForm( $formSubmitName );
			
			$data = [];
			try {
				$data = getFormPostData( $formData );
			} catch( InvalidValueException $e ) {
				$result = $e->data;
			}
			
			try {
				if( !$result )
					$result = $formData["action"]( $data );
			} catch( Exception $e ) {
				$result = $e->getMessage();
			}
			
			$msgGroup = $_POST["form_msgGroup"] ?? $_POST["form_modalID"] ?? "";
			
			if( is_string( $result ) )
				$result = [ "error" => $result ];
			
			if( isset( $result["error"] ) ) {
				reportMessage( $result["error"], [ "type" => "error", "group" => $msgGroup ] );
				
				if( isset( $_POST["form_modalID"] ) )
					addJSonready( "$('#{$_POST["form_modalID"]}').modal('show');" );
				
				$errElement = $result["errorElement"] ?? [];
				if( is_string( $errElement ) )
					$FORM_ERROR_ELEMENTS[$errElement] = true;
				else foreach( $errElement as $elem )
					$FORM_ERROR_ELEMENTS[$elem] = true;
				
			} else {
				if( isset( $result["succMessage"] ) )
					reportMessage( $result["succMessage"], [ "viewUID" => $_POST["form_viewUID"], "type" => "success" ] );
				
				if( isset( $result["redirect"] ) )
					$redirect = $result["redirect"];
				else {
					$redirect = $_POST["form_redirect"];
					$redirect = str_replace( "%resultData%", $result["resultData"] ?? null, $redirect );
				}
				
				header( "Location: $redirect", true, 303 );
			}
		}
	} );