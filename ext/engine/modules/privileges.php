<?php
	$INCLUDED ?? false or die;
	requireModules( "db", "users" );
	
	function userHasPrivilege( string $priv ) {
		global $USER_PRIVILEGES;
		return $USER_PRIVILEGES[$priv] ?? false;
	}
	
	function userHasPrivileges( ...$privs ) {
		foreach( $privs as $priv ) {
			if( is_array( $priv ) ) {
				if( !userHasPrivileges( ...$priv ) )
					return false;
				
			} elseif( !userHasPrivilege( $priv ) )
				return false;
		}
		
		return true;
	}
	
	call_user_func( function() {
		global $USER_PRIVILEGES, $USER_PRIVILEGES_ARRAY, $USER_DATA, $PAGES_DATA, $PAGES, $CURRENT_PAGE_DATA, $PAGE_403, $IS_LOGGED_IN, $LOADED_MODULES;
		
		$USER_PRIVILEGES = [];
		if( isLoggedIn() ) {
			$USER_PRIVILEGES["login"] = true;
			
			$q = dbQuery( "SELECT privileges FROM users WHERE id = ?", $USER_DATA["id"] );
			$USER_PRIVILEGES_ARRAY = explode( ";", $q->fetch()["privileges"] );
			
			if( is_callable( "extraPrivilegesAssigning" ) )
				extraPrivilegesAssigning();
			
			$USER_PRIVILEGES_ARRAY = array_unique( $USER_PRIVILEGES_ARRAY );
			foreach( $USER_PRIVILEGES_ARRAY as $priv )
				$USER_PRIVILEGES[$priv] = true;
		}
		
		if( $LOADED_MODULES["pages"] ?? false ) {
			requireModules( "messages" );
			
			foreach( $PAGES_DATA as $i => $data ) {
				$privs = $data["privileges"] ?? [];
				if( is_string( $privs ) )
					$privs = explode( ",", $privs );
				
				$currentPageId = count( $PAGES_DATA ) - 1;
				
				if( !userHasPrivileges( ...$privs ) ) {
					if( !$IS_LOGGED_IN ) {
						reportMessage( [ "text" => "Pro přístup k této stránce se musíte přihlásit.", "type" => "warning" ] );
						$PAGES_DATA[$i] = $PAGES["login"];
					} else
						$PAGES_DATA[$i] = $PAGE_403;
				}
				
				$CURRENT_PAGE_DATA = $PAGES_DATA[$currentPageId];
			}
		}
	} );