<?php
	$INCLUDED ?? false or die;
	
	/**
	 * DB constructor.
	 * @param $credentials array Array of db login credentials (host, user, password, db)
	 */
	call_user_func( function() {
		global $DB_DB, $DB_USER, $DB_HOST, $DB_PASSWD, $DB;
		
		$DB = new PDO( "mysql:dbname=$DB_DB;host=$DB_HOST", $DB_USER, $DB_PASSWD );
		$DB->exec( "SET NAMES utf8" );
		$DB->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		$DB->setAttribute( PDO::ATTR_EMULATE_PREPARES, 0 );
	} );
	
	$OPEN_QUERIES = 0;
	
	class DebugPDOStatement {
		
		public $query;
		public $stmt;
		
		public function __construct( $stmt, $query ) {
			global $OPEN_QUERIES;
			$this->query = $query;
			$this->stmt = $stmt;
			echo "$OPEN_QUERIES QUERY OPEN $query\n";
			$OPEN_QUERIES++;
		}
		
		public function __destruct() {
			$this->stmt->closeCursor();
			
			global $OPEN_QUERIES;
			echo "$OPEN_QUERIES QUERY CLOSE $this->query\n";
			$OPEN_QUERIES--;
		}
		
		public function fetch( ...$args ) {
			return $this->stmt->fetch( ...$args );
		}
		
		public function rowCount( ...$args ) {
			return $this->stmt->rowCount( ...$args );
		}
		
		public function execute( ...$args ) {
			return $this->stmt->execute( ...$args );
		}
		
		public function fetchAll( ...$args ) {
			return $this->stmt->fetchAll( ...$args );
		}
		
	}
	
	/**
	 * Executes an SQL query, returning first column of the first row as a return value
	 * @param string $sql SQL query
	 * @param array $params Eventual parameters in array
	 * @return mixed First column in the first row
	 */
	function dbQueryValue( $sql, ...$params ) {
		return dbQueryRow( $sql, ...$params )[0];
	}
	
	function dbQueryValueDef( $sql, $default, ...$params ) {
		return dbQueryRowDef( $sql, [ $default ], ...$params )[0];
	}
	
	/**
	 * Executes an sql query, returning one row fetched as a result
	 * @param string $sql SQL query
	 * @param array $params Eventual parameters in array
	 * @return mixed First row in AA
	 * @throws Exception
	 */
	function dbQueryRow( $sql, ...$params ) {
		$q = dbQuery( $sql, ...$params );
		
		if( $q->rowCount() == 0 )
			throw new Exception( "Wrong result row count" );
		
		return $q->fetch();
	}
	
	function dbQueryRowDef( $sql, $default, ...$params ) {
		$q = dbQuery( $sql, ...$params );
		
		if( $q->rowCount() == 0 )
			return $default;
		
		return $q->fetch();
	}
	
	/**
	 * Executes an sql query, expecting resultset (SELECT query)
	 * @param string $sql SQL query
	 * @param array $params Eventual parameters in array
	 * @return PDOStatement Resultset
	 */
	function dbQuery( $sql, ...$params ) {
		global $DB;
		
		$paramExpansion = [];
		$sql = preg_replace_callback( "/^(.*)\\(\\(\\?\\)\\)/si", function( $m ) use ( &$paramExpansion, $params ) {
			$argIndex = substr_count( $m[1], "?" );
			$argValue = $params[$argIndex];
			$argValueCount = count( $argValue );
			
			$paramExpansion[] = $argIndex;
			
			if( $argValueCount )
				return $m[1] . "( " . implode( ", ", array_fill( 0, $argValueCount, "?" ) ) . " )";
			else
				return $m[1] . "(NULL)";
		}, $sql );
		
		$paramExpansion = array_reverse( $paramExpansion );
		foreach( $paramExpansion as $index ) {
			$params = array_merge( array_slice( $params, 0, $index ), $params[$index], array_slice( $params, $index + 1 ) );
		}
		
		if( count( $params ) == 0 )
			return $DB->query( $sql );
		
		$q = $DB->prepare( $sql );
		$q->execute( $params );
		
		//return new DebugPDOStatement($q, $sql);
		return $q;
	}
	
	/**
	 * Executes an SQL insert query, returns last insert id
	 * @param $sql
	 * @param array $params
	 */
	function dbInsert( $sql, ...$params ) {
		dbExec( $sql, ...$params );
		return dbLastInsertId();
	}
	
	/**
	 * Executes a query (no resultset)
	 * @param string $sql SQL query
	 * @param array $params Eventual parameters in array
	 * @return int Rows affected
	 */
	function dbExec( $sql, ...$params ) {
		global $DB;
		
		if( !is_array( $params ) )
			$params = [ $params ];
		
		if( count( $params ) == 0 )
			return $DB->exec( $sql );
		
		//echo "EXEC $sql\n";
		
		$q = $DB->prepare( $sql );
		return $q->execute( $params );
	}
	
	function dbLastInsertId() {
		global $DB;
		return $DB->lastInsertId();
	}
	
	/**
	 * Passes only fields from $data whose key is in $filter = [ "key1", "key2," "key3" => true, "key4NOT" => false ]
	 * @param array $data
	 * @param array $filter
	 * @return array
	 */
	function dbSecureFieldsArray( array $data, array $filter ): array {
		$result = [];
		
		foreach( $filter as $key => $val ) {
			if( is_numeric( $key ) && array_key_exists( $val, $data ) )
				$result[$val] = $data[$val];
			elseif( !is_numeric( $key ) && $val && array_key_exists( $key, $data ) )
				$result[$key] = $data[$key];
		}
		
		return $result;
	}
	
	function dbUpdateRow( string $table, $constraints, array $data ) {
		if( is_null( $constraints ) || empty( $data ) )
			return;
		
		if( is_numeric( $constraints ) )
			$constraints = [ "id" => $constraints ];
		
		dbQuery(
				"UPDATE $table SET " . implode( "= ?, ", array_keys( $data ) ) . " = ? WHERE ( " . implode( "= ? ) AND (", array_keys( $constraints ) ) . " = ? )",
				...array_merge( array_values( $data ), array_values( $constraints ) ) );
	}
	
	function dbInsertRow( string $table, array $data ) {
		global $DB;
		
		dbQuery(
				"INSERT INTO $table (" . implode( ", ", array_keys( $data ) ) . ") VALUES (" . implode( ", ", array_fill( 0, count( $data ), "?" ) ) . ")",
				...array_values( $data )
		);
		return $DB->lastInsertId();
	}